## ELAPAS
This project is a web application developed using Laravel 8 and PHP 7.4. To run it, you'll need Docker installed and a local MySQL database server. While dockerizing a database server is an option, this guide assumes a local MySQL instance.

## Geoserver

- ## Vista Web
- url: http://192.168.16.26:8080/geoserver/web
- username: admin
- password: el4p4s2024

## Prerequisites:
- Docker
## Installation and Setup:
Use branch Test on this project
-----------------------------------------------------------------------------------------------------------------------------------
### 1.- Clone the .env file:
``` bash
cp .env.example .env
```

## 2.- Run Docker commands:
## a. First run:

docker compose up -d

-----------------------------------------------------------------------------------------------------------------------------------
This creates and starts all containers defined in your docker-compose.yml file.

## b. Composer installation:
``` bash
docker-compose exec app composer install
```

-----------------------------------------------------------------------------------------------------------------------------------
This installs the application's PHP dependencies within the Docker container named "app."

## c. Generate application key:
``` bash
docker-compose exec app php artisan key:generate
```
-----------------------------------------------------------------------------------------------------------------------------------
This creates a unique encryption key for your application, enhancing security.
## d. Generate access to storage
``` bash
docker-compose exec app php artisan storage:link
```
-----------------------------------------------------------------------------------------------------------------------------------
This generate the routes for php to give the access on the files of system the "app" container, making your application accessible.

## e. Generate migrations
``` bash
docker-compose exec app php artisan migrate --seed
```
-----------------------------------------------------------------------------------------------------------------------------------
This generate the seeders to get the access on the system.

## f. Run the application:
``` bash
docker-compose exec app php artisan serve
```
-----------------------------------------------------------------------------------------------------------------------------------
This starts the PHP development server within the "app" container, making your application accessible., but you must have to clone database 

## Access the Application:
#### Once the final command is executed, you should be able to access ELAPAS at  http://localhost:8001/

## Additional Notes:
- For production environments, consider a more robust web server instead of php artisan serve.
- Adjust the port number (8001) if necessary to avoid conflicts with other applications.

## System developed by Informatics Systems ELAPAS, for more information send a email message: 
- jraya@gmail.com
- carlosv97n@gmail.com
