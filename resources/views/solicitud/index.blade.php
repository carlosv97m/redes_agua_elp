@extends('adminlte::page')

@section('title', 'Solicitud')

@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>
    <h1>ELAPAS - Solicitud
        @can('solicitud.create')
            <a href="{{ route('solicitud.create') }}" class="btn btn-success btn-rounded" style="float: right;">
                Registrar Solicitud <i class="fa fa-plus-square"></i>
            </a>
        @endcan
        @can('jefe-red')
            <a href="{{ route('solicitud.reject') }}" class="btn btn-warning btn-rounded" style="float: right;">
                Solicitudes Pendientes<i class="fa fa-delete"></i>
            </a>
        @endcan
    </h1>

@stop
@section('content')

    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th>Nro</th>
                    <th>Fecha de solicitud</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th width="175px">Acciones</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($solicitud as $sol)
                    <tr>
                        <td>{{ 'S-' . $sol->id }}</td>
                        <td>{{ $sol->fecha_sol }}</td>
                        <td>{{ $sol->nombre_sol }}</td>
                        <td>{{ $sol->celular_sol }}</td>
                        <td>{{ $sol->zona_sol }}</td>
                        <td>{{ $sol->calle_sol }}</td>
                        <td>
                            <div class="btn-group">
                                <!-- Boton para mostrar el comprobante de la Solicitud -->
                                <a type="button"
                                    onclick="mostrar_comprobante('{{ route('solicitud.comprobante_solicitud', $sol) }}')"
                                    class='text-white d-inline btn btn-primary btn-icon' title="Comprobante"><i
                                        class="fas fa-ticket-alt"></i></a>
                                <!-- Boton para mostrar los Archivos de las solicitudes -->
                                <button class="d-inline btn btn-success btn-icon" id="archivos_solicitud"
                                    data-id="{{ $sol->id }}" title="Archivos" data-toggle="modal"
                                    data-target="#myModal"><i class="fas fa-plus"></i></button>
                                <!-- Boton para visualizar el Mapa de la Solicitud -->
                                <a type="button" class="d-inline btn btn-warning btn-icon" data-toggle="modal"
                                    data-target=".bd-example-modal-lg"
                                    onclick="visualizarMapa({{ $sol->x_aprox }},{{ $sol->y_aprox }}, {{ $sol->id }})"
                                    title="Visualizar" id="btn_mostrar_mapa">
                                    <i class="fas fa-eye"></i></a>

                                @if ($sol->estado_sol != 'aplazado')
                                    @can('solicitud.pdf')
                                        <button type='button' id='archivo_escaneado' data-id="{{ $sol->id }}"
                                            class="text-white d-inline btn btn-danger btn-icon" title="Solicitud Escaneada"><i
                                                class="fa fa-file-pdf"></i></button>
                                    @endcan
                                    @can('solicitud.edit')
                                        <a type="button" href='{{ route('solicitud.edit', $sol) }}'
                                            class='d-inline btn btn-info btn-icon'title="Editar"><i
                                                class="fas fa-pencil-alt"></i></a>
                                    @endcan
                                    @can('jefe-red')
                                        <a type="button" href='{{ route('solicitud.aprobar', $sol) }}'
                                            class='d-inline btn btn-success btn-icon boton-aprobar'title="Aprobar"><i
                                                class="fas fa-check"></i></a>
                                    @endcan
                                    @can('jefe-red')
                                        <a type="button" href="{{ route('solicitud.form_rechazado', $sol->id) }}"
                                            class='d-inline btn btn-danger btn-icon' title="Rechazar"><i
                                                class="fas fa-times"></i></a>
                                    @endcan
                                    @can('solicitud.delete')
                                        <div>
                                            <form action="{{ route('solicitud.destroy', $sol) }}" method="POST">
                                                @csrf
                                                @method('delete')
                                                <button class='d-inline btn btn-danger btn-icon' style="height: 100%"
                                                    title="Eliminar" type="submit"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </div>
                                    @endcan
                                @else
                                    <a type="button" onclick="mostrarPDF('{{ route('solicitud.PDFrechazado', $sol) }}')"
                                        target='_blank' class='text-white d-inline btn btn-danger btn-icon'
                                        title="Reporte Rechazado"><i class="fas fa-file"></i></a>
                                    {{-- <a type="button" onclick="mostrarPDF('{{ route('solicitud.escaneada', $sol->id) }}')"
                                        title="Solicitud Escaneada" class=" text-white btn btn-danger d-line btn-icon">
                                        <i class="fa fa-file-pdf"></i>
                                    </a> --}}
                                    <button type='button' id='archivo_escaneado' data-id="{{ $sol->id }}"
                                        class="text-white d-inline btn btn-danger btn-icon" title="Solicitud Escaneada"> <i
                                            class="fa fa-file-pdf"></i></button>
                                @endif
                            </div>

                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nro</th>
                    <th>Fecha de solicitud</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
        <div class="card-body">
            <h4>Estados</h4>
            <div>
                <p><b>Pendiente: </b> El usuario hizo su solicitud </p>
                <p><b>Aprobado: </b> La solicitud de ampliación fue aprobada por el jefe de red</p>
                <p><b>Aplazado: </b> La solicitud de ampliación fue aplazada por el jefe de red </p>
                <p><b>Asignado: </b> El jefe de red asignó a un inspector para que realice la inspeccion en la ubicación
                    solicitada</p>
                <p><b>Inspeccionado: </b> El inspector ya realizo el informe de inspeccion y solicita la revision del jefe
                    de red
                </p>
                <p><b>Autorizado: </b> El inspector ya puede realizar el peticion de materiales y mano de obra para la
                    ampliación</p>
                <p><b>Solicitud de firma: </b> El inspector ya realizó la petición de materiales y mano de obra y esta a la
                    espera de la aprobación del jefe de red</p>
                <p><b>Firmado: </b> El jefe de red ya firmó la petición de materiales y mano de obra, la ampliación es
                    derivada al proyectista</p>
                <p><b>Ejecutado: </b> La ampliación ya fue ejecutada</p>
                <p><b>Observado: </b> El jefe de red observó errores en el informe de inspección, pedido de materia o mano
                    de obra</p>
            </div>
        </div>


    </div>
    <div id="contenedor-mapa" style="display: none">
        <input type="hidden" id="obtenerAmpliaciones">

        <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i> Volver
        </button>
        <div class="col-md-12">
            <div id="map">
            </div>
        </div>
    </div>

    <!-- Vista de archivos -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Archivos de la Solicitud</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button> --}}
                </div>
                <div class="modal-body table table-bordered table-hover dataTable table-responsive w-100"
                    id="contenedor-espera">
                    <h1 id="tiempo_carga">Cargando......</h1>
                    <p id="sin_datos"></p>
                    <table id="datos_modal_archivos" class="datos_modal_archivos table table-bordered datatable"
                        width="100%" height="100%">
                    </table>
                </div>
                <div class="mostrar_guardar" style="display: none">
                    <label><input type="checkbox" id="myCheck"> Agregar nuevo registro</label>
                </div>
                <form method="POST" action="{{ route('archivos_solicitud.store') }}" id="ingresar_nuevo"
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off">
                    @csrf
                    <input type="hidden" name="solicitud_id" id="solicitud_id">
                    <div class="card-body">
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" lang="es" accept=".pdf"
                                        class=" custom-file-input @error('archivo_solicitud') is-invalid @enderror"
                                        onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                        id="archivo_solicitud" name="archivo_solicitud">
                                    <label class="custom-file-label" for="archivo_solicitud">Subir Solicitud
                                        Escaneada</label>
                                </div>
                            </div>
                            @error('archivo_solicitud')
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <label for="descripcion">Descripcion</label>
                                <textarea name="descripcion" id="descripcion" cols="60" rows="5" style="text-transform:uppercase;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="float: right;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-plus"></i> Agregar</button>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i>
                        Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Eliminacion de Archivos -->
    <div class="modal fade" id="eliminacion_archivos" tabindex="-1" role="dialog" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form method="POST" action="{{ route('archivos_solicitud.destroy') }}" id="ingresar_nuevo"
                    autocomplete="off" enctype="multipart/form-data" role="form" class="create">
                    @csrf
                    <input type="hidden" name="id_eliminar" id="id_eliminar">
                    <div class="card-body" style="text-align: center">
                        <h2><i class="fas fa-exclamation-triangle" style="color: yellow; font-size: 100px;"></i> <br>
                            ¿Está seguro?</h2>
                        <h3> ¡No podrás revertir esto!</h3>
                        <p id="id_para_eliminar" style="display: none"> <i class="fas fa-exclamation-triangle"></i> </p>
                    </div>
                    <div class="card-footer" style="text-align: center;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-trash"></i> Eliminar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i>
                            Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vista Archivos de la Solicitud -->
    <div class="modal fade" id='show_vista_archivos' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <!-- id='show_vista_archivos' -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-heeder">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> --}}
                </div>
                <h3 style="text-align: center">Cargando......</h3>
                <p id="impresion_archivos_solicitud" style="display: none"></p>
                <div class="contenedor_impresion" id="contenedor_impresion">
                    <iframe id="iframe" name="myIframe" frameborder="5" width="500" height="300"
                        style="display: none"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}"
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('js/archivos_solicitud.js') }}"></script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>

    <script>
        const ruta = "{{ route('archivos_solicitud.show') }}";
        const ruta2 = "{{ route('archivos_solicitud.mostrar') }}";
        const ruta3 = "{{ route('solicitud.escaneada') }}";
        const ruta_asset = "{{ asset('archivos_solicitudes/') }}";
        const ruta_asset2 = "{{ asset('solicitudes/') }}";
    </script>


    @if (session('eliminar') == 'Ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'Su registro ha sido eliminado.',
                'success'
            )
        </script>
    @endif
    @if (session('crear') == 'ok')
        <script>
            Swal.fire(
                'Registro Correcto!',
                'Su registro se guardo exitosamente.',
                'success'
            )
        </script>
    @endif
    @if (session('editar') == 'ok')
        <script>
            Swal.fire(
                'Registro Modificado!',
                'Su registro se edito exitosamente.',
                'success'
            )
        </script>
    @endif
    @if (session('archivo') == 'ok')
        <script>
            Swal.fire(
                'Registro Correcto!',
                'Su archivo fue guardo exitosamente.',
                'success'
            )
        </script>
    @endif
    @if (session('archivo') == 'no')
        <script>
            Swal.fire(
                'Ups.!',
                'Algo salio mal.',
                'danger'
            )
        </script>
    @endif
    @if (session('archivo_destroy') == 'ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'El archivo de su registro ha sido eliminado.',
                'success'
            )
        </script>
    @endif
    @if (session('archivo_destroy') == 'no')
        <script>
            Swal.fire(
                'Ups.!',
                'Usted no creo el archivo, revise por favor.',
                'danger'
            )
        </script>
    @endif
    @if (session('aprobar') == 'Ok')
        <script>
            Swal.fire(
                'Aprobado!',
                'La solicitud ha sido aprobada.',
                'success'
            )
        </script>
    @endif
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>
    <script src="{{ asset('js/solicitud.js') }}"></script>
    <script src="{{ asset('js/informes.js') }}"></script>

@stop
@section('footer')
    <strong>{{ date('Y') }} || ELAPAS - SISTEMA DE AMPLIACION DE REDES DE AGUA </strong>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
    <style type="text/css">
        #global {
            height: 200px;
            width: auto;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #mensajes {
            height: auto;
        }

        .texto {
            padding: 4px;
            background: #fff;
        }
    </style>

@stop
