@extends('adminlte::page')

@section('title', 'Actualizar solicitud')

@section('content_header')
    <h1>
        ELAPAS
        <a href="{{ route('solicitud.index') }}" class="btn btn-primary btn-rounded" style="float: right;">
            <i class="fa fa-arrow-alt-circle-left"></i>&nbsp;Volver Inicio </a>
    </h1>
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>

@stop

@section('content')
    <div class="container-fluid">
        <div class="justify-content-center row" id="contenedor-tabla">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Actualizar Solicitud</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- formulario inicio -->
                    <form action="{{ route('solicitud.update', $solicitud) }}" enctype="multipart/form-data" method="post"
                        role="form" id="form_solicitud" autocomplete="off">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="nombre_sol">Nombre del Solicitante</label>
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" name="nombre_sol" id="nombre_sol"
                                        style="text-transform:uppercase;" required autofocus class="form-control"
                                        placeholder="nombre del solicitante" value="{{ $solicitud->nombre_sol }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="celular_sol">Celular</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                        </div>
                                        <input type="text" name="celular_sol" id="celular_sol" required autofocus class="form-control"
                                            placeholder="Celular" value="{{ $solicitud->celular_sol }}">
                                    </div>

                                </div>

                                <div class="col-6">
                                    <label for="zona_sol">Zona</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input type="text" name="zona_sol" id="zona_sol"
                                            style="text-transform:uppercase;" required autofocus class="form-control" placeholder="Zona"
                                            value="{{ $solicitud->zona_sol }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8">
                                    <label for="calle_sol">Calle</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input type="text" name="calle_sol" id="calle_sol"
                                            style="text-transform:uppercase;" required autofocus class="form-control" placeholder="Calle"
                                            value="{{ $solicitud->calle_sol }}">
                                    </div>

                                </div>

                                <div class="col-4">
                                    <label for="fecha_sol">fecha</label>
                                    <input type="date" name="fecha_sol" id="fecha_sol" required autofocus class="form-control"
                                        placeholder="Zona" value="{{ $solicitud->fecha_sol }}">
                                    <input type="hidden" name="estado_sol" id="estado_sol"
                                        value="{{ $solicitud->estado_sol }}">
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-6">
                                    <label for="x_aprox">Latitud</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input type="text" name="x_aprox" id="x_aprox" required autofocus class="form-control"
                                            placeholder="Latitud" value="{{ $solicitud->x_aprox }}">
                                    </div>

                                </div>

                                <div class="col-6">
                                    <label for="y_aprox">Longitud</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input type="text" name="y_aprox" id="y_aprox" required autofocus class="form-control"
                                            placeholder="Longitud" value="{{ $solicitud->y_aprox }}">

                                    </div>
                                </div>
                            </div><br>
                            <div class="card-footer">
                                <button type="button" class="btn btn-block btn-outline-success" onclick="editarMapa()">
                                    Actualizar ubicacion aproximada</button>
                            </div>
                            <div class="mt-2 custom-file">
                                <input type="file" lang="es" accept=".pdf"
                                    onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                    id="sol_escaneada" name="sol_escaneada">
                                <label class="custom-file-label" for="sol_escaneada">Subir Solicitud Escaneada</label>
                                <div class="invalid-feedback">Documento Requerido</div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
                        </div>
                    </form>
                    {{-- Fin de formulario --}}
                </div>
            </div>
        </div>
        <div id="contenedor-mapa" style="display: none">
            <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i>
                Volver Atras</button>
            <div class="col-md-12">
                <div id="map">
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h5>Leyenda</h5>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/alc_camara.png') }}" class="form-check-input">
                                    <label class="form-check-label">alc_camara</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/alc_colector.png') }}" class="form-check-input">
                                    <label class="form-check-label">alc_colector</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/alc_embovedado.png') }}" class="form-check-input">
                                    <label class="form-check-label">alc_embvedado</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/ap_hidrante.png') }}" class="form-check-input">
                                    <label class="form-check-label">ap_hidrante</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/ap_tanque.png') }}" class="form-check-input">
                                    <label class="form-check-label">ap_tanque</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/ap_tuberia.png') }}" class="form-check-input">
                                    <label class="form-check-label">ap_tuberia</label>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-check form-check-inline">
                                    <img src="{{ asset('images/ap_valvula.png') }}" class="form-check-input">
                                    <label class="form-check-label">ap_valvula</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


@stop

@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}"
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script>
        function editarMapa() {
            const lat = document.getElementById('x_aprox').value;
            const long = document.getElementById('y_aprox').value;
            mostrarTabla(true);
            initEditMap(lat, long);
        }
    </script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js"></script>
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>


@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw-src.css" />
@stop
