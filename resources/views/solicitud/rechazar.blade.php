@extends('adminlte::page')

@section('title', 'Llenar informe')

@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;

        }
    </style>
    <h1>Rechazar Solicitud</h1>
@stop
@php

@endphp

@section('content')
    <div class="container">
        <div id="contenedor-mapa">
            <a type="button" href="{{ route('solicitud.index') }}" class="btn btn-primary"> <i
                    class="fas fa-arrow-circle-left"></i> Volver </a>
            <button onclick="downloadMap()" class="btn btn-danger"> <i class="fas fa-save"></i> Guardar Imagen</button>
            <form action="{{ route('solicitud.guardarAmpliacion', $solicitud->id) }}" method="POST" id="formAmpliaciones">
                @csrf</form>
            <input type="hidden" id="obtenerAmpliaciones"
                value="{{ route('solicitud.obtenerAmpliaciones', $solicitud->id) }}">
            <div class="col-12">
                <div id="map">
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h3>Formulario</h3>
            </div>
            <div class="card-body">

                <form action="{{ route('solicitud.sol_rechazar') }}" method="POST" role="form" id="form_rechazar"
                    autocomplete="off">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-8">
                            <label for="reservorio">Imagen Vista Previa</label>
                            <div class="input-group ">
                                <input type="hidden" name="id_solicitud" value="{{ $solicitud->id }}">
                                <img src="{{ asset('images/no-disponible.png') }}" width="100%"
                                    height="342px"alt="Vista Previa" id="imgMap">
                                <input type="hidden" name="textMap" id="textMap">
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="observaciones">Observaciones</label>
                            <div class="form-group">
                                <textarea class="form-control" name="observaciones" id="observaciones" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="concesion" class="checkbox">
                                <label for="concesion">Fuera del área de concesión</label>
                            </div>

                        </div>


                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-danger">Rechazar</button>
                </form>
            </div>

        </div>
    </div>




@stop



@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}"
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="{{ asset('js/html2canvas.min.js') }}"></script>
    <script src="{{ asset('js/leaflet_export.js') }}"></script>
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>

    <script>
        document.addEventListener('DOMContentLoaded', () => {
            initMap({{ $solicitud->x_aprox }}, {{ $solicitud->y_aprox }});
            // document.querySelector('#map').style.width = window.screen.width ;
        });
        document.querySelector('#concesion').addEventListener('click', (e) => {
            const observaciones = document.querySelector('#observaciones');
            if (e.target.checked) {
                observaciones.value = 'Se encuentra fuera del área de concesión';
            } else {
                observaciones.value = '';
            }
        })
    </script>
    <script>
        document.getElementById('form_rechazar').addEventListener('submit', (e) => {
            e.preventDefault();
            let val;
            const imagen = document.getElementById('imgMap').src.split('/')[4];
            if (imagen === "no-disponible.png") {
                Swal.fire({
                    icon: 'error',
                    title: 'Imagen Requerida',
                    text: 'Saque una captura del mapa',
                })
            } else {
                document.querySelector('#form_rechazar').submit()
            }
        })
    </script>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">

@stop
