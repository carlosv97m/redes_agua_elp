@extends('adminlte::page')

@section('title', 'Solicitud')

@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>
    <h1>ELAPAS - Solicitud</h1>

@stop
@section('content')

    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th>Nro</th>
                    <th>Fecha de solicitud</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th width="175px">Acciones</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($solicitud as $sol)
                    <tr>
                        <td>{{ 'S-' . $sol->id }}</td>
                        <td>{{ $sol->fecha_sol }}</td>
                        <td>{{ $sol->nombre_sol }}</td>
                        <td>{{ $sol->celular_sol }}</td>
                        <td>{{ $sol->zona_sol }}</td>
                        <td>{{ $sol->calle_sol }}</td>
                        <td>
                            <div class="btn-group">
                                <!-- Boton para mostrar el comprobante de la Solicitud -->
                                {{-- <a type="button" onclick="mostrar_comprobante('{{route('solicitud.comprobante_solicitud', $sol) }}')"  class='text-white d-inline btn btn-primary btn-icon' title="Comprobante"><i class="fas fa-ticket-alt"></i></a> --}}
                                <!-- Boton para mostrar los Archivos de las solicitudes -->
                                <button class="d-inline btn btn-success btn-icon" id="avance" data-id="{{ $sol->id }}"
                                    data-toggle="modal" data-target="#vista_avance" title="Avance de la Solicitud">
                                    <i class="fas fa-road"></i>
                                </button>
                                <!-- Para ver el reporte de los historicos -->
                                <a onclick="mostrarPDF('{{ route('solicitud.reporte_historicos', $sol->id) }}')"
                                    class="btn btn-info btn-icon" title="Reporte de Avance"><i
                                        class="fas fa-file-invoice"></i>
                                </a>
                                <button class="d-inline btn btn-warning btn-icon" id="cambio_estado_solicitud"
                                    data-id-sol="{{ $sol->id }}" data-toggle="modal"
                                    data-target="#cambio_del_estado_solicitud" title="Cambiar de estado">
                                    <i class="fas fa-arrow-left"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nro</th>
                    <th>Fecha de solicitud</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- Vista Solicitud -->
    <div class="modal fade" id='vista_avance' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <!-- id='vista_avance' -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Historico</h5>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i>
                        Cerrar</button>
                    </button>
                </div>
                <div class="modal-body">
                    <h3 id="tiempo_carga_avance" style="text-align: center">Cargando......</h3>
                    <h3 id="tiempo_carga_avance_false" style="display: none; text-align: center">Sin Informacion</h3>
                    <table id="table_avance" class="table_avance table table-bordered datatable" width="100%"
                        height="100%">
                    </table>
                </div>
                <div class="modal-footer" id="footer_avance" style="text-align: center">
                    <h3>Se permite retroceder a una accion atras</h3>
                    <form method="POST" action="{{ route('solicitud.cambiar_estado') }}" id="ingresar_nuevo"
                        enctype="multipart/form-data" role="form" class="form-inline" autocomplete="off">
                        @csrf
                        <input type="hidden" name="solicitud_id" id="solicitud_id">
                        <input type="hidden" name="estado" id="estado">
                        <label for="estado">Estado</label>
                        <input type="text" name="estado_1" id="estado_1" disabled>
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-plus"></i> Cambiar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cambio de estado indirecto --}}
    <div class="modal fade" id="cambio_del_estado_solicitud" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle" style="text-align: center">Cambiar Estado</h5>
                        </div>
                    </div> -->
                <div class="modal-body">
                    <h5 class="modal-title" id="exampleModalLongTitle" style="text-align: center">Cambiar Estado</h5>
                    <h3 id="tiempo_cambio_estado_solicitud" style="text-align: center">Cargando......</h3>
                    <h3 id="tiempo_cambio_estado_solicitud_false" style="display: none; text-align: center">Sin
                        Informacion......</h3>
                    <div class="modal-footer" id="formulario_cambio_estado" style="display: none;">
                        <form method="POST" action="{{ route('informes.paso_atras') }}" id="ingresar_nuevo"
                            enctype="multipart/form-data" role="form" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                <input type="hidden" name="solicitud_id_actual" id="solicitud_id_actual">
                                <div id="input_checked" style="display: none;">
                                    <label for="habilitarCampos">Habilitar Nuevo Cambio:</label>
                                    <input type="checkbox" id="habilitarCampos">
                                </div>
                                <div id="ocultar_con_existencia" style="display: block;">
                                    <div class="form-group">
                                        <label for="estado_actual">Estado Actual del Historico</label>
                                        <input type="text" name="estado_actual" id="estado_actual"
                                            class="form-control" readonly>
                                    </div>
                                </div>
                                <div id="informacion_del_cambio" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="de">Se cambio de:</label>
                                                <input type="text" name="de" id="de" class="form-control"
                                                    readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="a">A:</label>
                                                <input type="text" name="a" id="a" class="form-control"
                                                    readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <label for="estado_a_cambiar">Estado a cambiar</label>
                                    <select name="estado_a_cambiar" id="estado_a_cambiar" class="form-control" required>
                                        <option value="asignado">Asignado</option>
                                        <option value="inspeccionado">Inspeccionado</option>
                                        <option value="autorizado">Autorizado</option>
                                        <option value="observado">Observado</option>
                                        <option value="solicitud de firma">Solicitud de Firma</option>
                                        <option value="firmado">Firmado</option>
                                        <option value="ejecutado">Ejecutado</option>
                                        <option value="ejecutándose">Ejecutándose</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="motivo">Motivo</label>
                                    <textarea name="motivo" id="motivo" cols="20" rows="10" class="form-control"
                                        oninput="convertirAMayusculas(this)" required></textarea>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-plus"></i> Cambiar
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Cancelar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        const ruta_avance = "{{ route('solicitud.pasos_solicitud') }}";
        const ruta_paso_atras = "{{ route('informes.paso_atras') }}";

        function convertirAMayusculas(textarea) {
            textarea.value = textarea.value.toUpperCase();
        }
    </script>
    <script src="{{ asset('js/solicitud.js') }}"></script>

@stop
@section('footer')
    <strong>{{ date('Y') }} || ELAPAS - SISTEMA DE AMPLIACION DE REDES DE AGUA </strong>
@stop
