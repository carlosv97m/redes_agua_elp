@extends('adminlte::page')

@section('title', 'Registrar solicitud')

@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 70%;
        }
    </style>
    <div class="card-header">
        <div class="card-title">
            <h1>
                ELAPAS

            </h1>
        </div>
        <div class="float-right" style="text-align: right; display:block;">
            <a href="{{ route('solicitud.index') }}" class="btn btn-primary btn-rounded float-right" id="btn_atras"
                style="float: right;"><i class="fa fa-arrow-alt-circle-left"></i>&nbsp;Volver </a>
        </div>
        <div id="mostrar_mapa" style="display: none" class="card-tools">
            <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i> Volver
            </button>
        </div>
    </div>
@stop

@section('content')

    <div class="container-fluid">
        <div class="justify-content-center row" id="contenedor-tabla">
            <!-- left column -->

            <div class="col-md-6">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Registrar Solicitud</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- formulario inicio -->
                    <form action="{{ route('solicitud.store') }}" method="POST" enctype="multipart/form-data"
                        role="form" class="create" id="form_solicitud" autocomplete="off">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="nombre_sol">Nombre del Solicitante</label>
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" name="nombre_sol" id="nombre_sol"
                                        style="text-transform:uppercase;"
                                        class="form-control {{ $errors->has('nombre_sol') ? 'is-invalid' : '' }}"
                                        autocomplete="off" value="{{ old('nombre_sol') }}" autofocus
                                        placeholder="nombre del solicitante">
                                    @error('nombre_sol')
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @enderror
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="celular_sol">Celular</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                        </div>
                                        <input type="number" autocomplete="off" name="celular_sol" id="celular_sol"
                                            class="form-control {{ $errors->has('celular_sol') ? 'is-invalid' : '' }}"
                                            value="{{ old('celular_sol') }}"placeholder="Celular">
                                        @if ($errors->has('celular_sol'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('celular_sol') }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-6">
                                    <label for="zona_sol">Zona</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input type="text" autocomplete="off" name="zona_sol" id="zona_sol"
                                            style="text-transform:uppercase;"
                                            class="form-control {{ $errors->has('zona_sol') ? 'is-invalid' : '' }}"
                                            value="{{ old('zona_sol') }}" placeholder="Zona">
                                        @if ($errors->has('zona_sol'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('zona_sol') }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8">
                                    <label for="Calle">Calle</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input type="text" autocomplete="off" name="calle_sol" id="calle_sol"
                                            style="text-transform:uppercase;"
                                            class="form-control {{ $errors->has('zona_sol') ? 'is-invalid' : '' }}"
                                            placeholder="Calle">
                                        @if ($errors->has('calle_sol'))
                                            <div class="invalid-feedback">
                                                <strong>{{ $errors->first('calle_sol') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-4">
                                    <label for="exampleInputEmail1">fecha</label>
                                    <input type="date" name="fecha_sol" id="fecha_sol" class="form-control"
                                        value="{{ date('Y-m-d') }}">
                                    <input type="hidden" name="estado_sol" id="estado_sol" value="pendiente">
                                    <input type="hidden" name="x_aprox" id="x_aprox" {{-- value="-19.034432" --}}>
                                    <input type="hidden" name="y_aprox" id="y_aprox" {{-- value="-65.264812" --}}>
                                </div>
                            </div><br>
                            <div class="card-footer">

                                <button type="button" class="btn btn-block btn-outline-success" onclick="editarMapa()">
                                    Registrar ubicacion aproximada</button>

                                <div class=" mt-2 form-group">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" lang="es" accept=".pdf"
                                                class=" custom-file-input @error('sol_escaneada') is-invalid @enderror"
                                                onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                                id="sol_escaneada" name="sol_escaneada">
                                            <label class="custom-file-label" for="sol_escaneada">Subir Solicitud
                                                Escaneada</label>
                                        </div>
                                    </div>
                                    @error('sol_escaneada')
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-block btn-primary">Registrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="contenedor-mapa" style="display: none">
            <input type="hidden" id="obtenerAmpliaciones">
            <div class="col-md-12">
                <div id="map">
                </div>
            </div>
        </div>
    </div>
    <hr>

@stop

@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}"
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js"></script>

    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>

    @if (session('crear') == 'Ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'Su registro ha sido eliminado.',
                'success'
            )
        </script>
    @else
        @if ($errors->any())
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo salió mal!',
                })
            </script>
        @endif
    @endif
    <script>
        function editarMapa() {
            const lat = -19.034432;
            const long = -65.264812;
            mostrarTabla(true);
            initEditMap(lat, long);
            $('#mostrar_mapa').css('display', 'block')
        }
    </script>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw-src.css" />
@stop
