@extends('adminlte::page')

@section('title', 'Editar Material')

@section('content_header')
    <h1>Editar Material</h1>
@stop

@section('content')
<div class="justify-content-center row">
    <!-- left column -->
    <div class="col-md-8">
    <!-- general form elements -->
        <div class="card card-primary ">
        <div class="card-header">
            <h3 class="card-title">Editar Material</h3>
        </div>
        <!-- /.card-header -->
        <!-- formulario inicio -->
        <form action="{{route('materials.update',$material)}}" method="POST" role="form" id="form_materials" autocomplete="off">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="codigo">Codigo del material</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text" name="codigo" id="codigo" class="form-control" style="text-transform:uppercase;" value="{{$material->codigo}}" placeholder="codigo del material">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre_material">Nombre del material</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text" name="nombre_material" id="nombre_material" class="form-control" style="text-transform:uppercase;" value="{{$material->nombre_material}}" placeholder="nombre del material">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cantidad">Cantidad del material</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="number" name="cantidad" id="cantidad" class="form-control"  value="{{$material->cantidad}}" placeholder="cantidad del material">
                    </div>
                </div>
                <div class="form-group">
                    <label for="precio">Precio del material</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text" name="precio" id="precio" class="form-control"  value="{{$material->precio_unitario}}" placeholder="precio del material">
                    </div>
                </div>
                <div class="form-group">
                    <label for="observaciones">Observaciones</label>
                    <div class="input-group ">
                        <textarea name="observaciones" id="observaciones" class="form-control" style="text-transform:uppercase;" >{{$material->observaciones}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <select class="form-control" name="estado" id="estado">
                        <option value="disponible">Disponible</option>
                        <option value="no disponible">No Disponible</option>
                      </select>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-block btn-primary">Registrar</button>
            </div>
        </form>
        {{-- Fin de formulario --}}
        </div>
    </div>
</div>

@stop

@section('js')
    <script>
    $('.select2').select2();
    const input = document.getElementById('precio');
    input.addEventListener('blur', function() {
        const valor = this.value.trim(); // eliminamos los espacios en blanco al inicio y al final
        if (isNaN(valor)) {
            alert('Debe ingresar un número válido.');
            this.value = ''; // borra el valor introducido
        }
    })
    </script>
@stop
