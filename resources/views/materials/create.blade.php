@extends('adminlte::page')

@section('title', 'Material')

@section('content_header')
    <h1>Materiales</h1>
@stop

@section('content')
<div class="justify-content-center row">
    <!-- left column -->
    <div class="col-md-8">
    <!-- general form elements -->
        <div class="card card-primary ">
        <div class="card-header">
            <h3 class="card-title">Registrar Material</h3>
        </div>
        <!-- /.card-header -->
        <!-- formulario inicio -->
        <form action="{{route('materials.store')}}" method="POST" class="create" role="form" id="form_materials" autocomplete="off">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="codigo">Codigo del material</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text" name="codigo" id="codigo" class="form-control" style="text-transform:uppercase;" placeholder="nombre del material" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre_material">Nombre del material</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text" name="nombre_material" id="nombre_material" class="form-control" style="text-transform:uppercase;" placeholder="nombre del material" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="precio_unitario">Precio Unitario</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text" name="precio_unitario" id="precio_unitario" class="form-control" placeholder="Precio unitario, use '.' para indicar decimal" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
                        </div>
                        <input type="text"  name="cantidad" id="cantidad" class="form-control" placeholder="0" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="unidad_medida">Unidad de Medida</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-sort-numeric-down"></i></span>
                        </div>
                        <input type="text" name="unidad_medida" id="unidad_medida" class="form-control" style="text-transform:uppercase;" placeholder="Ej. metros" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="observaciones">Observaciones</label>
                    <div class="input-group ">
                        <textarea name="observaciones" id="observaciones" class="form-control" style="text-transform:uppercase;"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="estado">Estado</label>
                    <select class="form-control" name="estado" id="estado">
                        <option value="disponible">Disponible</option>
                        <option value="no disponible">No Disponible</option>
                      </select>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-block btn-primary">Registrar</button>
            </div>
        </form>
        {{-- Fin de formulario --}}
        </div>
    </div>
</div>

@stop

@section('js')
    <script>
    $('.select2').select2();
    const input = document.getElementById('cantidad');
    input.addEventListener('blur', function() {
        const valor = this.value.trim(); // eliminamos los espacios en blanco al inicio y al final
        if (isNaN(valor)) {
            alert('Debe ingresar un número válido.');
            this.value = ''; // borra el valor introducido
        }
    })
    const input2 = document.getElementById('precio_unitario');
    input2.addEventListener('blur', function() {
        const valor = this.value.trim(); // eliminamos los espacios en blanco al inicio y al final
        if (isNaN(valor)) {
            alert('Debe ingresar un número válido.');
            this.value = ''; // borra el valor introducido
        }
    })
    </script>
@stop

