@extends('adminlte::page')

@section('title', 'Inspeccionados')
@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>
@stop

@section('content')
    <h1>ELAPAS - Inspeccionados


    </h1>
    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable w-100" id="example" >
            <thead>
                <tr>
                    <th>NRO</th>
                    <th>NOMBRE SOLICITANTE</th>
                    <th>TELEFONO</th>
                    <th>FECHA <br>INSPECCION</th>
                    <th>CALLE - <br> ZONA</th>
                    @can('jefe-red')
                    <th>INSPECTOR</th>
                    @endcan
                    <th>ESTADO</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($informes as $inf)
                    @php
                        $mapa_geo = $inf->ubicacion == null ? 'https://maps.google.com/?q=' . $inf->x_aprox . ',' . $inf->y_aprox . '' : $inf->ubicacion;
                    @endphp
                    <tr>
                        <td>{{ 'S-' . $inf->id_solicitud }}</td>
                        <td>{{ $inf->nombre_sol }}</td>
                        @if ($inf->celular_sol != '0')
                            <td>{{ $inf->celular_sol }}</td>
                        @else
                            <td>S/N</td>
                        @endif
                        <td>{{ date('d-m-Y H:i:s', strtotime($inf->fecha_inspeccion)) }}</td>
                        <td>{{ $inf->calle_sol }} - <br> {{ $inf->zona_sol }}</td>
                        @can('jefe-red')
                            <td>{{strtoupper($inf->nombre_inspector)}}</td>
                        @endcan
                        @if ($inf->estado == 'observado')
                            <td align="center"><span class="badge badge-danger">{{ strtoupper($inf->estado) }}</span></td>
                        @else
                            <td align="center"><span class="badge badge-primary">{{ strtoupper($inf->estado) }}</span></td>
                        @endif
                        <td>
                            <div class="btn-group">
                                <!-- Boton para mostrar los Archivos de las solicitudes -->
                                <button class="d-inline btn btn-success btn-icon" id="archivos_solicitud" data-id="{{ $inf->id_solicitud }}" title="Archivos" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i></button>
                                <!-- Boton para mostrar la solicitud incial -->
                                <button type='button' id='archivo_escaneado_informes' data-id="{{ $inf->id_solicitud }}" class="d-inline btn btn-info btn-icon" title="Solicitud Escaneada"><i class="fa fa-file-pdf"></i></button>
                                {{-- <a type="button" class="d-inline btn btn-warning btn-icon" title="visualizar" onclick="visualizarMapa({{ $inf->x_exact == null ? $inf->x_aprox : $inf->x_exact }},{{ $inf->y_exact == null ?  $inf->y_aprox : $inf->y_exact }}, {{ $inf->id_solicitud }})"> <i class="fas fa-eye"></i></a> --}}
                                <a type="button" href="{{ $mapa_geo }}" target="_blank" class="d-inline btn btn-success btn-icon"> <i class="fas fa-map-marker-alt"></i> </a>
                                <a onclick="mostrarPDF('{{ route('descargarPDF.informe', $inf->id_informe) }}')" target="_blank" class='text-white btn btn-danger btn-icon' title="Informe"><i class="fas fa-file-pdf"></i></a>
                                <a href='{{ route('informes.edit', $inf->id_informe) }}' class='btn btn-info btn-icon' title="Llenar"><i class="fas fa-pencil-alt"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>NRO</th>
                    <th>NOMBRE SOLICITANTE</th>
                    <th>TELEFONO</th>
                    <th>FECHA <br>INSPECCION</th>
                    <th>CALLE - <br> ZONA</th>
                    @can('jefe-red')
                    <th>INSPECTOR</th>
                    @endcan
                    <th>ESTADO</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
        <div class="card-body">
            <h4>Estados</h4>
            <div>
                <p><b>Pendiente: </b> El usuario hizo su solicitud </p>
                <p><b>Aprobado: </b> La solicitud de ampliación fue aprobada por el jefe de red</p>
                <p><b>Aplazado: </b> La solicitud de ampliación fue aplazada por el jefe de red </p>
                <p><b>Asignado: </b> El jefe de red asignó a un inspector para que realice la inspeccion en la ubicación
                    solicitada</p>
                <p><b>Inspeccionado: </b> El inspector ya realizo el informe de inspeccion y solicita la revision del jefe
                    de red
                </p>
                <p><b>Autorizado: </b> El inspector ya puede realizar el peticion de materiales y mano de obra para la
                    ampliación</p>
                <p><b>Solicitud de firma: </b> El inspector ya realizó la petición de materiales y mano de obra y esta a la
                    espera de la aprobación del jefe de red</p>
                <p><b>Firmado: </b> El jefe de red ya firmó la petición de materiales y mano de obra, la ampliación es
                    derivada al proyectista</p>
                <p><b>Ejecutado: </b> La ampliación ya fue ejecutada</p>
                <p><b>Observado: </b> El jefe de red observó errores en el informe de inspección, pedido de materia o mano
                    de obra</p>
            </div>
        </div>

        <!-- Large modal -->


        <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" style="overflow:hidden;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">LISTA DE MATERIALES</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="contenido">
                        {{--  --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="contenedor-mapa" style="display: none">
        <input type="hidden" id="obtenerAmpliaciones">

        <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i> Volver
        </button>
        <div class="col-md-12">
            <div id="map">
            </div>


        </div>
    </div>
    <!-- Vista de archivos -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Archivos de la Solicitud</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button> --}}
                </div>
                <div class="modal-body table table-bordered table-hover dataTable table-responsive w-100"
                    id="contenedor-tabla">
                    <h1 id="tiempo_carga">Cargando......</h1>
                    <p id="sin_datos"></p>
                    <table id="datos_modal_archivos" class="datos_modal_archivos table table-bordered datatable"
                        width="100%" height="100%">
                    </table>
                </div>
                <div class="mostrar_guardar" style="display: none">
                    <label><input type="checkbox" id="myCheck"> Agregar nuevo registro</label>
                </div>
                <form method="POST" action="{{ route('archivos_solicitud.store') }}" id="ingresar_nuevo"
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off">
                    @csrf
                    <input type="hidden" name="solicitud_id" id="solicitud_id">
                    <div class="card-body">
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" lang="es" accept=".pdf"
                                        class=" custom-file-input @error('archivo_solicitud') is-invalid @enderror"
                                        onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                        id="archivo_solicitud" name="archivo_solicitud">
                                    <label class="custom-file-label" for="archivo_solicitud">Subir Solicitud
                                        Escaneada</label>
                                </div>
                            </div>
                            @error('archivo_solicitud')
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <label for="descripcion">Descripcion</label>
                                <textarea name="descripcion" id="descripcion" cols="60" rows="5" style="text-transform:uppercase;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="float: right;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-plus"></i> Agregar</button>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i>
                        Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Eliminacion de Archivos -->
    <div class="modal fade" id="eliminacion_archivos" tabindex="-1" role="dialog" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form method="POST" action="{{ route('archivos_solicitud.destroy') }}" id="ingresar_nuevo"
                    autocomplete="off" enctype="multipart/form-data" role="form" class="create">
                    @csrf
                    {{-- @method('delete') --}}
                    <input type="hidden" name="id_eliminar" id="id_eliminar">
                    <div class="card-body" style="text-align: center">
                        <h2><i class="fas fa-exclamation-triangle" style="color: yellow; font-size: 100px;"></i> <br>
                            ¿Está seguro?</h2>
                        <h3> ¡No podrás revertir esto!</h3>
                        <p id="id_para_eliminar" style="display: none"> <i class="fas fa-exclamation-triangle"></i> </p>
                    </div>
                    <div class="card-footer" style="text-align: center;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-trash"></i> Eliminar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i>
                            Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vista Archivos de la Solicitud -->
    <div class="modal fade" id='show_vista_archivos' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <!-- id='show_vista_archivos' -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-heeder">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> --}}
                </div>
                <h3 style="text-align: center">Cargando......</h3>
                <p id="impresion_archivos_solicitud" style="display: none"></p>
                <div class="contenedor_impresion" id="contenedor_impresion">
                    <iframe id="iframe" name="myIframe" frameborder="5" width="500" height="300"
                        style="display: none"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}";
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
        const ruta = "{{ route('archivos_solicitud.show') }}";
        const ruta2 = "{{ route('archivos_solicitud.mostrar') }}";
        const ruta3 = "{{ route('solicitud.escaneada') }}";
        const ruta4 = "{{ route('informes.verificacion_estado') }}"
        const ruta_asset = "{{ asset('archivos_solicitudes/') }}";
        const ruta_asset2 = "{{ asset('solicitudes/') }}";
    </script>
    <script>
        $('.select2').select2();
        $.fn.modal.Constructor.prototype._enforceFocus = function() {};

        function llamar(url) {
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200) {
                    document.getElementById("contenido").innerHTML = ajax.responseText;
                }
            };
            ajax.open("GET", url, true);
            ajax.send();
        }

        $('#solicitarAutorizacion').submit(function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Desea solicitar la revisión del informe?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Solicitar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    this.submit();

                }
            })
        })
    </script>
    <script>
        function visualizarMapa(lat, long, ruta) {
            mostrarTabla(true);
            document.querySelector('#obtenerAmpliaciones').value = 'solicitud/' + ruta + '/obtener_ampliacion';
            ruta == null ? initMap(lat, long, 'mostrar') : initMap(lat, long);
        }
    </script>
    <script src="{{ asset('js/archivos_solicitud.js') }}"></script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>
    <script src="{{ asset('js/informes.js') }}"></script>

@stop
@section('footer')
    <strong>{{ date('Y') }} || ELAPAS - SISTEMA DE AMPLIACION DE REDES DE AGUA </strong>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
    <style>
        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        }
    </style>
@stop
