@extends('adminlte::page')

@section('title', 'Concluidos')


@section('content_header')
    <div class="row col-sm-12">
        <h1 class="col-sm-10">ELAPAS - Solicitude Ejecutadas</h1>
        {{-- <button style="display:none;" id="btnVolver" onclick="ocultarVolver()" class=" col-sm-2 btn btn-danger btn-icon"><i class="fas fa-arrow-circle-left"> Volver</i></button> --}}
    </div>



@stop
@section('content')
    <div class="card" id="formulario">
        <div class="card-body">
            {{-- <h5 class="card-title">Cambia las fechas para obtener resultados</h5> --}}
            <div class="row">
                {{-- <form method="POST" action="{{ route('solicitud.cambiar_estado') }}" id="ingresar_nuevo"
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off"> --}}
                <form class="form-inline col-sm-12" action="{{ route('informes.ejecutados') }}" method='GET'
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off">@csrf
                    <div class="form-group">
                        <label for="nombre_material">De :<input type="date" name='fecha_inicio' id="fecha_inicio"
                                value={{ $fecha_inicio }} class="form-control"></label>

                    </div>
                    <div class="form-group">
                        <label for="nombre_material">Hasta :
                            <input type="date" name='fecha_fin' id="fecha_fin" value={{ $fecha_fin }}
                                class="form-control"> </label>

                    </div>
                    <div class="form-group">
                        {{-- id="btn_imprimir" --}}
                        <button class="btn btn-info btn-icon" title="Limpiar" id="limpiar" type="button">Limpiar&nbsp;<i
                                class="ml-2 mr-2 fas fa-sync fa-spin fa-lg"></i></button>

                        <div id="imprimir"
                            @if (count($informes) > 0) style="display:block;" @else style="display:none;" @endif>
                            {{-- <form method="post" action="{{ route('informes.reporte_ejecutados') }}">
                                    @csrf
                                    <input type="hidden" name="form_fecha_inicio" id="form_fecha_inicio">
                                    <input type="hidden" name="form_fecha_fin" id="form_fecha_fin">
                                    <button type="submit" class="btn btn-danger btn-icon" title="Imprimir" >Imprimir&nbsp;<i class="fas fa-file-pdf"></i></button>
                                </form> --}}
                            <button type="button" class="btn btn-danger btn-icon mr-2" title="Imprimir"
                                id="btn_imprimir">Imprimir&nbsp;<i class="fas fa-file-pdf"></i></button>
                        </div>
                        <button type="submit" class="btn btn-primary btn-icon" title="Buscar Amliaciones"
                            id="btn_imprimir">Buscar&nbsp;<i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th>#</th>
                    <th>NOMBRE SOLICITANTE</th>
                    <th>FECHA DE<br> SOLICITUD</th>
                    <th>FECHA DE<br> INSPECCION</th>
                    <th>CALLE</th>
                    <th>ZONA</th>
                    <th>FECHA PROGRAMADA <br> DE EJECUCION</th>
                    <th>FECHA FIN DE<br> EJECUCION</th>
                    <th>Inspector</th>
                    <th>ESTADO</th>
                    <th>-</th>
                <tr>
            </thead>
            <tbody>
                @if (count($informes) > 0)
                    @foreach ($informes as $element)
                        <tr>
                            <td> {{ $loop->index + 1 }} </td>
                            <td> {{ $element->nombre_sol }} </td>
                            <td> {{ date('d-m-Y',strtotime($element->fecha_sol)) }} </td>
                            <td> {{ date('d-m-Y H:i:s',strtotime($element->fecha_hora_in)) }} </td>
                            <td> {{ $element->calle_sol }} </td>
                            <td> {{ $element->zona_sol }} </td>
                            <td> {{ date('d-m-Y',strtotime($element->fecha_progrmada)) }} </td>
                            <td> {{ date('d-m-Y',strtotime($element->fecha_ejecutada)) }} </td>
                            <td> {{ strtoupper($element->name) }} </td>
                            <td> {{ strtoupper($element->estado_in) }} </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="text-center">
                        <td colspan="9">No se encontraron resultados</td>
                    </tr>
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>NOMBRE SOLICITANTE</th>
                    <th>FECHA DE<br> SOLICITUD</th>
                    <th>FECHA DE<br> INSPECCION</th>
                    <th>CALLE</th>
                    <th>ZONA</th>
                    <th>FECHA PROGRAMADA <br> DE EJECUCION</th>
                    <th>FECHA FIN DE<br> EJECUCION</th>
                    <th>Inspector</th>
                    <th>ESTADO</th>
                    <th>-</th>
                <tr>
            </tfoot>
        </table>
    </div>






@stop
@section('js')
    <script>
        const ruta_ejecutados = "{{ route('informes.ejecutados') }}"
        const ruta_impresion = "{{ route('informes.reporte_ejecutados') }}"
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('js/informes.js') }}"></script>
    <script src="{{ asset('js/reporte_ampliaciones.js') }}"></script>


@stop
