@extends('adminlte::page')

@section('title', 'Autorizacio')
@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>
@stop

@section('content')
    <h1>ELAPAS - Informes Autorizados</h1>
    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th>NRO</th>
                    <th>ZONA O BARRIO</th>
                    <th>NOMBRE SOLICITANTE</th>
                    <th>FECHA DE EJECUCION PROGRAMADA</th>
                    @can('jefe-red')
                        <th>INSPECTOR</th>
                    @endcan
                    @can('users.index')
                        <th>INSPECTOR</th>
                    @endcan
                    <th>ESTADO</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $num = 1;
                @endphp
                @foreach ($informes as $inf)
                    <tr>
                        <td>{{ 'S-' . $inf->id_solicitud }}</td>
                        <td>{{ $inf->zona_sol }}</td>
                        <td>{{ $inf->nombre_sol }}</td>
                        <td>
                            {{ date('d-m-Y', strtotime($inf->fecha_programada)) }}
                        </td>
                        @can('jefe-red')
                            <td>
                                {{ strtoupper($inf->nombre_inspector) }}
                            </td>
                        @endcan
                        @can('users.index')
                            <td>
                                {{ strtoupper($inf->nombre_inspector) }}
                            </td>
                        @endcan
                        @if ($inf->estado == 'observado')
                            <td align="center"><span class="badge badge-danger">{{ strtoupper($inf->estado) }}</span></td>
                        @else
                            <td align="center"><span class="badge badge-primary">{{ strtoupper($inf->estado) }}</span></td>
                        @endif
                        <td width="200px">
                            <div class="btn-group">
                                @php
                                    // NO es una buena practica hacer las consultas en la vista, deberia ser por modelo o controlador
                                    $mano_obra = DB::table('informes')
                                        ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                                        ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                                        ->join('ejecucions', 'ejecucions.solicitud_id', '=', 'solicituds.id')
                                        ->join('mano_obras', 'mano_obras.ejecucion_id', '=', 'ejecucions.id')
                                        ->select(DB::raw('COUNT(mano_obras.id) as pedidos_maeterial'))
                                        ->where('informes.id', $inf->id_informe)
                                        ->first();
                                    // echo $mano_obra->pedidos_maeterial;
                                @endphp
                                <!-- Boton para mostrar los Archivos de las solicitudes -->
                                <button class="d-inline btn btn-success btn-icon" id="archivos_solicitud"
                                    data-id="{{ $inf->id_solicitud }}" title="Archivos" data-toggle="modal"
                                    data-target="#myModal">
                                    <i class="fas fa-plus"></i>
                                </button>
                                <a type="button" class="d-inline btn btn-warning btn-icon" title="Visualizar"
                                    onclick="visualizarMapa({{ $inf->x_exact }},{{ $inf->y_exact }}, '{{ route('solicitud.obtenerAmpliaciones', $inf->id_solicitud) }}')">
                                    <i class="fas fa-eye"></i>
                                </a>
                                @can('users.index')
                                    {{-- @if ($inf->estado == 'autorizado' || $inf->estado == 'observado') --}}
                                    <button type="button" class='btn btn-info btn-icon ' data-toggle="modal"
                                        data-target=".bd-example-modal-lg"
                                        onclick="llamar('{{ route('informes.show', $inf->id_informe) }}','material')"
                                        title="Ver Materiales">
                                        <i class="fas fa-box"></i>
                                    </button>
                                    <button type="button" class='btn btn-danger btn-icon ' data-toggle="modal"
                                        data-target=".bd-example-modal-lg"
                                        onclick="llamar('{{ route('informes.show_costos', $inf->id_informe) }}','material')"
                                        title="Editar Costos Materiales">
                                        <i class="fas fa-box"></i>
                                    </button>
                                    @if ($inf->id_ejecucion)
                                        <button type="button"
                                            onclick="llamar('{{ route('mano_obra.show', $inf->id_ejecucion) }}','mano_obra')"
                                            data-toggle="modal" data-target="#modal_mano_obra" class='btn btn-warning btn-icon'
                                            title="Registrar Mano de Obra">
                                            <i class="fas fa-hammer"></i>
                                        </button>
                                        @if($inf->estado == "ejecutado")
                                        <button type="button" class='btn btn-success btn-icon' data-toggle="modal"
                                            data-target=".bd-example-modal-lg{{ $inf->id_ejecucion }}" onclick=""
                                            title="Generar Informes"><i class="fas fa-file-signature"></i></button>
                                        <div class="modal fade bd-example-modal-lg{{ $inf->id_ejecucion }}" role="dialog"
                                            aria-labelledby="myLargeModalLabel" aria-hidden="true" style="overflow:hidden;">
                                            <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Ejecución</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="justify-content-center row">
                                                            <!-- left column -->
                                                            <div class="col-md-12">
                                                                <!-- general form elements -->
                                                                <div class="card card-primary ">
                                                                    <div class="card-header">
                                                                        <h3 class="card-title">Programar Ejecución</h3>
                                                                    </div>
                                                                    <!-- /.card-header -->
                                                                    <!-- formulario inicio -->
                                                                    <form
                                                                        action="{{ route('ejecucion.ejecutada', $inf->id_ejecucion) }}"
                                                                        method="POST" role="form" id="form_materials"
                                                                        autocomplete="off">
                                                                        @csrf
                                                                        <div class="card-body">
                                                                            <input type="hidden" name="id_informe"
                                                                                value="{{ $inf->id_informe }}">
                                                                            <div class="form-group">
                                                                                <label for="nombre_material">Solicitud</label>
                                                                                <p class="form-control">{{ $inf->nombre_sol }}
                                                                                    -
                                                                                    {{ $inf->zona_sol }} </p>
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="fecha_inspe">Fecha de
                                                                                    ejecución</label>
                                                                                <div class="input-group ">
                                                                                    <div class="input-group-prepend">
                                                                                    </div>
                                                                                    <input class="form-control" id="fecha_inspe"
                                                                                        name="fecha_ejecutada" type="date"
                                                                                        value="" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.card-body -->
                                                                        <div class="card-footer">
                                                                            <button type="submit"
                                                                                class="btn btn-block btn-primary">Enviar</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                    {{-- @if ($mano_obra->pedidos_maeterial >= 3)
                                        <button type="button" id="solicitar" data-id="{{ $inf->id_informe }}"
                                            class='d-inline btn btn-success btn-icon' data-toggle="modal"
                                            data-target="#solicitando_firma" title="Solicitar Firma">
                                            <i class="fas fa-check-double"></i>
                                        </button>
                                    @endif --}}
                                @endcan
                                @can('inspector')
                                    @if ($inf->estado == 'autorizado' || $inf->estado == 'observado')
                                        <button type="button" class='btn btn-info btn-icon ' data-toggle="modal"
                                            data-target=".bd-example-modal-lg"
                                            onclick="llamar('{{ route('informes.show', $inf->id_informe) }}','material')"
                                            title="Material">
                                            <i class="fas fa-box"></i>
                                        </button>
                                        @if ($inf->id_ejecucion != null)
                                            <button type="button"
                                                onclick="llamar('{{ route('mano_obra.show', $inf->id_ejecucion) }}','mano_obra')"
                                                data-toggle="modal" data-target="#modal_mano_obra"
                                                class='btn btn-warning btn-icon' title="Registrar Mano de Obra">
                                                <i class="fas fa-hammer"></i>
                                            </button>
                                        @endif
                                        @if ($mano_obra->pedidos_maeterial >= 3)
                                            <button type="button" id="solicitar" data-id="{{ $inf->id_informe }}"
                                                class='d-inline btn btn-success btn-icon' data-toggle="modal"
                                                data-target="#solicitando_firma" title="Solicitar Firma">
                                                <i class="fas fa-check-double"></i>
                                            </button>
                                        @endif
                                        <a onclick="mostrarPDF('{{ route('pedidoPDF.informe', $inf->id_informe) }}')"
                                            target="_blank" class='d-inline btn btn-success btn-icon'
                                            title="Solicitud de Materiales">
                                            <i class="fas fa-file-pdf"></i>
                                        </a>
                                    @elseif ($inf->estado == 'ejecutandose')
                                        <button type="button" id="solicitar" data-id="{{ $inf->id_informe }}"
                                            class='d-inline btn btn-success btn-icon' data-toggle="modal"
                                            data-target="#solicitando_firma" title="Solicitar Mas Materiales"><i
                                                class="fas fa-check-double"></i>
                                        </button>
                                    @endif
                                @endcan
                                {{-- <button type="button" class='btn btn-danger btn-icon ' data-toggle="modal"
                                        data-target=".bd-example-modal-lg"
                                        onclick="llamar('{{ route('informes.show_costos', $inf->id_informe) }}','material')"
                                        title="Editar Costos Materiales">
                                        <i class="fas fa-box"></i>
                                    </button> --}}
                                <button type="button" class="btn btn-primary btn-icon" title="Informes"
                                    data-toggle="modal" data-target="#exampleModal{{ $num }}">
                                    <i class="fa fa-file"></i>
                                </button>
                                <div class="modal fade" id="exampleModal{{ $num }}" tabindex="-1"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Informes</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div style="text-align: center;">

                                                    <p>
                                                        <button type='button' id='archivo_escaneado_informes'
                                                            data-id="{{ $inf->id_solicitud }}"
                                                            class=" text-white btn btn-danger btn-icon w-75">
                                                            Solicitud Escaneada&nbsp;
                                                            <i class="fa fa-file-pdf"></i>
                                                        </button>
                                                    </p>
                                                    <p>
                                                        <a onclick="mostrarPDF('{{ route('descargarPDF.informe', $inf->id_informe) }}')"
                                                            target="_blank"
                                                            class='text-white btn btn-danger btn-icon w-75'>
                                                            Informe de Inspección&nbsp;
                                                            <i class="fas fa-file-pdf"></i>
                                                        </a>
                                                    </p>
                                                    @can('jefe-red')
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('pedidoPDF.informe', $inf->id_informe) }}')"
                                                                target="_blank"
                                                                class='text-white btn btn-danger btn-icon w-75'>
                                                                Pedido de Material&nbsp;
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </p>
                                                    @endcan
                                                    @can('users.index')
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('pedidoPDF.informe', $inf->id_informe) }}')"
                                                                target="_blank"
                                                                class='text-white btn btn-danger btn-icon w-75'>
                                                                Pedido de Material&nbsp;
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </p>
                                                    @endcan
                                                    <p>
                                                        <a onclick="mostrarPDF('{{ route('reportePDF.informe_material', $inf->id_informe) }}')"
                                                            target="_blank"
                                                            class='text-white btn btn-danger btn-icon w-75'>
                                                            Informe Ampliacion&nbsp;
                                                            <i class="fas fa-file-pdf"></i>
                                                        </a>
                                                    </p>
                                                    @can('users.index')
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('descargarPDF.proyecto', $inf->id_informe) }}')"
                                                                target="_blank"
                                                                class="text-white btn btn-danger btn-icon w-75">Informe
                                                                de Proyección &nbsp;<i class="fas fa-file-pdf"></i></a>
                                                        </p>
                                                        @if ($inf->estado == 'ejecutado')
                                                            <p>
                                                                <a onclick="mostrarPDF('{{ route('reportePDF.informe_descargo_material', $inf->id_informe) }}')"
                                                                    target="_blank"
                                                                    class='text-white btn btn-danger btn-icon w-75'>Informe de
                                                                    Ejecución de Proyecto&nbsp;<i
                                                                        class="fas fa-file-pdf"></i></a>
                                                            </p>
                                                            <p>
                                                                <a onclick="mostrarPDF('{{ route('reportePDF.acta_final', $inf->id_solicitud) }}')"
                                                                    target="_blank"
                                                                    class='text-white btn btn-danger btn-icon w-75'>Acta de
                                                                    Ejecución de Proyecto&nbsp;<i
                                                                        class="fas fa-file-pdf"></i></a>
                                                            </p>
                                                        @endif
                                                    @endcan
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @can('jefe-red')
                                    @if ($inf->estado == 'solicitud de firma')
                                        <a href='{{ route('informes.firmar', $inf->id_informe) }}'
                                            class='btn btn-success btn-icon ' title="firmar"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <a href='{{ route('informes.devolver_asignado', $inf->id_informe) }}'
                                            class='btn btn-danger btn-icon ' title="devolver informe">
                                            <i class="fas fa-undo-alt"></i>
                                        </a>
                                    @endif
                                @endcan

                            </div>

                        </td>

                    </tr>
                    @php
                        $num++;
                    @endphp
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>NRO</th>
                    <th>ZONA O BARRIO</th>
                    <th>NOMBRE SOLICITANTE</th>
                    <th>FECHA DE EJECUCION PROGRAMADA</th>
                    @can('jefe-red')
                        <th>INSPECTOR</th>
                    @endcan
                    @can('users.index')
                        <td>
                            {{ strtoupper($inf->nombre_inspector) }}
                        </td>
                    @endcan
                    <th>ESTADO</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
        <div class="card-body">
            <h4>Estados</h4>
            <div>
                <p><b>Pendiente: </b> El usuario hizo su solicitud </p>
                <p><b>Aprobado: </b> La solicitud de ampliación fue aprobada por el jefe de red</p>
                <p><b>Aplazado: </b> La solicitud de ampliación fue aplazada por el jefe de red </p>
                <p><b>Asignado: </b> El jefe de red asignó a un inspector para que realice la inspeccion en la ubicación
                    solicitada</p>
                <p><b>Inspeccionado: </b> El inspector ya realizo el informe de inspeccion y solicita la revision del jefe
                    de red
                </p>
                <p><b>Autorizado: </b> El inspector ya puede realizar el peticion de materiales y mano de obra para la
                    ampliación</p>
                <p><b>Solicitud de firma: </b> El inspector ya realizó la petición de materiales y mano de obra y esta a la
                    espera de la aprobación del jefe de red</p>
                <p><b>Firmado: </b> El jefe de red ya firmó la petición de materiales y mano de obra, la ampliación es
                    derivada al proyectista</p>
                <p><b>Ejecutado: </b> La ampliación ya fue ejecutada</p>
                <p><b>Observado: </b> El jefe de red observó errores en el informe de inspección, pedido de materia o mano
                    de obra</p>
            </div>
        </div>

        <!-- Materiales -->
        <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" style="overflow:hidden;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">LISTA DE MATERIALES</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div style="height: 300px; overflow-y: scroll;">
                        <div class="modal-body" id="contenido">
                            {{--  --}}
                        </div>
                    </div>
                    <div id="estado_informe"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mano de Obra -->
        <div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel"
            aria-hidden="true" id="modal_mano_obra" style="overflow:hidden;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ACTIVIDADES DE MANO DE OBRA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div style="height: 300px; overflow-y: scroll;">
                        <div class="modal-body" id="contenido_mano_obra">
                            {{--  --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vista de mapa -->
    <div id="contenedor-mapa" style="display: none">
        <input type="hidden" id="obtenerAmpliaciones">

        <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i
                class="fas fa-arrow-circle-left"></i>&nbsp;Volver</button>
        <div class="col-md-12">
            <div id="map">
            </div>
        </div>
    </div>
    <!-- Vista de archivos -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Archivos de la Solicitud</h5>
                </div>
                <div class="modal-body table table-bordered table-hover dataTable table-responsive w-100"
                    id="contenedor-tabla">
                    <h1 id="tiempo_carga">Cargando......</h1>
                    <p id="sin_datos"></p>
                    <table id="datos_modal_archivos" class="datos_modal_archivos table table-bordered datatable"
                        width="100%" height="100%">
                    </table>
                </div>
                <div class="mostrar_guardar" style="display: none">
                    <label><input type="checkbox" id="myCheck"> Agregar nuevo registro</label>
                </div>
                <form method="POST" action="{{ route('archivos_solicitud.store') }}" id="ingresar_nuevo"
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off">
                    @csrf
                    <input type="hidden" name="solicitud_id" id="solicitud_id">
                    <div class="card-body">
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" lang="es" accept=".pdf"
                                        class=" custom-file-input @error('archivo_solicitud') is-invalid @enderror"
                                        onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                        id="archivo_solicitud" name="archivo_solicitud">
                                    <label class="custom-file-label" for="archivo_solicitud">Subir Solicitud
                                        Escaneada</label>
                                </div>
                            </div>
                            @error('archivo_solicitud')
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <label for="descripcion">Descripcion</label>
                                <textarea name="descripcion" id="descripcion" cols="60" rows="5" style="text-transform:uppercase;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="float: right;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-plus"></i> Agregar</button>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i>
                        Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Eliminacion de Archivos -->
    <div class="modal fade" id="eliminacion_archivos" tabindex="-1" role="dialog" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form method="POST" action="{{ route('archivos_solicitud.destroy') }}" id="ingresar_nuevo"
                    autocomplete="off" enctype="multipart/form-data" role="form" class="create">
                    @csrf
                    {{-- @method('delete') --}}
                    <input type="hidden" name="id_eliminar" id="id_eliminar">
                    <div class="card-body" style="text-align: center">
                        <h2><i class="fas fa-exclamation-triangle" style="color: yellow; font-size: 100px;"></i> <br>
                            ¿Está seguro?</h2>
                        <h3> ¡No podrás revertir esto!</h3>
                        <p id="id_para_eliminar" style="display: none"> <i class="fas fa-exclamation-triangle"></i> </p>
                    </div>
                    <div class="card-footer" style="text-align: center;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-trash"></i> Eliminar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i>
                            Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vista Archivos de la Solicitud -->
    <div class="modal fade" id='show_vista_archivos' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <!-- id='show_vista_archivos' -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-heeder">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> --}}
                </div>
                <h3 style="text-align: center">Cargando......</h3>
                <p id="impresion_archivos_solicitud" style="display: none"></p>
                <div class="contenedor_impresion" id="contenedor_impresion">
                    <iframe id="iframe" name="myIframe" frameborder="5" width="500" height="300"
                        style="display: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- Solicitar Firmado -->
    <div class="modal fade" id="solicitando_firma" tabindex="-1" role="dialog" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form method="POST" action="{{ route('informes.esperar_aprobacion') }}" enctype="multipart/form-data"
                    role="form" class="create">
                    @csrf
                    {{-- @method('delete') --}}
                    <input type="hidden" name="id_informe" id="id_informe">
                    <div class="card-body" style="text-align: center">
                        <i class="fas fa-exclamation-triangle" style="color: yellow; font-size: 100px;"></i>
                        <h3 id="mensaje_informacion_estado"></h3>
                        <p id="id_para_eliminar" style="display: none"> <i class="fas fa-exclamation-triangle"></i> </p>
                    </div>
                    <div class="card-footer" style="text-align: center; display: none;" id="botones_estado">
                        <button type="submit" class="btn btn-warning"><i class="fas fa-check-double"></i>
                            Solicitar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i>
                            Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
    @if (session('modificado') == 'ok')
        <script>
            Swal.fire(
                'Modificado!',
                'Su registro ha sido Corregido.',
                'success'
            )
        </script>
    @endif
    @if (session('modificado') == 'error')
        <script>
            Swal.fire(
                'Error!',
                'Error al realizar la peticion.',
                'danger'
            )
        </script>
    @endif
    @if (session('actualizado') == 'ok')
        <script>
            Swal.fire(
                'Modificado!',
                'Se modifico la fecha de Ejecucion.',
                'success'
            )
        </script>
    @endif
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}";
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script>
        $('.select2').select2();
        $.fn.modal.Constructor.prototype._enforceFocus = function() {};

        function llamar(url, opcion) {
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200) {
                    // console.log({ajax});
                    if (opcion == 'material')
                        document.getElementById("contenido").innerHTML = ajax.responseText;
                    else {
                        document.getElementById("contenido_mano_obra").innerHTML = ajax.responseText;
                    }
                }
            };
            ajax.open("GET", url, true);
            ajax.send();
        }

        function visualizarMapa(lat, long, ruta) {
            mostrarTabla(true);
            document.querySelector('#obtenerAmpliaciones').value = ruta;
            ruta == null ? initMap(lat, long, 'mostrar') : initMap(lat, long);
        }
    </script>
    <script src="{{ asset('js/archivos_solicitud.js') }}"></script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>
    <script src="{{ asset('js/informes.js') }}"></script>
    <script>
        const ruta_update_costos = "{{ route('informes.update_costos') }}"
        const ruta = "{{ route('archivos_solicitud.show') }}";
        const ruta2 = "{{ route('archivos_solicitud.mostrar') }}";
        const ruta3 = "{{ route('solicitud.escaneada') }}";
        const ruta4 = "{{ route('informes.verificacion_estado') }}"
        const ruta_asset = "{{ asset('archivos_solicitudes/') }}";
        const ruta_asset2 = "{{ asset('solicitudes/') }}";
    </script>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
    <style>
        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 1.6cm;
            }
        }
    </style>
@stop
