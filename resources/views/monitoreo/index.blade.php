@extends('adminlte::page')

@section('title', 'Monitoreo y Proyectos')
@php
    $dias = ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'];
    $fecha = '12-05-2021';
    $dia = $dias[date('N', strtotime($fecha))];
    $n = 0;
@endphp
@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>
    <h1>ELAPAS - LISTADO DE SOLICITUDES
    </h1>




@stop
@section('content')

    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th>Nro</th>
                    <th>Nombre solicitante</th>
                    <th>Fecha Solicitud</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th>Estado</th>
                    <th width="120">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($solicitudall as $sol)
                    @php
                        $n++;
                    @endphp

                    <tr>
                        <td>{{ 'S-' . $sol->solicitud_id }}</td>
                        <td>{{ $sol->nombre_sol }}</td>
                        <td>{{ $sol->fecha_sol }}</td>
                        <td>{{ $sol->zona_sol }}</td>
                        <td>{{ $sol->calle_sol }}</td>
                        <td align="center"><span
                                class="badge badge-primary">{{ $sol->estado_in == null ? strtoupper($sol->estado_sol) : strtoupper($sol->estado_in) }}</span>
                        </td>
                        <td width="200px">
                            <div class="btn-group">
                                <!-- Boton para mostrar los Archivos de las solicitudes -->
                                <button class="d-inline btn btn-success btn-icon" id="archivos_solicitud"
                                    data-id="{{ $sol->solicitud_id }}" title="Archivos" data-toggle="modal"
                                    data-target="#myModal"><i class="fas fa-plus"></i></button>
                                <!-- Vista de la solicitud -->
                                <button type='button' id='archivo_escaneado_informes' data-id="{{ $sol->solicitud_id }}"
                                    class="text-white btn btn-danger d-line btn-icon" title="Solicitud Escaneada"><i
                                        class="fa fa-file-pdf"></i></button>
                                <!-- Para ver el reporte de los historicos -->
                                <a onclick="mostrarPDF('{{ route('solicitud.reporte_historicos', $sol->solicitud_id) }}')"
                                    class="btn btn-info btn-icon" title="Reporte de Avance"><i
                                        class="fas fa-file-invoice"></i></a>
                                <a type="button" class="d-inline btn btn-warning btn-icon" title="Visualizar"
                                    data-toggle="modal" data-target=".bd-example-modal-lg"
                                    onclick="visualizarMapa({{ $sol->x_aprox }},{{ $sol->y_aprox }}, {{ $sol->solicitud_id }})"
                                    id="btn_mostrar_mapa">
                                    <i class="fas fa-eye"></i></a>

                                {{-- <a type="button" onclick="mostrarPDF('{{ route('solicitud.escaneada', $sol->solicitud_id) }}')"
                                    title="Mostrar Solicitud" class="text-white btn btn-danger d-line btn-icon">
                                    <i class="fa fa-file-pdf"></i>
                                </a> --}}

                                @can('Monitor')
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#exampleModal{{ $n }}">
                                        <i class="fa fa-file"></i>
                                    </button>

                                    <div class="modal fade" id="exampleModal{{ $n }}" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Informes</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" style="text-align: center;">
                                                    @if (
                                                        $sol->estado_in == 'inspeccionado' ||
                                                            $sol->estado_in == 'autorizado' ||
                                                            $sol->estado_in == 'firmado' ||
                                                            $sol->estado_in == 'ejecutandose' ||
                                                            $sol->estado_in == 'ejecutado')
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('descargarPDF.informe', $sol->informe_id) }}')"
                                                                target="_blank"
                                                                class='text-white btn btn-danger btn-icon w-75'>Informe de
                                                                Inspección <i class="fas fa-file-pdf"></i></a>
                                                        </p>
                                                    @endif
                                                    @if ($sol->estado_in == 'firmado' || $sol->estado_in == 'ejecutandose' || $sol->estado_in == 'ejecutado')
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('pedidoPDF.informe', $sol->informe_id) }}')"
                                                                target="_blank"
                                                                class='text-white btn btn-danger btn-icon w-75'>Pedido de
                                                                Material
                                                                <i class="fas fa-file-pdf"></i></a>

                                                        </p>
                                                    @endif


                                                    @if ($sol->estado_in == 'ejecutado')
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('reportePDF.informe_descargo_material', $sol->informe_id) }}')"
                                                                target="_blank"
                                                                class='text-white btn btn-danger btn-icon w-75'>Informe de
                                                                Ejecución
                                                                de Proyecto<i class="fas fa-file-pdf"></i></a>

                                                        </p>
                                                    @endif
                                                    @if ($sol->estado_sol == 'aplazado')
                                                        <p>
                                                            <a type="button"
                                                                onclick="mostrarPDF('{{ route('solicitud.PDFrechazado', $sol->solicitud_id) }}')"
                                                                target='_blank'
                                                                class='text-white d-inline btn btn-danger btn-icon'
                                                                title="Reporte Rechazado"><i class="fas fa-file"></i> Informe
                                                                Solicitud Aplazado</a>
                                                        </p>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endcan
                                @can('Proyectista')
                                    <button type="button" class="d-inline btn btn-primary btn-icon" title="Informes"
                                        data-toggle="modal" data-target="#exampleModal{{ $n }}">
                                        <i class="fa fa-file"></i>
                                    </button>
                                    <a type="button" href="{{ $sol->ubicacion }}" target="_blank"
                                        class="d-inline btn btn-success btn-icon" title="Ubicación Geografica">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </a>

                                    <div class="modal fade" id="exampleModal{{ $n }}" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Informes</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div style="text-align: center;">
                                                        <p>
                                                            <a onclick="mostrarPDF('{{ route('reportePDF.informe_material', $sol->informe_id) }}')"
                                                                target="_blank"
                                                                class=' text-white btn btn-danger btn-icon w-75'>Informe
                                                                Ampliacion
                                                                <i class="fas fa-file-pdf"></i></a>
                                                        </p>
                                                        @if ($sol->estado_in == 'ejecutandose' || $sol->estado_in == 'ejecutado')
                                                            <p>
                                                                <a onclick="mostrarPDF('{{ route('descargarPDF.proyecto', $sol->informe_id) }}')"
                                                                    target="_blank"
                                                                    class=" text-white btn btn-danger btn-icon w-75"
                                                                    title="Informe Proyeccion">
                                                                    Informe Proyección<i class="fas fa-file-pdf"></i></a>
                                                            </p>
                                                        @endif
                                                        {{-- @if ($inf->fecha_ejecutada != null)
                                                <p>
                                                    <a href='{{route('reportePDF.informe_descargo_material',$inf->id_informe)}}' target="_blank"
                                                        class='btn btn-danger btn-icon w-75'>Informe Descargo Material<i class="fas fa-box-open"></i></a>

                                                </p>


                                            @endif --}}
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($sol->estado_in == 'firmado')
                                        <a href="{{ route('informes.aprobar_proyecto', $sol->informe_id) }}"
                                            class="btn btn-success btn-icon" title="Aprobar Proyecto">
                                            <i class="fas fa-check"></i></a>
                                    @endif
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
            <tfoot>
                <tr>
                    <th>Nro</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
        <div class="card-body">
            <h4>Estados</h4>
            <div>
                <p><b>Pendiente: </b> El usuario hizo su solicitud </p>
                <p><b>Aprobado: </b> La solicitud de ampliación fue aprobada por el jefe de red</p>
                <p><b>Aplazado: </b> La solicitud de ampliación fue aplazada por el jefe de red </p>
                <p><b>Asignado: </b> El jefe de red asignó a un inspector para que realice la inspeccion en la ubicación
                    solicitada</p>
                <p><b>Inspeccionado: </b> El inspector ya realizo el informe de inspeccion y solicita la revision del jefe
                    de red
                </p>
                <p><b>Autorizado: </b> El inspector ya puede realizar el peticion de materiales y mano de obra para la
                    ampliación</p>
                <p><b>Solicitud de firma: </b> El inspector ya realizó la petición de materiales y mano de obra y esta a la
                    espera de la aprobación del jefe de red</p>
                <p><b>Firmado: </b> El jefe de red ya firmó la petición de materiales y mano de obra, la ampliación es
                    derivada al proyectista</p>
                <p><b>Ejecutado: </b> La ampliación ya fue ejecutada</p>
                <p><b>Observado: </b> El jefe de red observó errores en el informe de inspección, pedido de materia o mano
                    de obra</p>
            </div>
        </div>

    </div>

    {{-- Visualizar Mapa --}}
    <div id="contenedor-mapa" style="display: none">
        <input type="hidden" id="obtenerAmpliaciones">
        <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i> Volver
        </button>
        <div class="col-md-12">
            <div id="map">
            </div>


        </div>
    </div>
    {{-- Fin Visualizar Mapa --}}
    <!-- Vista de archivos -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle"
        aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header"></div>
                <div class="modal-body table table-bordered table-hover dataTable table-responsive w-100"
                    id="contenedor-tabla">
                    <h1 id="tiempo_carga">Cargando......</h1>
                    <p id="sin_datos"></p>
                    <table id="datos_modal_archivos" class="datos_modal_archivos table table-bordered datatable"
                        width="100%" height="100%">
                    </table>
                </div>
                <div class="mostrar_guardar" style="display: none">
                    <label><input type="checkbox" id="myCheck"> Agregar nuevo registro</label>
                </div>
                <form method="POST" action="{{ route('archivos_solicitud.store') }}" id="ingresar_nuevo"
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off">
                    @csrf
                    <input type="hidden" name="solicitud_id" id="solicitud_id">
                    <div class="card-body">
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" lang="es" accept=".pdf"
                                        class=" custom-file-input @error('archivo_solicitud') is-invalid @enderror"
                                        onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                        id="archivo_solicitud" name="archivo_solicitud">
                                    <label class="custom-file-label" for="archivo_solicitud">Subir Solicitud
                                        Escaneada</label>
                                </div>
                            </div>
                            @error('archivo_solicitud')
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <label for="descripcion">Descripcion</label>
                                <textarea name="descripcion" id="descripcion" cols="60" rows="5" style="text-transform:uppercase;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="float: right;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-plus"></i> Agregar</button>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i>
                        Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Eliminacion de Archivos -->
    <div class="modal fade" id="eliminacion_archivos" tabindex="-1" role="dialog" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form method="POST" action="{{ route('archivos_solicitud.destroy') }}" id="ingresar_nuevo"
                    autocomplete="off" enctype="multipart/form-data" role="form" class="create">
                    @csrf
                    {{-- @method('delete') --}}
                    <input type="hidden" name="id_eliminar" id="id_eliminar">
                    <div class="card-body" style="text-align: center">
                        <h2><i class="fas fa-exclamation-triangle" style="color: yellow; font-size: 100px;"></i> <br>
                            ¿Está seguro?</h2>
                        <h3> ¡No podrás revertir esto!</h3>
                        <p id="id_para_eliminar" style="display: none"> <i class="fas fa-exclamation-triangle"></i> </p>
                    </div>
                    <div class="card-footer" style="text-align: center;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-trash"></i> Eliminar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i>
                            Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vista Archivos de la Solicitud -->
    <div class="modal fade" id='show_vista_archivos' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <!-- id='show_vista_archivos' -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-heeder">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> --}}
                </div>
                <h3 style="text-align: center">Cargando......</h3>
                <p id="impresion_archivos_solicitud" style="display: none"></p>
                <div class="contenedor_impresion" id="contenedor_impresion">
                    <iframe id="iframe" name="myIframe" frameborder="5" width="500" height="300"
                        style="display: none"></iframe>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}"
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>
    <script src="{{ asset('js/informes.js') }}"></script>
    <script src="{{ asset('js/archivos_solicitud.js') }}"></script>
    <script>
        function visualizarMapa(lat, long, ruta) {
            mostrarTabla(true);
            console.log('solicitud/' + ruta + '/obtener_ampliacion')
            document.querySelector('#obtenerAmpliaciones').value = 'solicitud/' + ruta + '/obtener_ampliacion';
            ruta == null ? initMap(lat, long, 'mostrar') : initMap(lat, long);
        }
    </script>
    <script>
        const ruta = "{{ route('archivos_solicitud.show') }}";
        const ruta2 = "{{ route('archivos_solicitud.mostrar') }}";
        const ruta3 = "{{ route('solicitud.escaneada') }}";
        const ruta_asset = "{{ asset('archivos_solicitudes/') }}";
        const ruta_asset2 = "{{ asset('solicitudes/') }}";
    </script>

@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
@stop
