@php
    setlocale(LC_TIME, 'spanish');
    $fecha = strtotime($solicitud->fecha_sol);
    $anio = date('Y', $fecha);
    $mes = date('m', $fecha);
    $dia = date('d', $fecha);
    $meses = [
        '1' => 'Enero',
        '2' => 'Febrero',
        '3' => 'Marzo',
        '4' => 'Abril',
        '5' => 'Mayo',
        '6' => 'Junio',
        '7' => 'Julio',
        '8' => 'Agosto',
        '9' => 'Septiembre',
        '10' => 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre',
    ];
    setlocale(LC_TIME, 'spanish');

    $Mes_ = $meses[intval($mes)];
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INFORME RECHAZADO</title>
</head>

<style>
    p {
        padding: 10px;
        font-size: 16px;
    }

    th {
        padding: 10px;
    }

    table,
    td {
        border: 1px solid black;
        width: 100%;
        padding: 5px;
    }

    #table1 {
        border-collapse: collapse;
        border-spacing: 10px;
    }

    .centrar {
        text-align: center;
    }

    div.firma {
        padding-left: 60px;
    }

    div.firma2 {
        text-align: right;
    }
</style>

<body>
    <x-header-informe />
    <br><br>
    <h3 class="centrar">INFORME DE SOLICITUD PENDIENTE</h3>
    <div align="center">
        <img src="{{ asset('rechazadas/' . $solicitud->sol_rechazada) }}" width="680px" height="350px" alt="">
    </div>
    <p>Estimado usuario <strong>{{ $solicitud->nombre_sol }}</strong>, en respuesta a
        su solicitud realiazada el dia {{ $dia }} de {{ $Mes_ }} de {{ $anio }} le informamos
        que lamentablemente no se no se podrá realiazar la instalación de agua potable en su domicilio ubicado en la
        calle <strong>{{ $solicitud->calle_sol }}</strong> zona <strong>{{ $solicitud->zona_sol }}</strong> debido al
        siguiente mottivo: <br><br> {{ $solicitud->observaciones }}
    </p>



    <br><br>
    <div class="firma">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        ..............................................
    </div>
    <div class="firma">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        FIRMA JEFE DE RED
    </div>
</body>

</html>
