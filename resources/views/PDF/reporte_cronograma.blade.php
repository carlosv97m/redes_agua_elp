<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Cronograma</title>
<style>
    body{
        font-family:verdana;
    }
    table{
        border-collapse:collapse;
    }
    .container{
        /* margin:30px 50px; */
        text-align:center;
    }
    .centrar{
        text-align: center;
    }
</style>
</head>
@php
    $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
    $fecha_inicio=date('d-m-Y', (strtotime($fecha_inicio)));
    $fecha_fin=date('d-m-Y', (strtotime($fecha_fin)));
@endphp
<body>
    <x-header/>
    <br><br><br><br>
    <div class="container">
    <table border="1" width="100%">
    <tr>
        <th class="centrar" colspan="10">Cronograma de Ampliacion de red Matriz de <strong>{{$fecha_inicio}}</strong>&nbsp;a&nbsp;<strong>{{$fecha_fin}}</strong></th>
    </tr>
    <tr>
        <th>N°</th>
        <th>BARRIO</th>
        <th>Nombre Solicitante</th>
        <th>Celular</th>
        <th>L</th>
        <th>M</th>
        <th>X</th>
        <th>J</th>
        <th>V</th>
        <th>INSPECTOR</th>
    </tr>
    @foreach ($cronogramas as $cronograma)
        <tr>
            <td>S-{{$cronograma->id_solicitud}}</td>
            <td>{{$cronograma->zona}}</td>
            <td>{{$cronograma->nombre_sol}}</td>
            <td>{{$cronograma->celular}}</td>
            @if($dias[(date('N', strtotime($cronograma->fecha_inspe)))]=="lunes")
                <td>{{ date('H:i:s', strtotime($cronograma->fecha_inspe)) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @elseif($dias[(date('N', strtotime($cronograma->fecha_inspe)))]=="martes")
                <td></td>
                <td>{{ date('H:i:s', strtotime($cronograma->fecha_inspe)) }}</td>
                <td></td>
                <td></td>
                <td></td>
            @elseif($dias[(date('N', strtotime($cronograma->fecha_inspe)))]=="miércoles")
                <td></td>
                <td></td>
                <td>{{ date('H:i:s', strtotime($cronograma->fecha_inspe)) }}</td>
                <td></td>
                <td></td>
            @elseif ($dias[(date('N', strtotime($cronograma->fecha_inspe)))]=="jueves")
                <td></td>
                <td></td>
                <td></td>
                <td>{{ date('H:i:s', strtotime($cronograma->fecha_inspe)) }}</td>
                <td></td>
            @elseif ($dias[(date('N', strtotime($cronograma->fecha_inspe)))]=="viernes")
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ date('H:i:s', strtotime($cronograma->fecha_inspe)) }}</td>
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
            <td>{{$cronograma->name}}</td>
        </tr>
    @endforeach
    </table>
    </div>
</body>
</html>
