@php
    setlocale(LC_TIME, 'es_ES');
    $fecha = strtotime($solicitud->fecha_sol);
    $anio = date('Y', $fecha);
    $mes = date('M', $fecha);
    $dia = date('d', $fecha);
    setlocale(LC_TIME, 'es_ES');

    $Mes_ = strftime('%B', strtotime($mes));
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comprobante de Solicitud</title>
</head>

<style>
    p {
        padding: 10px;
        font-size: 16px;
    }

    .centrar {
        text-align: center;
    }

    b {
        text-transform: uppercase;
        font-size: small;
    }

    .izquierdo {
        float: left;
        text-align: center !important;
        display: flex;
        font-size: 10px;
    }

    .derecha {
        float: right;
        text-align: right !important;
        font-size: 10px;
    }
</style>

<body style="border-style: double; border-width: 5px 5px">
    <div class="header">
        <div class="izquierdo">
            <img src="{{ asset('images/pedido.png') }}" style="width:100px; height:50px" alt="">
            <b>Sistema informático de ampliación de redes de agua</b>
        </div>

        <div class="derecha">
            <b>Fecha de Impresión: </b><br>{{ date('d-m-Y H:i:s') }}
        </div>
    </div>
    <br><br>
    <h2 class="centrar">Solicitud de ampliación de red de agua, codigo: <strong>S-{{ $solicitud->id }}</strong></h2>
    <p class="centrar" style="margin:5px 20px 5px 40px ">Nombre Solicitante:
        <strong>{{ $solicitud->nombre_sol }}</strong></p>
    <p class="centrar">Fecha de Solicitud: <strong>{{ date('d-m-Y H:i:s', strtotime($solicitud->created_at)) }}</strong>
    </p>
    <p class="centrar">Celular del Solicitante: <strong>{{ $solicitud->celular_sol }}</strong></p>
</body>

</html>
