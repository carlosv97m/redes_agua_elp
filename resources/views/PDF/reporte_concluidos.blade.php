@extends('adminlte::page')

@section('title', 'Informes')
@section('css')
<style>
    table,td,th{
        border: 1px solid black;
        border-collapse:collapse;
        font-size: 18px;

    }
    .head = {
        background-color: #0080ff;
        background: #004080;
    }
    .centrar{
        text-align:center;
    }
    .tamanio{
        font-size: 15px;
    }
</style>
@stop

@section('content')
    <x-header/>
    <br><br><br><br>
    <h1 class="centrar">ELAPAS - Reporte de Solicitudes de Ampliacion Concluidos</h1>
    <h2 align="center">Fechas <strong>{{ $fecha_inicio }}</strong>&nbsp;a&nbsp;<strong>{{ $fecha_fin }}</strong></h2>
    {{-- <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla"> --}}
        <table width="100%">
            <thead>
                <tr>
                    <th class="head centrar">#</th>
                    <th class="head centrar">NOMBRE SOLICITANTE</th>
                    <th class="head centrar">FECHA DE<br> SOLICITUD</th>
                    <th class="head centrar">FECHA DE<br> INSPECCION</th>
                    <th class="head centrar">CALLE</th>
                    <th class="head centrar">ZONA</th>
                    <th class="head centrar">FECHA PROGRAMADA <br> DE EJECUCION</th>
                    <th class="head centrar">FECHA FIN DE<br> EJECUCION</th>
                    <th class="head centrar">Inspector</th>
                    <th class="head centrar">ESTADO</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($informe as $inf)
                    <tr>
                        <td class="centrar tamanio"> {{ $loop->index + 1 }} </td>
                        <td class="centrar tamanio"> {{ $inf->nombre_sol }} </td>
                        <td class="centrar tamanio"> {{ date('d-m-Y',strtotime($inf->fecha_sol)) }} </td>
                        <td class="centrar tamanio"> {{ date('d-m-Y H:i:s',strtotime($inf->fecha_hora_in)) }} </td>
                        <td class="centrar tamanio"> {{ $inf->calle_sol }} </td>
                        <td class="centrar tamanio"> {{ $inf->zona_sol }} </td>
                        <td class="centrar tamanio"> {{ date('d-m-Y',strtotime($inf->fecha_progrmada)) }} </td>
                        <td class="centrar tamanio"> {{ date('d-m-Y',strtotime($inf->fecha_ejecutada)) }} </td>
                        <td class="centrar tamanio"> {{ strtoupper($inf->name) }} </td>
                        <td class="centrar tamanio"> {{ strtoupper($inf->estado_in) }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    {{-- </div> --}}
@stop

@section('footer')
    <strong>{{ date('Y') }} || ELAPAS - SISTEMA DE AMPLIACION DE REDES DE AGUA </strong>
@stop
