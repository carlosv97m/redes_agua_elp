@extends('adminlte::page')

@section('title', 'Informes')
@section('css')
<style>
    table,td,th{
        border: 1px solid black;
        border-collapse:collapse;
        font-size: 28px;

    }
    .head = {
        background-color: #0080ff;
        background: #004080;
    }
    .centrar{
        text-align:center;
    }
    .tamanio{
        font-size: 15px;
    }
</style>
@stop

@section('content')
    <x-header/>
    <br><br><br><br>
    <h1 class="centrar">ELAPAS - Reporte de Avance de la Solicitud</h1>
    <table width="100%">
        <thead>
            <tr>
                <th class="head centrar">Estado</th>
                <th class="head centrar">Responsable</th>
                <th class="head centrar">Fecha de avance</th>
                <th class="head centrar">Días transcurridos</th>
                <th class="head centrar">Derivado A</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($solicitud as $historical)
                @if ($historical->estado == 'solicitado')
                    <tr>
                        <td class="centrar tamanio">{{ strtoupper($historical->estado) }}</td>
                        <td class="centrar tamanio">{{ strtoupper($historical->tipo_user) }}&nbsp;-&nbsp;{{ strtoupper(($historical->name)) }}</td>
                        <td class="centrar tamanio" colspan="2">{{ date('d-m-Y H:i:s', strtotime($historical->created_at)) }}</td>
                        <td class="centrar tamanio">
                            Derivado a Jefe de Red de Agua
                        </td>
                    </tr>
                @else
                    <tr>
                        <td class="centrar tamanio">{{ strtoupper($historical->estado) }}</td>
                        <td class="centrar tamanio">{{ strtoupper($historical->tipo_user) }}&nbsp;-&nbsp;{{ strtoupper(($historical->name)) }}</td>
                        <td class="centrar tamanio">{{ date('d-m-Y H:i:s', strtotime($historical->created_at)) }}</td>
                        <td class="centrar tamanio">{{ $historical->dias_transcurridos }}</td>
                        <td class="centrar tamanio">
                            @php
                                if($historical->estado === 'aprobado'){
                                    echo "Derivado a Jefe de Red de Agua";
                                } elseif($historical->estado == 'asignado'){
                                    echo "Derivado a Inspector";
                                } elseif($historical->estado == 'autorizado'){
                                    echo "Derivado a Inspector";
                                } elseif($historical->estado == 'inspeccionado'){
                                    echo "Derivado a Jefe de Red de Agua";
                                } elseif($historical->estado == 'solicitud de firma'){
                                    echo "Derivado a Jefe de Red de Agua";
                                } elseif($historical->estado == 'firmado'){
                                    echo "Derivado a Proyectista";
                                } elseif($historical->estado == 'ejecutandose'){
                                    echo "Derivado a Inspector";
                                } elseif($historical->estado == 'ejecutado'){
                                    echo "Proyecto Terminado";
                                }
                            @endphp
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

@stop

@section('footer')
    <strong>{{ date('Y') }} || ELAPAS - SISTEMA DE AMPLIACION DE REDES DE AGUA </strong>
@stop
