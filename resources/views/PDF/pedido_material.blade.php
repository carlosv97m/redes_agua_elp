@php
    setlocale(LC_TIME, 'spanish');
    $fecha = strtotime(date('d-m-Y'));
    $anio = date('Y', $fecha);
    $mes = date('m', $fecha);
    $dia = date('d', $fecha);
    setlocale(LC_TIME, 'spanish');
    $jefe_red = 'Rene Iglesias';
    $Mes_ = strftime('%B', strtotime($mes));
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pedido de material</title>

    <style>
        .letra {
            font-size: 10px;
        }

        .centrar {
            text-align: center;
        }

        .derecha {
            text-align: right;
        }

        .izquierda {
            width: 120px;
            text-align: left;
        }

        .contenido {
            height: 200px;
        }

        .sinBorde {
            border-bottom: 1px solid #fff
        }

        u {
            border-bottom: 1px dotted #000;
            text-decoration: none;
        }

        .img {
            width: 150px;
            height: 100px;
        }
    </style>
</head>

<body>
    @php
        $pedido = 10000 + $informe->id;
    @endphp
    <table width="100%">
        <tr>
            <td class="izquierda"><img class="img"
                    src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('images/pedido.png'))) }}">
            </td>
            <td width="100px" class="letra centrar">EMPRESA LOCAL DE AGUA POATBALE Y ALCANTARILLADO SUCRE <br> AV. JAIME MENDOZA N 866</td>
            {{-- <td class="letra derecha">{{ '0' . $pedido }}</td> --}}
        </tr>
    </table>
    <h3 class="centrar">PEDIDO DE MATERIALES A ALMACENES<br>
       REDES DE AGUA S-{{ $informe->solicitud->id }}
    </h3>
    <div class="container">
        <table border="1" style="float:left; border-collapse:collapse; width:100%">
            <thead>
                <tr style="background-color: lightgray">
                    <th colspan="4" class="centrar">INFORMACION DE SOLICITUD</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>PARA USO:</th>
                    <td>AMPLIACION DE TUBERIA MATRIZ EN {{strtoupper($informe->solicitud->zona_sol)}}</td>
                    <th>FECHA SOLICITUD</th>
                    <td>{{date('d-m-Y')}}</td>
                </tr>

            </tbody>
        </table>
        <br>
        <br>
        <br>


        <table border="1" style=" margin-top:20px;border-collapse:collapse; width:100%;">
            <thead>
                <tr style="background-color: lightgray">
                    <th colspan="4" align="center">DETALLE MATERIALES</th>
                </tr>
            </thead>
            <tr>
                <th align="center" style="width:100px;">Cantidad <br> Solicitada</th>
                <th align="center" style="width:100px;">Unidad de <br> medida</th>
                <th align="center">DESCRIPCION</th>
                <th align="center" style="width:100px;">Codigo</th>
            </tr>
            @foreach ($materiales as $material)
                <tr>

                    <td align="center">{{ $material->cantidad }}</td>
                    <td align="center">{{ $material->u_medida }}</td>
                    <td align="center">{{ $material->nombre_material }}</td>
                    <td align="center">{{ $material->codigo }}</td>

                </tr>
            @endforeach
            <tr>
                <td style="height:15%; border-top:1px solid #fff"></td>
                <td style="height:15%; border-top:1px solid #fff"></td>
                <td style="height:15%; border-top:1px solid #fff"></td>
                <td style="height:15%; border-top:1px solid #fff"></td>

            </tr>
        </table>
        {{-- <table border="1" style="border-collapse:collapse; width:100%;">
            <tr style="height:90px; margin:0px;">
                <td width="200px">Pedido Por:
                    <br>
                    <strong> {{ $inspector->name }} </strong>
                    <br>
                    <img width="120" height="60"
                        src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('images/' . $inspector->name . '.png'))) }}">
                </td>

                <td>Autorizado
                    Por:<br><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $jefe_r->name }}
                        <br>&nbsp;&nbsp;&nbsp;
                        @if ($inspector->estado == 'firmado' || $inspector->estado == 'ejecutado' || $informe->estado_in == 'ejecutandose')
                            <img width="120" height="60"
                                src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('images/' . $jefe_red . '.png'))) }}">
                        @endif
                </td>
                <td>Vo.Bo. <br><br><br><br></td>
                <td>Vo.Bo. <br><br><br><br></td>
            </tr>
            <tr align="center">
                <td style="">Nombre Firma</td>
                <td style="">Jefe de Sección</td>
                <td style="">Gerencia Área</td>
                <td style="">Gerencia Administrativa</td>
            </tr>
        </table> --}}
        <table border="1" style="border-collapse:collapse; width:100%;">
            <tr style="height:90px; margin:0px; align-items: center">
                <td width="200px">Pedido Por:
                    <br><strong align="center">
                        <br>
                        <br>
                        <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $inspector->name }}
                    </strong>
                </td>

                <td >Autorizado
                    Por:<br><strong align="center">
                        <br>
                        <br>
                        <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $jefe_r->name }}
                    </strong>
                </td>
                <td>Vo.Bo. <br><br><br><br></td>
                <td>Vo.Bo. <br><br><br><br></td>
            </tr>
            <tr align="center">
                <td style="">Firma Nombre</td>
                <td style="">Jefe de Sección</td>
                <td style="">Gerencia Área</td>
                <td style="">Gerencia Administrativa</td>
            </tr>
        </table>

    </div>



</body>

</html>
