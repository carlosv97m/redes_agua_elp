<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ampliacion tuberia matriz</title>
</head>
@php
    setlocale(LC_TIME, 'spanish');
    $usuario = '';
    $diametro = '';
    $longitud = '';
    $barrio = '';
    $zona = '';
    $fecha_ejecucion = '';
    $fecha_conclusion = '';
    $monto = '';
    $porcentaje = '';
    foreach ($acta as $key) {
        $usuario = $key->name;
        $diametro = $key->diametro_in;
        $longitud = $key->longitud_inspector;
        $barrio = $key->calle_sol;
        $zona = $key->zona_sol;
        $fecha_ejecucion = $key->fecha_progrmada;
        $fecha_conclusion = $key->fecha_ejecutada;
        $monto = $key->total_cantidad_material + $key->total_costo_material + ($key->total_cantidad_obra + $key->total_costo_obra);
        $porcentaje = round((($key->total_material_elapas + $key->total_obra_elapas) * 100) / ($key->total_material + $key->total_obra),2);
    }
@endphp
@php
    $precio_total = 0.0;
    $costo_total = 0.0;
@endphp
@foreach ($materiales as $material)
    @php
        $sub_total = round($material->sub_total, 2);
    @endphp
    @php
        $precio_total = $precio_total + $sub_total;
    @endphp
@endforeach
@php
    $costo_total = $costo_total + $precio_total;
@endphp
@php
    $precio_total = 0.0;
    $n = 1;
@endphp
@foreach ($mano_obra as $mano)
    @php
        $sub_total = round($mano->cantidad * $mano->precio_unitario, 2);
    @endphp

    @php
        $precio_total = $precio_total + $sub_total;
    @endphp
@endforeach
@php
    $costo_total = $costo_total + $precio_total;
@endphp
<style>
    body {
        font-family: verdana;
    }

    .container {
        margin: 0px 5px;
    }

    #contenedor {
        width: 680px;
        margin: 0 auto;
    }

    table,
    td {
        border-collapse: collapse;
        border: 0;
        border-bottom: 1px solid #000
    }

    .centrar {
        text-align: center;
    }

    .total {
        float: right;
        border: 1px dashed black;
        height: 21px;
        width: 83px;
        outline: 1px solid black;
    }

    .tamanio {
        font-size: 13px;
    }
</style>

<body>
    <div class="container" id="contenedor">
        <div>
            <h2 align="center">ACTA DE ENTREGA DE OBRA:</h2>
            <h3 align="center">AMPLIACION RED DE AGUA DISTRITO 5 DE SUCRE INSPECTOR: {{ $usuario }}</h3>
            <h3 align="center">PROYECTO POR ADMINISTRACION DIRECTA - ELAPAS: AMPLIACION RED DE AGUA BARRIO
                {{ $barrio }} "{{ $zona }}" D-5 PVC {{ $diametro }}"</h3>
        </div>
        <div>
            <div>
                <table border="none" align="center">
                    <tr>
                        <td>LONGITUD DE TUBERIA:</td>
                        <td>{{ $longitud }}&nbsp;m</td>
                    </tr>
                    <tr>
                        <td>FINANCIAMIENTO:</td>
                        <td>{{ $porcentaje }}&nbsp;%</td>
                    </tr>
                    <tr>
                        <td>EJECUTOR:</td>
                        <td>ELAPAS JEFATURA RED DE AGUA</td>
                    </tr>
                    <tr>
                        <td>MONTO DE OBRA:</td>
                        <td>Bs.&nbsp;{{ $costo_total }}</td>
                    </tr>
                    <tr>
                        <td>FECHA DE CONCLUSION DE OBRAS:</td>
                        <td>{{ date('d-m-Y', strtotime($fecha_ejecucion)) }}</td>
                    </tr>
                    <tr>
                        <td>FECHA DE RECEPCION DE OBRA:</td>
                        <td>{{ date('d-m-Y', strtotime($fecha_conclusion)) }}</td>
                    </tr>
                    <tr>
                        <td>% DE AVANCE FISICO:</td>
                        <td>100 %</td>
                    </tr>
                    <tr>
                        <td>SUPERVISION:</td>
                        <td>Ing. Rene Iglesias</td>
                    </tr>
                    <tr>
                        <td>FISCAL DE OBRAS:</td>
                        <td>Ing. Enzo Porcel A.</td>
                    </tr>
                </table>
            </div>
            <p class="tamanio">
                En Presencia del supervisor de obras, fiscal de obra, inspector de red de agua y beneficiarios
                de la
                AMPLIACION de Red de Agua, se procedió a la firma del: ACTA DE ENTREGA DE OBRA AMPLIACIONRED
                DE AGUA : BARRIO {{ $barrio }} "{{ $zona }}" PVC {{ $diametro }}".
            </p>
            <br><br><br><br>
            <div>
                <label class="tamanio">&nbsp;&nbsp;{{ $usuario }}</label><br>
                <label class="tamanio">&nbsp;&nbsp;INSPECTOR RED DE AGUA:</label><br>
            </div>
        </div>

    </div>
    </div>

</body>

</html>
