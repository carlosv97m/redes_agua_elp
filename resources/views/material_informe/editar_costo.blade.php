@if ($mat_inf == [])
    <p>No se asignaron materiales</p>
@else
    {{-- <h3>ID: {{ $primer_id->id }}</h3>
<h3>Total: {{ $ultimo_id->id }}</h3>
<h3>{{ $total_ids }}</h3> --}}
    <div class="table table-bordered table-hover dataTable table-responsive">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th width="170">ID</th>
                    <th>MATERIAL</th>
                    {{-- <th width="150">UNIDAD <br> MEDIDA</th> --}}
                    <th width="150">CANTIDAD <br> SOLICITADA </th>
                    <th width="150">PRECIO <br> UNITARIO </th>
                    <th width="150">Costo</th>
                    <th width="150">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mat_inf as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td align="center">{{ $value->material_n }}</td>
                        {{-- <td>{{ $value->u_medida }}</td> --}}
                        <td>
                            <input style="width: 80;" type="number" name="cantidad_solicitada" id="cantidad_solicitada" value="{{ $value->cantidad }}">
                        </td>
                        <td>
                            <input style="width: 80;" type="number" name="precioUnitario" id="precioUnitario"
                                value="{{ $value->precio_unitario }}">
                        </td>
                        <td>
                            <input type="hidden" name="id" id="id" value="{{ $value->id }}">
                            <input style="width: 80;" type="number" name="precioTotal" id="precioTotal"
                                value="{{ $value->sub_total }}">
                        </td>
                        <td>
                            <button type="button" class="d-inline btn btn-warning btn-icon editCost" title="Modificar Costo">
                                Editar Costo <i class="fas fa-pencil-alt"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@endif
@section('js')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        const ruta_update_costos = "{{ route('informes.update_costos') }}"
    </script>
    <script src="{{ asset('js/informes.js') }}"></script>
    @if (session('modificado') == 'ok')
        <script>
            Swal.fire(
                'Modificado!',
                'Su registro ha sido Corregido.',
                'success'
            )
        </script>
    @endif
    @if (session('modificado') == 'error')
        <script>
            Swal.fire(
                'Error!',
                'Error al realizar la peticion.',
                'danger'
            )
        </script>
    @endif
@endsection
