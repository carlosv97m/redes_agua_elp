@extends('adminlte::page')

@section('title', 'Cronograma')
@php
    $dias = ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'];
    $fecha = '12-05-2021';
    $dia = $dias[date('N', strtotime($fecha))];
@endphp
@section('content_header')
    <style>
        #map {
            margin-top: 20px;
            width: 100%;
            height: 400px;
        }
    </style>
    @can('jefe-red')
        <h1>ELAPAS - CRONOGRAMA DE ASIGNACION
        </h1>
        <h2>{{ date('d-m-Y') }}</h2>
    @endcan
    @can('Monitor')
        <h1>ELAPAS - LISTADO DE SOLICITUDES</h1>
    @endcan


@stop
@section('content')

    <div class="table table-bordered table-hover dataTable table-responsive" id="contenedor-tabla">
        <table class="table table-bordered datatable" id="example">
            <thead>
                <tr>
                    <th>Nro</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th>estado</th>
                    <th width="120">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @can('jefe-red')
                    @foreach ($solicitud as $sol)
                        <tr>
                            <td>S-{{ $sol->id }}</td>
                            <td>{{ $sol->nombre_sol }}</td>
                            <td>{{ $sol->celular_sol }}</td>
                            <td>{{ $sol->zona_sol }}</td>
                            <td>{{ $sol->calle_sol }}</td>
                            <td>{{ $sol->estado_sol }}</td>
                            <td>
                                <div class="btn-group">
                                    <!-- Boton para mostrar los Archivos de las solicitudes -->
                                    <button class="d-inline btn btn-success btn-icon" id="archivos_solicitud"
                                        data-id="{{ $sol->id }}" title="Archivos" data-toggle="modal"
                                        data-target="#myModal"><i class="fas fa-plus"></i></button>
                                    <button type="button" class='btn btn-primary btn-icon' data-toggle="modal"
                                        data-target=".bd-example-modal-lg{{ $sol->id }}" onclick=""
                                        title="Asignar Inspector"><i class="fas fa-user-check"></i></button>

                                    <!-- Large modal -->
                                    <div class="modal fade bd-example-modal-lg{{ $sol->id }}" role="dialog"
                                        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="overflow:hidden;">
                                        <div class="modal-dialog modal-md">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Inspectores</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" id="contenido">
                                                    <div class="justify-content-center row">
                                                        <!-- left column -->
                                                        <div class="col-md-12">
                                                            <!-- general form elements -->
                                                            <div class="card card-primary ">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">Asignar Inspector</h3>
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <!-- formulario inicio -->
                                                                <form action="{{ route('cronograma.store') }}" method="POST"
                                                                    role="form" id="form_materials" autocomplete="off">
                                                                    @csrf
                                                                    <div class="card-body">
                                                                        <div class="form-group">
                                                                            <label for="nombre_material">Solicitud</label>
                                                                            <p class="form-control">{{ $sol->nombre_sol }}</p>
                                                                            <input type="hidden" name="solicitud_id"
                                                                                value="{{ $sol->id }}">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="nombre_material">Nombre del
                                                                                Inspector</label>
                                                                            <select class="form-control  select2" name="user_id"
                                                                                id="user_id">
                                                                                <option selected>---Seleccione Inspector---
                                                                                </option>
                                                                                @foreach ($inspectores as $inspector)
                                                                                    <option value="{{ $inspector->id }}">
                                                                                        {{ $inspector->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="fecha_inspe">Fecha de inspeccion</label>
                                                                            <div class="input-group ">
                                                                                <div class="input-group-prepend">
                                                                                </div>
                                                                                <input class="form-control" id="fecha_inspe"
                                                                                    name="fecha_inspe" type="datetime-local"
                                                                                    value="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.card-body -->
                                                                    <div class="card-footer">
                                                                        <button type="submit"
                                                                            class="btn btn-block btn-primary">Asignar</button>
                                                                    </div>
                                                                </form>
                                                                {{-- Fin de formulario --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <a type="button" class="d-inline btn btn-warning btn-icon" data-toggle="modal"
                                        data-target=".bd-example-modal-lg"
                                        onclick="visualizarMapa({{ $sol->x_aprox }},{{ $sol->y_aprox }}, {{ $sol->id }})"
                                        title="Visualizar" id="btn_mostrar_mapa">
                                        <i class="fas fa-eye"></i></a>
                                    {{-- <a type="button" class="d-inline btn btn-warning btn-icon" title="Visualizar" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="mimapa({{$sol->x_aprox}},{{$sol->y_aprox}})" id="btn_mostrar_mapa">
                          <i class="fas fa-eye"></i></a> --}}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endcan

            </tbody>
            <tfoot>
                <tr>
                    <th>Nro</th>
                    <th>Nombre solicitante</th>
                    <th>Celular</th>
                    <th>Zona</th>
                    <th>Calle</th>
                    <th>estado</th>
                    <th>Acciones</th>
                </tr>
            </tfoot>
        </table>
        <div class="card-body">
            <h4>Estados</h4>
            <div>
                <p><b>Pendiente: </b> El usuario hizo su solicitud </p>
                <p><b>Aprobado: </b> La solicitud de ampliación fue aprobada por el jefe de red</p>
                <p><b>Aplazado: </b> La solicitud de ampliación fue aplazada por el jefe de red </p>
                <p><b>Asignado: </b> El jefe de red asignó a un inspector para que realice la inspeccion en la ubicación
                    solicitada</p>
                <p><b>Inspeccionado: </b> El inspector ya realizo el informe de inspeccion y solicita la revision del jefe
                    de red
                </p>
                <p><b>Autorizado: </b> El inspector ya puede realizar el peticion de materiales y mano de obra para la
                    ampliación</p>
                <p><b>Solicitud de firma: </b> El inspector ya realizó la petición de materiales y mano de obra y esta a la
                    espera de la aprobación del jefe de red</p>
                <p><b>Firmado: </b> El jefe de red ya firmó la petición de materiales y mano de obra, la ampliación es
                    derivada al proyectista</p>
                <p><b>Ejecutado: </b> La ampliación ya fue ejecutada</p>
                <p><b>Observado: </b> El jefe de red observó errores en el informe de inspección, pedido de materia o mano
                    de obra</p>
            </div>
        </div>
    </div>
    <div id="contenedor-mapa" style="display: none">
        <input type="hidden" id="obtenerAmpliaciones">
        <button onclick="mostrarTabla(false)" class="btn btn-primary"> <i class="fas fa-arrow-circle-left"></i> Volver
        </button>
        <div class="col-md-12">
            <div id="map">
            </div>


        </div>
    </div>

    {{-- Modal para el registrar coordenadas --}}
    {{-- <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div id="map" class="modal-content">

    </div>
  </div>
</div> --}}

    {{-- Modal fin --}}
    <!-- Vista de archivos -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Archivos de la Solicitud</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button> --}}
                </div>
                <div class="modal-body table table-bordered table-hover dataTable table-responsive w-100"
                    id="contenedor-tabla">
                    <h1 id="tiempo_carga">Cargando......</h1>
                    <p id="sin_datos"></p>
                    <table id="datos_modal_archivos" class="datos_modal_archivos table table-bordered datatable"
                        width="100%" height="100%">
                    </table>
                </div>
                <div class="mostrar_guardar" style="display: none">
                    <label><input type="checkbox" id="myCheck"> Agregar nuevo registro</label>
                </div>
                <form method="POST" action="{{ route('archivos_solicitud.store') }}" id="ingresar_nuevo"
                    enctype="multipart/form-data" role="form" class="create" autocomplete="off">
                    @csrf
                    <input type="hidden" name="solicitud_id" id="solicitud_id">
                    <div class="card-body">
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" lang="es" accept=".pdf"
                                        class=" custom-file-input @error('archivo_solicitud') is-invalid @enderror"
                                        onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])"
                                        id="archivo_solicitud" name="archivo_solicitud">
                                    <label class="custom-file-label" for="archivo_solicitud">Subir Solicitud
                                        Escaneada</label>
                                </div>
                            </div>
                            @error('archivo_solicitud')
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>
                        <div class=" mt-2 form-group">
                            <div class="input-group">
                                <label for="descripcion">Descripcion</label>
                                <textarea name="descripcion" id="descripcion" cols="60" rows="5" style="text-transform:uppercase;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="float: right;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-plus"></i> Agregar</button>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fa fa-times"></i>
                        Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Eliminacion de Archivos -->
    <div class="modal fade" id="eliminacion_archivos" tabindex="-1" role="dialog" aria-hidden="true"
        style="display: none">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form method="POST" action="{{ route('archivos_solicitud.destroy') }}" id="ingresar_nuevo"
                    autocomplete="off" enctype="multipart/form-data" role="form" class="create">
                    @csrf
                    {{-- @method('delete') --}}
                    <input type="hidden" name="id_eliminar" id="id_eliminar">
                    <div class="card-body" style="text-align: center">
                        <h2><i class="fas fa-exclamation-triangle" style="color: yellow; font-size: 100px;"></i> <br>
                            ¿Está seguro?</h2>
                        <h3> ¡No podrás revertir esto!</h3>
                        <p id="id_para_eliminar" style="display: none"> <i class="fas fa-exclamation-triangle"></i> </p>
                    </div>
                    <div class="card-footer" style="text-align: center;">
                        <button type="submit" class="btn btn-success" id="agregar_nuevo_archivo"> <i
                                class="fa fa-trash"></i> Eliminar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-times"></i>
                            Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Vista Archivos de la Solicitud -->
    <div class="modal fade" id='show_vista_archivos' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <!-- id='show_vista_archivos' -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-heeder">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button> --}}
                </div>
                <h3 style="text-align: center">Cargando......</h3>
                <p id="impresion_archivos_solicitud" style="display: none"></p>
                <div class="contenedor_impresion" id="contenedor_impresion">
                    <iframe id="iframe" name="myIframe" frameborder="5" width="500" height="300"
                        style="display: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyBUW9zimMRIYLdOBY4FrSyqd13IaJ7N4Y0"></script>


@stop
@section('js')
    <script>
        const camara_icono = "{{ asset('images/alc_camara.png') }}";
        const ubicacion_icono = "{{ asset('images/marker-icon.png') }}"
    </script>
    <script src="{{ asset('vendor/leaflet/js/leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/esri-leaflet-geocoder.js') }}" crossorigin=""></script>
    <script src="{{ asset('vendor/leaflet/js/easy-button.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('js/betterwms.js') }}"></script>
    <script src="{{ asset('js/mapas.js') }}"></script>
    <script src="{{ asset('js/solicitud.js') }}"></script>
    <script src="{{ asset('js/informes.js') }}"></script>
    <script src="{{ asset('js/cronograma.js') }}"></script>
    <script src="{{ asset('js/archivos_solicitud.js') }}"></script>
    <script>
        const ruta = "{{ route('archivos_solicitud.show') }}";
        const ruta2 = "{{ route('archivos_solicitud.mostrar') }}";
        const ruta3 = "{{ route('solicitud.escaneada') }}";
        const ruta_asset = "{{ asset('archivos_solicitudes/') }}";
        const ruta_asset2 = "{{ asset('solicitudes/') }}";
    </script>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/leaflet.css') }}" crossorigin="" />
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/esri-leaflet-geocoder.css') }}" crossorigin="">
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/css/easy-button.css') }}">
@stop
