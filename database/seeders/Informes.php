<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Informes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `informes` (`id`, `fecha_hora_in`, `fecha_autorizacion`, `fecha_autorizado`, `fecha_visto_bueno`, `espesifiar_in`, `x_exact`, `y_exact`, `ubicacion_geo`, `longitud_in`,`longitud_inspector`, `diametro_in`, `num_ben_in`, `num_flia_in`, `condicion_rasante`, `reservorio`, `tendido_u`, `tendido_c`, `excabacion`, `tapado_sanja`, `estado_in`, `solicitud_id`, `imagen_amp`, `created_at`, `updated_at`) VALUES
            (1, '2022-05-18 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 1, NULL, '2022-05-12 10:12:18', '2022-05-12 10:12:18'),
            (2, '0008-12-18 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 2, NULL, '2022-07-15 13:11:03', '2022-07-15 13:11:03'),
            (3, '2022-08-10 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 7, NULL, '2022-08-10 13:13:59', '2022-08-10 13:13:59'),
            (4, '2022-08-10 11:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 8, NULL, '2022-08-10 13:14:25', '2022-08-10 13:14:25'),
            (5, '2022-08-10 12:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 9, NULL, '2022-08-10 13:14:50', '2022-08-10 13:14:50'),
            (6, '2022-08-10 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 4, NULL, '2022-08-10 13:15:29', '2022-08-10 13:15:29'),
            (7, '2022-08-10 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 5, NULL, '2022-08-10 13:15:58', '2022-08-10 13:15:58'),
            (8, '2022-08-10 11:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 6, NULL, '2022-08-10 13:16:29', '2022-08-10 13:16:29'),
            (9, '2022-08-24 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 14, NULL, '2022-08-23 08:28:33', '2022-08-23 08:28:33'),
            (10, '2022-08-24 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 15, NULL, '2022-08-23 08:34:22', '2022-08-23 08:34:22'),
            (11, '2022-08-24 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 17, NULL, '2022-08-23 09:33:49', '2022-08-23 09:33:49'),
            (12, '2022-08-31 08:34:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 19, NULL, '2022-08-30 08:33:07', '2022-08-30 08:33:07'),
            (13, '2022-08-31 11:03:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 16, NULL, '2022-08-30 09:01:55', '2022-08-30 09:01:55'),
            (14, '2022-08-31 16:04:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 21, NULL, '2022-08-30 16:01:57', '2022-08-30 16:01:57'),
            (15, '2022-09-07 10:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asignado', 26, NULL, '2022-09-07 08:28:45', '2022-09-07 08:28:45');
        ");
    }
}
