<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO redes_agua.materials
        (id, nombre_material, observaciones, precio_unitario, unidad_med, estado, created_at, updated_at)
        VALUES(1, 'Tubos - 6 metros', NULL, 5.0, 'pieza', 'disponible', '2023-03-21 11:47:14.000', '2023-03-21 11:47:14.000');
        ");
    }
}
