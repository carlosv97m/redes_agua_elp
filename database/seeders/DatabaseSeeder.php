<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(Actividad_mano_obraSeeder::class);
        $this->call(MaterialSeeder::class);
        $this->call(Solicitudes::class);
        $this->call(Cronogramas::class);
        $this->call(Informes::class);
    }
}
