<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::updateOrCreate(['name' => 'administrador']); // 1
        $role2 = Role::updateOrCreate(['name' => 'Jefe de red']); // 2
        $role3 = Role::updateOrCreate(['name' => 'Inspector']); //3
        $role4 = Role::updateOrCreate(['name' => 'Secretaria']); //4
        $role5 = Role::updateOrCreate(['name' => 'Monitor']); //5
        $role6 = Role::updateOrCreate(['name' => 'Proyectista']); //6


        Permission::updateOrCreate(['name' => 'dash'])->syncRoles([$role, $role2, $role3, $role4, $role5, $role6]);

        Permission::updateOrCreate(['name' => 'users.index'])->syncRoles([$role]);
        Permission::updateOrCreate(['name' => 'users.edit'])->syncRoles([$role]);

        Permission::updateOrCreate(['name' => 'solicitud.index'])->syncRoles([$role, $role2, $role4]);
        Permission::updateOrCreate(['name' => 'solicitud.create'])->syncRoles([$role, $role4]);
        Permission::updateOrCreate(['name' => 'solicitud.edit'])->syncRoles([$role, $role4]);
        Permission::updateOrCreate(['name' => 'solicitud.pdf'])->syncRoles([$role,$role2, $role4]);
        Permission::updateOrCreate(['name' => 'solicitud.delete'])->syncRoles([$role, $role4]);
        Permission::updateOrCreate(['name' => 'solicitud.ampliaciones'])->syncRoles([$role, $role2, $role3, $role4]);


        Permission::updateOrCreate(['name' => 'informes.index'])->syncRoles([$role, $role2, $role3]);
        Permission::updateOrCreate(['name' => 'informes.create'])->syncRoles([$role, $role2, $role3]);
        Permission::updateOrCreate(['name' => 'informes.edit'])->syncRoles([$role, $role3]);
        Permission::updateOrCreate(['name' => 'informes.delete'])->syncRoles([$role, $role3]);
        Permission::updateOrCreate(['name' => 'jefe-red'])->syncRoles([$role2]);
        Permission::updateOrCreate(['name' => 'inspector'])->syncRoles([$role3]);

        Permission::updateOrCreate(['name' => 'materials.index'])->syncRoles([$role, $role2, $role3]);
        Permission::updateOrCreate(['name' => 'materials.create'])->syncRoles([$role, $role2, $role3]);
        Permission::updateOrCreate(['name' => 'materials.edit'])->syncRoles([$role, $role3]);
        Permission::updateOrCreate(['name' => 'materials.delete'])->syncRoles([$role, $role3]);

        Permission::updateOrCreate(['name' => 'Monitor'])->syncRoles([$role5]);
        Permission::updateOrCreate(['name' => 'Proyectista'])->syncRoles([$role6]);
        Permission::updateOrCreate(['name' => 'Secretaria'])->syncRoles([$role4,$role3,$role2]);

        Permission::updateOrCreate(['name' => 'informes-ejecucion'])->syncRoles([$role2, $role3]);
        Permission::updateOrCreate(['name' => 'menu-solicitud'])->syncRoles([$role2, $role4]);
        Permission::updateOrCreate(['name' => 'menu-informes'])->syncRoles([$role2, $role3, $role5, $role6]);
    }
}
