<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivosSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivos_solicituds', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_archivo',100);
            $table->string('descripcion',250);
            $table->date('fecha_archivo');
            $table->integer('user_id');
            $table->integer('solicitud_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivos_solicituds');
    }
}
