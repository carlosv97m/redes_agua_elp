function mostrarPDF(url) {
    let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
    let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if (es_chrome) {
        var iframe = document.createElement("iframe");
        iframe.style.display = "none";
        iframe.src = url;
        document.body.appendChild(iframe);
        iframe.focus();
        iframe.contentWindow.print();
        console.log(iframe.contentWindow)
    } else if (es_firefox) {
        console.log('El navegador es Firefox');
        var win = window.open(url, "_blank");
        win.onload = function () {
            win.print();
        }
        win.focus();
    } else {
        var win = window.open(url, "_blank");
        win.focus();
    }
}

$(document).on('click', "#archivo_escaneado_informes", function () {
    let id = $(this).data('id');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    console.log(data);
    $("#datos_modal_archivos").empty();
    $("#impresion_archivos_solicitud").empty();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: ruta3,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            console.log('Nombre del Archivo', resp.data);
            $('#show_vista_archivos').modal('hide');
            let nuevo_iframe = ruta_asset2 + '/' + resp.data;
            console.log(nuevo_iframe);
            let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
            let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
            if (es_chrome) {
                $('#show_vista_archivos').modal('hide');
                var iframe = document.createElement("iframe");
                iframe.style.display = "none";
                iframe.src = nuevo_iframe;
                document.body.appendChild(iframe);
                iframe.focus();
                iframe.contentWindow.print();
                console.log(iframe.contentWindow);
            } else if (es_firefox) {
                console.log('El navegador es Firefox');
                window.location.href = nuevo_iframe;
            } else {
                $('#show_vista_archivos').modal('hide');
                var win = window.open(nuevo_iframe, "_blank");
                win.focus();
            }
        }

    });

});

$(document).on('click', "#solicitar", function () {
    let id = $(this).data('id');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    console.log(data);
    $("#id_informe").empty();
    $("#mensaje_informacion_estado").empty();
    $('#botones_estado').css('display', 'none');
    var mensaje = document.getElementById('mensaje_informacion_estado');
    var text = '';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: ruta4,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            console.log(resp);
            $('#botones_estado').css('display', 'block')
            if (resp.data == 'autorizado') {
                text = "¡No podrás solicitar mas materiales o mano de obra!";
            } else if (resp.data == 'observado') {
                text = "¡No podrás solicitar mas materiales o mano de obra!";
            } else {
                text = "¡Habilitaras nuevamente la solicitud de materiales y mano de obra!";
            }
            mensaje.innerText = text;
            var id_documento = document.getElementById('id_informe').append(id);
            var id_documento = document.getElementById('id_informe').value = id;
        }
    })
});

$('#limpiar').click(function () {
    // $('.concluidos').empty();
    // document.getElementById("fecha_fin").value = "";
    // document.getElementById("fecha_inicio").value = "";
    // $('#form_fecha_inicio').empty();
    // $('#form_fecha_fin').empty();
    // $('#imprimir').css('display', 'none');
    window.location.href = ruta_ejecutados;
})
$('#btn_imprimir').click(function () {
    let fecha_i = $("#fecha_inicio").val().length > 0 ? '/' + $("#fecha_inicio").val() : '';
    let fecha_f = $("#fecha_fin").val().length > 0 ? '/' + $("#fecha_fin").val() : '';
    var ruta = ruta_impresion + fecha_i + fecha_f;
    console.log(ruta);
    let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
    if (es_chrome) {
        var iframe = document.createElement("iframe");
        iframe.style.display = "none";
        iframe.src = ruta;
        document.body.appendChild(iframe);
        iframe.focus();
        iframe.contentWindow.print();
        console.log(iframe.contentWindow)
    } else {
        var win = window.open(url, "_blank");
        win.focus();
    }
})
// $("#fecha_fin").change(function () {
//     var fin = $("#fecha_fin").val();
//     var inicio = $("#fecha_inicio").val();
//     // $('#form_fecha_inicio').empty();
//     // $('#form_fecha_fin').empty();
//     var data = {
//         'fecha_inicio': inicio,
//         'fecha_fin': fin,
//     }
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });
//     let num = 1;
//     $('.concluidos').empty();
//     console.log(data);
//     $.ajax({
//         url: ruta_busqueda,
//         type: "POST",
//         data: data,
//         dataType: 'json',
//         cache: false,
//         success: function (resp) {
//             // var form_fecha_inicio = document.getElementById('form_fecha_inicio').append(inicio);
//             // var form_fecha_inicio = document.getElementById('form_fecha_inicio').value=inicio;
//             // var form_fecha_fin = document.getElementById('form_fecha_fin').append(fin);
//             // var form_fecha_fin = document.getElementById('form_fecha_fin').value=fin;
//             $('#imprimir').css('display', 'block');
//             console.log(resp);
//             $('.concluidos').append("<thead>" +
//                 "<tr>" +
//                 "<th scope='col'>#</th>" +
//                 "<th scope='col'>NOMBRE SOLICITANTE</th>" +
//                 "<th scope='col'>FECHA <br> SOLICITADA</th>" +
//                 "<th scope='col'>FECHA <br> INSPECCION</th>" +
//                 "<th scope='col'>CALLE</th>" +
//                 "<th scope='col'>ZONA</th>" +
//                 "<th scope='col'>ESTADO</th>" +
//                 "<th scope='col'>FECHA <br> EJECUCION</th>" +
//                 // "<th scope='col'>Inspector</th>" +
//                 "<tr>" +
//                 "</thead>");
//             resp.data.forEach(element => {
//                 $(".concluidos").append("<tr>" +
//                     "<td scope='row'>" + num + "</td>" +
//                     "<td scope='row'>" + element.nombre_sol + "</td>" +
//                     "<td scope='row'>" + element.fecha_sol + "</td>" +
//                     "<td scope='row'>" + element.fecha_hora_in + "</td>" +
//                     "<td scope='row'>" + element.calle_sol + "</td>" +
//                     "<td scope='row'>" + element.zona_sol + "</td>" +
//                     "<td scope='row'>" + element.estado_in + "</td>" +
//                     "<td scope='row'>" + element.fecha_ejecutada + "</td>" +
//                     "</tr>");
//                 num++
//             });
//         }
//     })
// })
$(document).on('click', "#btn_materialForm", function () {
    console.log('Se hizo click en el botón');
    var r_url = ruta_update_costos;
    var rows = $('#example tbody tr');

    rows.each(function () {
        var idInput = $(this).find('input[name="id"]');
        var precioUnitarioInput = $(this).find('input[name="precioUnitario"]');
        var precioTotalInput = $(this).find('input[name="precioTotal"]');
        console.log("DATA INPUT ", precioTotalInput)
        if (idInput.length > 0 && precioUnitarioInput.length > 0 && precioTotalInput.length > 0) {
            var id = idInput.val();
            var precioUnitario = precioUnitarioInput.val();
            var precioTotal = precioTotalInput.val();

            var data = {
                'id': id,
                'precioUnitario': precioUnitario,
                'precioTotal': precioTotal,
                _token: $('input[name="_token"]').val()
            };

            console.log("datos: ", { data });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: r_url,
                method: 'POST',
                data: data,
                dataType: 'json',
                cache: false,
                success: function (response) {
                    console.log('Datos enviados con éxito');
                },
                error: function (textStatus) {
                    console.log('Error al enviar los datos: ' + textStatus);
                }
            });
        }
    });
});

$(document).on('click', ".editCost", function () {
    var row = $(this).closest('tr');
    var id = row.find('input[name="id"]').val();
    var cantidad_solicitada = row.find('input[name="cantidad_solicitada"]').val();
    var precioUnitario = row.find('input[name="precioUnitario"]').val();
    var precioTotal = row.find('input[name="precioTotal"]').val();
    var data = {
        'id': id,
        'cantidad_solicitada': cantidad_solicitada,
        'precioUnitario': precioUnitario,
        'precioTotal': precioTotal,
        _token: $('input[name="_token"]').val()
    };

    console.log("datos: ", { data });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: ruta_update_costos,
        method: 'POST',
        data: data,
        dataType: 'json',
        cache: false,
        success: function (response) {
            console.log('Datos enviados con éxito', response);
            var status = response.status;
            var message = response.message;
            if (status === 'ok') {
                // alert('Éxito: ' + message);
                Swal.fire(
                    '¡Éxito!',
                    message,
                    'success'
                );
            } else if (status === 'error') {
                // alert('Error: ' + message);
                Swal.fire(
                    'Error',
                    message,
                    'error'
                );
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error al enviar los datos: ' + textStatus);
            Swal.fire(
                'Error',
                'Error al enviar los datos: ' + textStatus,
                'error'
            );
        }
    });
});


