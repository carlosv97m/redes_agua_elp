// import { jsPDF } from "jspdf";
function modalObservaciones(ruta) {
    document.querySelector('#id_ruta').value = 'solicitud/' + ruta + '/rechazar';
    document.querySelector('#observaciones').value = "";
}
function visualizarMapa(lat, long, ruta) {
    mostrarTabla(true);
    console.log(document.querySelector('#obtenerAmpliaciones').value)
    document.querySelector('#obtenerAmpliaciones').value = 'solicitud/' + ruta + '/obtener_ampliacion';
    console.log(ruta, 'a ver')
    ruta == null ? initMap(lat, long, 'mostrar') : initMap(lat, long);

}
$(document).on('click', "#comrpobante_sol", function () {

    let id = $(this).data('id');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    console.log(data);
    $('#comprobante_modal').modal('hide');
    $.ajax({
        url: ruta_comprobante,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            if (resp) {
                console.log('ENTRO');
                let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
                let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
                if (es_chrome) {
                    var iframe = document.createElement("iframe");
                    iframe.style.display = "none";
                    iframe.src = url;
                    document.body.appendChild(iframe);
                    iframe.focus();
                    iframe.contentWindow.print();
                    console.log(iframe.contentWindow)
                } else if (es_firefox) {
                    console.log('El navegador es Firefox');
                    window.location.href = url;
                } else {
                    var win = window.open(url, "_blank");
                    win.focus();
                }
            }
        }

    });
})

function mostrar_comprobante(url) {
    console.log(url);
    let es_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    if (es_chrome) {
        var iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        iframe.src = url;
        document.body.appendChild(iframe);
        iframe.focus();
        iframe.contentWindow.print();
        console.log(iframe.contentWindow);
    } else if (es_firefox) {
        console.log('El navegador es Firefox');
        // fetch(url)
        //     .then(response => response.text())
        //     .then(html => {
        //         console.log(html)
        //         var win = window.open(url, '_blank');
        //         win.focus();
        //     })
        //     .catch(error => console.log('Hubo un problema con la petición Fetch: ' + error.message));
        var win = window.open(url, "_blank");
        win.onload = function () {
            win.print();
        }
        win.focus();
    } else {
        var win = window.open(url, '_blank');
        win.focus();
    }
}

$(document).on('click', "#archivo_escaneado", function () {
    let id = $(this).data('id');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    var asset = ruta_asset2;
    // console.log(asset);
    $("#datos_modal_archivos").empty();
    $("#impresion_archivos_solicitud").empty();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // var text = document.createTextNode(id);
    // document.getElementById('impresion_archivos_solicitud').appendChild(text);
    var id_e = document.getElementById('impresion_archivos_solicitud').append(id);
    var id_e = document.getElementById('impresion_archivos_solicitud').value = id;
    var frame = document.getElementById("iframe");
    $.ajax({
        url: ruta3,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            console.log('Nombre del Archivo', resp.data);
            $('#show_vista_archivos').modal('hide');
            let nuevo_iframe = asset + '/' + resp.data;
            console.log(nuevo_iframe);
            let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
            let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
            if (es_chrome) {
                $('#show_vista_archivos').modal('hide');
                var iframe = document.createElement("iframe");
                iframe.style.display = "none";
                iframe.src = nuevo_iframe;
                document.body.appendChild(iframe);
                iframe.focus();
                iframe.contentWindow.print();
                console.log(iframe.contentWindow)
            } else if (es_firefox) {
                console.log('El navegador es Firefox');
                window.location.href = nuevo_iframe;
            } else {
                $('#show_vista_archivos').modal('hide');
                var win = window.open(nuevo_iframe, "_blank");
                win.focus();
            }
        }
    });

});

$(document).on('click', "#avance", function () {
    // alert('hola');
    let id = $(this).data('id');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    let num = 1;
    console.log(data);
    $('#estado').empty();
    $("#table_avance").empty();
    $("#tiempo_carga_avance").css('display', 'block');
    $("#tiempo_carga_avance_false").css('display', 'none');
    $("#footer_avance").css('display', 'none');
    $("#solicitud_id").empty();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var id_documento = document.getElementById('solicitud_id').append(id);
    var id_documento = document.getElementById('solicitud_id').value = id;
    $.ajax({
        url: ruta_avance,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            console.log(resp);
            if (resp.success === true) {
                if (resp.ultimos_registros.length > 1) {
                    //
                    document.getElementById('estado').append(resp.ultimos_registros[1].estado);
                    document.getElementById('estado').value = resp.ultimos_registros[1].estado;
                    document.getElementById('estado_1').append(resp.ultimos_registros[1].estado);
                    document.getElementById('estado_1').value = resp.ultimos_registros[1].estado;
                    // console.log('Nombre del Archivo', resp.ultimos_registros[1].estado);
                    $('#tiempo_carga_avance').css('display', 'none');
                    $('.table_avance').append("<thead>" +
                        "<tr>" +
                        "<th scope='col'>#</th>" +
                        "<th scope='col'>Fecha y Hora</th>" +
                        "<th scope='col'>Estado</th>" +
                        "<tr>" +
                        "</thead>");

                    resp.data.forEach((element, index) => {
                        let span = "";
                        if (index === resp.data.length - 1) {
                            // Agrega el span solo si este es el último elemento del arreglo
                            span = "<span class='badge badge-primary'>" + element.estado + "</span>";
                        } else {
                            span = element.estado;
                        }
                        if (element.estado === 'ejecutado') {
                            $('#footer_avance').css('display', 'none');
                        } else {
                            $('#footer_avance').css('display', 'block');
                        }
                        $(".table_avance").append("<tr>" +
                            "<td scope='row'>" + num + "</td>" +
                            "<td scope='row'>" + Date(element.created_at) + "</td>" +
                            "<td scope='row'>" + span + "</td>" +
                            "</tr>");
                        num++;
                        // console.log(resp.data);
                    });
                } else {
                    $('#tiempo_carga_avance').css('display', 'none');
                    $("#tiempo_carga_avance_false").css('display', 'block');
                }
            } else {
                $('#tiempo_carga_avance').css('display', 'none');
                $("#tiempo_carga_avance_false").css('display', 'block');
                console.log(resp.mensaje);
            }
        }

    });

});
$(document).on('click', "#cambio_estado_solicitud", function () {
    // alert('hola');
    let id = $(this).data('id-sol');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    $('#estado_actual').empty();
    $("#tiempo_cambio_estado_solicitud").css('display', 'block');
    $("#tiempo_cambio_estado_solicitud_false").css('display', 'none');
    $("#formulario_cambio_estado").css('display', 'none');
    $("#ocultar_con_existencia").css('display', 'block');
    $("#informacion_del_cambio").css('display', 'none');
    $("#solicitud_id_actual").empty();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: ruta_avance,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            if (resp.success === true) {
                console.log(resp);
                $('#tiempo_cambio_estado_solicitud').css('display', 'none');
                $("#formulario_cambio_estado").css('display', 'block');
                document.getElementById('solicitud_id_actual').append(id);
                document.getElementById('solicitud_id_actual').value = id;

                if (typeof resp.estado_en_el_que_estaba !== 'undefined' && resp.estado_en_el_que_estaba.length > 0) {
                    //
                    $("#ocultar_con_existencia").css('display', 'none');
                    $("#informacion_del_cambio").css('display', 'block');
                    $("#input_checked").css('display', 'block');
                    document.getElementById('estado_actual').append(resp.estado_que_se_cambio);
                    document.getElementById('estado_actual').value = resp.estado_que_se_cambio;
                    document.getElementById('de').append(resp.estado_en_el_que_estaba);
                    document.getElementById('de').value = resp.estado_en_el_que_estaba;
                    document.getElementById('a').append(resp.estado_que_se_cambio);
                    document.getElementById('a').value = resp.estado_que_se_cambio;
                    document.getElementById('motivo').value = resp.motivo;
                    console.log("Con Cambios de Estados");
                } else {
                    //
                    $("#ocultar_con_existencia").css('display', 'block');
                    document.getElementById('estado_actual').append(resp.ultimos_registros[0].estado);
                    document.getElementById('estado_actual').value = resp.ultimos_registros[0].estado;
                    console.log("Sin cambios de Estados");
                }
            } else {
                $('#tiempo_cambio_estado_solicitud').css('display', 'none');
                $("#tiempo_cambio_estado_solicitud_false").css('display', 'block');
                console.log(resp.mensaje);
            }
        }

    });
});
const habilitarCampos = document.getElementById('habilitarCampos');
habilitarCampos.addEventListener('change', function () {
    // console.log("se hizo click");
    let ultimo_historico = document.getElementById('ocultar_con_existencia');
    let grupo_de_los_ultimos_cambios = document.getElementById('informacion_del_cambio');
    let id = document.getElementById('solicitud_id_actual').value;
    if (habilitarCampos.checked) {
        var data = {
            'id': id,
            _token: $('input[name="_token"]').val()
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: ruta_avance,
            type: "POST",
            data: data,
            dataType: 'json',
            cache: false,
            success: function (resp) {
                document.getElementById('estado_actual').append(resp.ultimos_registros[0].estado);
                document.getElementById('estado_actual').value = resp.ultimos_registros[0].estado;
                document.getElementById('motivo').value = '';
            }
        });
        ultimo_historico.style.display = 'block';
        grupo_de_los_ultimos_cambios.style.display = 'none';
    } else {
        var data = {
            'id': id,
            _token: $('input[name="_token"]').val()
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: ruta_avance,
            type: "POST",
            data: data,
            dataType: 'json',
            cache: false,
            success: function (resp) {
                document.getElementById('estado_actual').append(resp.estado_que_se_cambio);
                document.getElementById('estado_actual').value = resp.estado_que_se_cambio;
                document.getElementById('motivo').value = resp.motivo;
            }
        });
        ultimo_historico.style.display = 'none';
        grupo_de_los_ultimos_cambios.style.display = 'block';
    }
});

// aprobar solicitud
document.querySelector('.boton-aprobar').addEventListener('click', (e) => {
    e.preventDefault();
    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Desea aprobar esta solicitud?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aprobar',
        cancelButtonText: 'cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            const enlace = typeof e.target.href === 'undefined' ? e.target.parentElement.href : e.target.href;
            window.location = enlace;
        }
    });
});

function mostrarPDF(url) {
    let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
    let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if (es_chrome) {
        var iframe = document.createElement("iframe");
        iframe.style.display = "none";
        iframe.src = url;
        document.body.appendChild(iframe);
        iframe.focus();
        iframe.contentWindow.print();
        console.log(iframe.contentWindow)
    } else if (es_firefox) {
        console.log('El navegador es Firefox');
        var win = window.open(url, "_blank");
        win.onload = function () {
            win.print();
        }
        win.focus();
    } else {
        var win = window.open(url, "_blank");
        win.focus();
    }
}
