$(document).on('click', "#archivos_solicitud", function() {
    $('#show_vista_archivos').modal('hide');
    let id = $(this).data('id');
    let num = 1;
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    var text = document.createTextNode("No existen archivos relacionados a esta solicitud");
    var sin_datos = $('#sin_datos');
    $("#datos_modal_archivos").empty();
    $("#solicitud_id").empty();
    $("#sin_datos").empty();
    $('#id_eliminar').empty();
    $('#ingresar_nuevo').css('display','none');
    $('#tiempo_carga').css('display', 'block');
    $('.mostrar_guardar').css('display', 'none');
    $("#myCheck:checkbox:checked").prop("checked", false);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var id_documento = document.getElementById('solicitud_id').append(id);
    var id_documento = document.getElementById('id_eliminar').append(id);
    var id_documento = document.getElementById('solicitud_id').value=id;
    var id_documento = document.getElementById('id_eliminar').value=id;
    $.ajax({
        url: ruta,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            $('#tiempo_carga').css('display', 'none');
            $(".datos_modal_archivos").css('display', 'block');
            $('.mostrar_guardar').css('display', 'block');
            if(resp.data.length == 0){
                sin_datos.css('display', 'block');
                document.getElementById('sin_datos').appendChild(text);
                // console.log('sin datos');
            } else {
                // console.log('con datos');
                $("#sin_datos").css('display', 'none');
                $('.datos_modal_archivos').append("<thead>"+
                    "<tr>"+
                        "<th scope='col'>#</th>"+
                        "<th scope='col'>Nombre del Archivo</th>"+
                        "<th scope='col'>Descripcion del Archivo</th>"+
                        "<th scope='col'>Revisar</th>" +
                    "<tr>"+
                "</thead>");
                resp.data.forEach(element => {
                    $(".datos_modal_archivos").append("<tr>"+
                        "<td scope='row'>" + num + "</td>" +
                        "<td scope='row'>" + element.nombre + "</td>" +
                        "<td scope='row' style='max-width: 400px; overflow-wrap: break-word;'>" + element.desc + "</td>" +
                        "<td scope='row' >" + "<button type='button' id='impresion' data-id=" + element.id_as + " class='btn btn-primary'> <i class='fa fa-eye'></i>Ver</button>" +
                        "<button type='button' id='eliminar' data-id=" + element.id_as + " class='btn btn-danger' data-toggle='modal' data-target='#eliminacion_archivos'> <i class='fas fa-trash'></i>Borrar</button>" + "</td>" +
                    "</tr>");
                    num++
                });
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
});


$(document).on('click', "#impresion", function() {
    $('#myModal').modal('hide');
    $('#show_vista_archivos').modal('hide');

    let id = $(this).data('id');
    var data = {
        'id': id,
        _token: $('input[name="_token"]').val()
    };
    var asset = ruta_asset;
    console.log(asset);
    $("#datos_modal_archivos").empty();
    $("#impresion_archivos_solicitud").empty();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // var text = document.createTextNode(id);
    // document.getElementById('impresion_archivos_solicitud').appendChild(text);
    var id_e = document.getElementById('impresion_archivos_solicitud').append(id);
    var id_e = document.getElementById('impresion_archivos_solicitud').value=id;
    var frame = document.getElementById("iframe");
    $.ajax({
        url: ruta2,
        type: "POST",
        data: data,
        dataType: 'json',
        cache: false,
        success: function (resp) {
            console.log('Nombre del Archivo',resp.data);

            let nuevo_iframe = asset+'/'+resp.data;
            console.log(nuevo_iframe);
            let es_chrome = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
            let es_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
            if (es_chrome) {
                $('#show_vista_archivos').modal('hide');
                var iframe = document.createElement("iframe");
                iframe.style.display = "none";
                iframe.src = nuevo_iframe;
                document.body.appendChild(iframe);
                iframe.focus();
                iframe.contentWindow.print();
                console.log(iframe.contentWindow)
            } else if (es_firefox) {
                console.log('El navegador es Firefox');
                window.location.href = nuevo_iframe;
            } else {
                $('#show_vista_archivos').modal('hide');
                var win = window.open(nuevo_iframe, "_blank");
                win.focus();
            }
            // document.getElementById('imagen').src=nuevo_iframe;
        }

    });

});

function convertirPDF() {
    const element = document.querySelector('#respuesta');
    element.focus()
    mostrarPDF(element.innerHTML);
}

$(document).on('click', '#eliminar', function () {
    $('#show_vista_archivos').modal('hide');
    $('#myModal').modal('hide');
    let eliminacion_id = $(this).data('id');
    var data = {
        'id': eliminacion_id,
        _token: $('input[name="_token"]').val()
    };
    $("#datos_modal_archivos").empty();
    $('#id_para_eliminar').empty();
    var id_eliminar = document.createTextNode(eliminacion_id);
    document.getElementById('id_para_eliminar').appendChild(id_eliminar);
    var id_e = document.getElementById('id_eliminar').append(eliminacion_id);
    var id_e = document.getElementById('id_eliminar').value=eliminacion_id;
    var id_e = document.getElementById('id_eliminar').innerHTML=eliminacion_id;
})

$(document).on('click', "#myCheck", function() {
    var checkBox = document.getElementById("myCheck");
    if (checkBox.checked == true){
        $('#ingresar_nuevo').css('display','block');
    } else {
        $('#ingresar_nuevo').css('display','none');
    }
});

$(document).on('click', "#agregar_nuevo_archivo", function() {
    e.preventDefault();
    Swal.fire({
        title: '¿Está seguro?',
        text: "¿Desea aprobar esta solicitud?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aprobar',
        cancelButtonText: 'cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            const enlace = typeof e.target.href === 'undefined' ? e.target.parentElement.href : e.target.href;
            window.location = enlace;
        }
    });
});
