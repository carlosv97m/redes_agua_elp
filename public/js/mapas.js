
var baseUrl = "https://gis.sapaleipa-services.net/geoserver/elapas/wms";
var baseUrlDatos = "http://100.100.100.114:8080/geoserver/elapas/ows";
var acceso = false;
var map;
var streets;
var openStreetMap;
var labels;
//Limite de Sucre
var mapAreaConcesion;

// REDES ALCANTARILLADO
// var mapTuberiasAguaPotable;
// var mapDisritosHidrometricos;
// var mapCuencas;
// var mapDistritosMunicipales;
//MAPAS
var map_ap_tuberia;
// var map_alc_embovedado;
// var map_alc_colector;
// var map_alc_camara;
// var map_ap_hidrante;
var map_ap_tanque;
// var map_ap_valvula;
var mapAmpliaciones;
// ITEMS
var drawnItems;
var ap_tuberia_items;
// var alc_embovedado_items;
// var alc_colector_items;
// var alc_camara_items;
// var ap_hidrante_items;
var ap_tanque_items;
// var ap_valvula_items;
var concesionItems;

var controles;
// Check areas

// var MyCustomMarker = L.Icon.extend({
//     options: {
//         iconAnchor: new L.Point(12, 12),
//         iconSize: new L.Point(24, 24),
//         iconUrl: camara_icono
//     }
// });
var MyMarkerLocation = L.Icon.extend({
    options: {
        iconAnchor: new L.Point(12, 41),
        iconSize: new L.Point(25, 41),
        iconUrl: ubicacion_icono
    }
});

L.Polyline = L.Polyline.include({
    getDistance: function () {
        // distance in meters
        var mDistanse = 0,
            length = this._latlngs.length;
        for (var i = 1; i < length; i++) {
            mDistanse += this._latlngs[i].distanceTo(this._latlngs[i - 1]);
        }
        // optional
        return Math.round(mDistanse);
    }
});

function initEditMap(x_aprox, y_aprox) {
    map = null;
    iniciarLayers(x_aprox, y_aprox, 'actualizar');

}
function initMap(x_aprox, y_aprox, opcion = 'mapa') {
    map = null;
    iniciarLayers(x_aprox, y_aprox, opcion);

}

function mapTools(lat, long) {
    var searchControl = L.esri.Geocoding.geosearch({
        position: 'topright',
        useMapBounds: false,
        placeholder: 'Ej: Junín',
        title: 'Buscar una dirección',
        zoomToResult: false,
        // providers: [
        //     L.esri.Geocoding.featureLayerProvider({
        //         label: 'Dirección y Barrio',
        //         url: baseUrl + 'MapaBaseWebMercator/MapServer/1',
        //         searchFields: ['DIRECCION']
        //     })
        // ]
    }).addTo(map);


    L.easyButton('fa-map-marker', function () {
        var sucre = [lat, long];
        map.setView(sucre, 18);
    }).addTo(map);

    var results = new L.LayerGroup().addTo(map);

    searchControl.on('results', function (data) {
        results.clearLayers();
        results.addLayer(L.marker(data.results[0].latlng));
        results.eachLayer(function (layer) {
            layer.bindPopup('<strong>Dirección: </strong>' + data.results[0].text);
        });
        map.setView(data.results[0].latlng, 18);
    });
}

function mostrarTabla(flag, distance = false) {
    $('#mostrar_mapa').css('display', 'none');
    $('.float-right').css('display', 'none');
    if (flag) {
        let contenedorMapa = $("#contenedor-mapa");
        if (acceso) {
            let mapa = document.createElement("div");
            mapa.setAttribute("id", "map");
            contenedorMapa.append(mapa);
        }
        $("#contenedor-tabla").hide();
        contenedorMapa.show();
        acceso = true;
    } else {
        $("#map").remove();
        $("#contenedor-tabla").show();
        $("#contenedor-mapa").hide();
        $('.float-right').css('display', 'block');
        if (distance) calcularDistancia();
    }
}


function drawMapa(flag) {
    let lat = document.getElementById('x_exact').value;
    let long = document.getElementById('y_exact').value;
    mostrarTabla(flag);

    iniciarLayers(lat, long, 'editar');

}



function bindPopup(layer) {
    let props = {}
    if (features.has(layer._leaflet_id)) {
        props = features.get(layer._leaflet_id).properties
    } else {
        props = featuresDB.get(layer._leaflet_id).properties
    }

    let table = ''
    for (var key in props) {
        if (!String(key).startsWith('_')) {
            table += '<tr><th><input type="text" value="' + key + '"' + ' /></th>' +
                '<td><input type="text" value="' + props[key] + '"' + ' /></td></tr>'
        }
    }
    content = '<table class="tablaPopup">' + table + '</table>' +
        '<div class="popupButtons">' +
        '<button class="guardar">Guardar</button> ' +
        '<button class="cancelar">Cancelar</button>' +
        '</div>'
    layer.bindPopup(L.popup({
        closeButton: false,
        maxWidth: 500,
        maxHeight: 400,
        autoPanPadding: [5, 45]
    }, layer).setContent(content))

    layer.on('popupopen', popupOpen)
}

function cargarTuberias() {


    map_ap_tuberia = L.tileLayer.betterWms(baseUrl, {
        layers: 'ap_tuberia',
        transparent: true,
        format: 'image/png'
    });
    // map_alc_camara = L.tileLayer.betterWms(baseUrl, {
    //     layers: 'alc_camara',
    //     transparent: true,
    //     format: 'image/png'
    // })
    // map_alc_colector = L.tileLayer.betterWms(baseUrl, {
    //     layers: 'alc_colector',
    //     transparent: true,
    //     format: 'image/png'
    // });
    // map_alc_embovedado = L.tileLayer.betterWms(baseUrl, {
    //     layers: 'alc_embovedado',
    //     transparent: true,
    //     format: 'image/png'
    // });
    // map_ap_hidrante = L.tileLayer.betterWms(baseUrl, {
    //     layers: 'ap_hidrante',
    //     transparent: true,
    //     format: 'image/png'
    // });
    map_ap_tanque = L.tileLayer.betterWms(baseUrl, {
        layers: 'ap_tanque',
        transparent: true,
        format: 'image/png'
    });
    // map_ap_valvula = L.tileLayer.betterWms(baseUrl, {
    //     layers: 'ap_valvula',
    //     transparent: true,
    //     format: 'image/png'
    // });


    // console.log(aux,'hola xD')

    // map_ap_tuberia.bindPopup(function (layer) {
    //     console.log("hola ap tuberia")
    //     var material = "hola";
    //     // if (layer.feature.properties.MATERIAL === 1) {
    //     //     material = 'PVC';
    //     // } else if (layer.feature.properties.MATERIAL === 2) {
    //     //     material = 'FF';
    //     // } else if (layer.feature.properties.MATERIAL === 3) {
    //     //     material = 'FG';
    //     // } else if (layer.feature.properties.MATERIAL === 4) {
    //     //     material = 'HDPE';
    //     // }
    //     return L.Util.template('<strong>ID: </strong>{OBJECTID}<br><strong>Codigo de Catastro: </strong>{CodigoCatastro}<br><strong>Material: </strong>' + material + '<br><strong>Diametro: </strong>{Diametro}<br><strong>Nombre de Red: </strong>{NombreRed}', layer.feature.properties);
    // });


}

function cargarConcesion() {
    mapAreaConcesion = L.esri.featureLayer({
        url: 'http://100.100.100.126:6080/arcgis/rest/services/Mapas/MapaBaseWebMercator/MapServer/3'
    });
}

function iniciarLayers(lat, long, opcion, datos = null) {
    // console.log(document.getElementById('obtenerAmpliaciones').value)
    if (opcion === 'actualizar') {
        cargarLayersActualizar(lat, long);
    } else if (opcion === 'mostrar') {
        cargarLayers(datos, lat, long);
    } else {
        $.get(document.getElementById('obtenerAmpliaciones').value, function (data) {
            if (data != null) {
                data = JSON.parse(data.ampliacion);
            }
            if (opcion === 'editar') {
                cargarLayersEditar(data, lat, long);
            } else if (opcion === 'mapa') {
                // console.log(data, 'ja')
                cargarLayers(data, lat, long);
            }
        });
    }


}

function cargarLayersEditar(data, lat, long) {
    cargarConcesion();
    cargarTuberias();
    console.log()
    mapAmpliaciones = L.geoJSON(data, {
        style: function (feature) {
            return { color: '#00ff00' }

        },


    });
    // dibujarCamaras(mapAmpliaciones)
    console.log('cargar layers editar', mapAmpliaciones)
    drawnItems = L.featureGroup();
    addNonGroupLayers(mapAmpliaciones, drawnItems);
    ap_tuberia_items = L.featureGroup([map_ap_tuberia]);
    // alc_camara_items = L.featureGroup([map_alc_camara]);
    // alc_colector_items = L.featureGroup([map_alc_colector]);
    // alc_embovedado_items = L.featureGroup([map_alc_embovedado]);
    // ap_hidrante_items = L.featureGroup([map_ap_hidrante]);
    ap_tanque_items = L.featureGroup([map_ap_tanque]);
    // ap_valvula_items = L.featureGroup([map_ap_valvula]);
    concesionItems = L.featureGroup([mapAreaConcesion]);


    openStreetMap = L.tileLayer('http://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
        maxZoom: 18,
        maxNativeZoom: 18,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    const mapasBase = {
        "Google": openStreetMap
    }
    const capasLineas = {
        "ampliaciones": drawnItems,
        "area_de_concesion": concesionItems,
        "ap_tuberia": ap_tuberia_items,
        // "alc_camara": alc_camara_items,
        // "alc_colector": alc_colector_items,
        // "alc_embovedado": alc_embovedado_items,
        // "ap_hidrante": ap_hidrante_items,
        "ap_tanque": ap_tanque_items,
        // "ap_valvula": ap_valvula_items

    }
    map = L.map('map', {
        center: { lat: lat, lng: long },
        zoom: 18,
        maxZoom: 18,
        zoomControl: false,
        layers: [openStreetMap, drawnItems],
        fullscreenControl: true
    })

    controles = L.control.layers(
        mapasBase,
        capasLineas, { position: 'topright', collapsed: true }
    )
    controles.addTo(map)


    map.addControl(new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
        },
        draw: {
            rectangle: false,
            polygon: false,
            circle: false,
            // marker: {
            //     icon: new MyCustomMarker()
            // },
            marker:false,
            circlemarker: false,

        }

    }))


    map.on(L.Draw.Event.CREATED, function (e) {
        var type = e.layerType,
            layer = e.layer;
        if (type === 'polyline') {
            console.log(layer);
            layer.options.color = '#00ff00'
            layer.options.opacity = 1.0;
            drawnItems.addLayer(layer);
            guardarcambios('crear');
            console.log(layer.getDistance())
        }
        // if (type === 'marker') {
        //     console.log(layer);
        //     layer.options.color = '#FF00BD'
        //     layer.options.opacity = 1.0;
        //     drawnItems.addLayer(layer);
        //     guardarcambios('crear');
        //     // console.log(layer.getDistance())
        // }

        console.log(e, 'crear botones')
        // if (type === 'point') {
        //     console.log(layer);
        //     layer.options.color = '#FF00BD'
        //     layer.options.opacity = 1.0;
        //     drawnItems.addLayer(layer);
        //     // guardarcambios('crear');
        //     // console.log(layer.getDistance())
        // }
    });

    map.on('draw:edited', function (e) {
        var layers = e.layers;
        layers.eachLayer(function (layer) {
            guardarcambios('editar');
        });
    });

    map.on('draw:deleted', function (e) {
        var layers = e.layers;
        layers.eachLayer(function (layer) {
            guardarcambios('eliminar');
        });
    });


    L.marker([lat, long], { icon: new MyMarkerLocation() }).addTo(map);
    console.log(map)


    // tuberiasItems.addTo(map);
    // AQUI CARGAR LOS LAYERS


    /* capasMapa(); */

    mapTools(lat, long);

}

function cargarLayers(data, lat, long) {
    cargarConcesion();
    cargarTuberias();

    mapAmpliaciones = L.geoJSON(data, {
        style: function (feature) {
            // console.log(new MyCustomMarker(), 'cargarLAyers 1')
            return {
                color: '#00ff00',
                weight: 6,
            }


        },

    });
    console.log(mapAmpliaciones, 'cargarLAyers 1')
    // dibujarCamaras(mapAmpliaciones);
    drawnItems = L.featureGroup();
    addNonGroupLayers(mapAmpliaciones, drawnItems);
    ap_tuberia_items = L.featureGroup([map_ap_tuberia]);
    // alc_camara_items = L.featureGroup([map_alc_camara]);
    // alc_colector_items = L.featureGroup([map_alc_colector]);
    // alc_embovedado_items = L.featureGroup([map_alc_embovedado]);
    // ap_hidrante_items = L.featureGroup([map_ap_hidrante]);
    ap_tanque_items = L.featureGroup([map_ap_tanque]);
    // ap_valvula_items = L.featureGroup([map_ap_valvula]);
    concesionItems = L.featureGroup([mapAreaConcesion]);


    openStreetMap = L.tileLayer('http://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
        maxZoom: 18,
        maxNativeZoom: 18,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    const mapasBase = {
        "Google": openStreetMap
    }
    const capasLineas = {
        "ampliaciones": drawnItems,
        "area_de_concesion": concesionItems,
        "ap_tuberia": ap_tuberia_items,
        // "alc_camara": alc_camara_items,
        // "alc_colector": alc_colector_items,
        // "alc_embovedado": alc_embovedado_items,
        // "ap_hidrante": ap_hidrante_items,
        "ap_tanque": ap_tanque_items,
        // "ap_valvula": ap_valvula_items
    }
    map = L.map('map', {
        center: { lat: lat, lng: long },
        zoom: 18,
        maxZoom: 18,
        zoomControl: false,
        layers: [openStreetMap, drawnItems],
        fullscreenControl: true
    })

    L.control.layers(
        mapasBase,
        capasLineas, { position: 'topright', collapsed: true }
    ).addTo(map)

    L.marker([lat, long], { icon: new MyMarkerLocation() }).addTo(map);


    /* capasMapa(); */

    mapTools(lat, long);

}

function cargarLayersActualizar(lat, long) {
    console.log("hola ")
    let options = {};
    cargarConcesion();
    cargarTuberias();
    drawnItems = L.featureGroup();
    ap_tuberia_items = L.featureGroup([map_ap_tuberia]);
    // alc_camara_items = L.featureGroup([map_alc_camara]);
    // alc_colector_items = L.featureGroup([map_alc_colector]);
    // alc_embovedado_items = L.featureGroup([map_alc_embovedado]);
    // ap_hidrante_items = L.featureGroup([map_ap_hidrante]);
    ap_tanque_items = L.featureGroup([map_ap_tanque]);
    // ap_valvula_items = L.featureGroup([map_ap_valvula]);
    concesionItems = L.featureGroup([mapAreaConcesion]);




    openStreetMap = L.tileLayer('http://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
        maxZoom: 18,
        maxNativeZoom: 18,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    const mapasBase = {
        "Google": openStreetMap
    }
    const capasLineas = {
        "ubicación": drawnItems,
        "area_de_concesion": concesionItems,
        "ap_tuberia": ap_tuberia_items,
        // "alc_camara": alc_camara_items,
        // "alc_colector": alc_colector_items,
        // "alc_embovedado": alc_embovedado_items,
        // "ap_hidrante": ap_hidrante_items,
        "ap_tanque": ap_tanque_items,
        // "ap_valvula": ap_valvula_items

    }
    console.log(capasLineas);
    if (lat != -19.034432 && long != -65.264812) {
        options = {
            center: { lat: lat, lng: long },
            maxZoom: 18,
            zoom: 18,
            zoomControl: false,
            layers: [openStreetMap, drawnItems],
            fullscreenControl: true
        }
    } else {
        options = {
            center: { lat: -19.0429, lng: -65.2554 },
            zoom: 15,
            maxZoom: 18,
            zoomControl: false,
            layers: [openStreetMap, drawnItems],
            fullscreenControl: true
        }
    }

    map = L.map('map', options);

    L.control.layers(
        mapasBase,
        capasLineas, { position: 'topright', collapsed: true }
    ).addTo(map)

    map.addControl(new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
        },
        delete: {
            disabled: true,
        },
        draw: {
            rectangle: false,
            polygon: false,
            circle: false,
            polyline: false,
            circlemarker: false

        }

    }))

    map.on(L.Draw.Event.CREATED, function (e) {
        var type = e.layerType,
            layer = e.layer;
        if (type === 'marker') {
            drawnItems.clearLayers();
            drawnItems.addLayer(layer);
            document.getElementById('x_aprox').value = layer._latlng.lat;
            document.getElementById('y_aprox').value = layer._latlng.lng;
        }
    });

    map.on('draw:edited', function (e) {
        var layers = e.layers;
        layers.eachLayer(function (layer) {
            document.getElementById('x_aprox').value = layer._latlng.lat;
            document.getElementById('y_aprox').value = layer._latlng.lng;
        });
    });

    map.on('draw:deleted', function (e) {
        var layers = e.layers;
        layers.eachLayer(function (layer) {
            document.getElementById('x_aprox').value = lat;
            document.getElementById('y_aprox').value = long;
        });
    });

    if (lat != -19.034432 && long != -65.264812) {
        var marcador = L.marker([lat, long], { icon: new MyMarkerLocation() })
        drawnItems.addLayer(marcador)
    }
    // dibujarCamaras(mapAmpliaciones)


    /* capasMapa(); */

    mapTools(lat, long);





}



function addNonGroupLayers(sourceLayer, targetGroup) {
    if (sourceLayer instanceof L.LayerGroup) {
        sourceLayer.eachLayer(function (layer) {
            addNonGroupLayers(layer, targetGroup);
        });
    } else {
        targetGroup.addLayer(sourceLayer);
    }
}

function guardarcambios(cadena) {
    const layerJSON = drawnItems.toGeoJSON();
    let formData = new FormData(document.getElementById('formAmpliaciones'));
    formData.append('ampliaciones', JSON.stringify(layerJSON));
    $.ajax({
        url: document.querySelector('#formAmpliaciones').action,
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
        }
    });
}

function calcularDistancia() {

    $.get(document.getElementById('obtenerAmpliaciones').value, function (data) {
        let distancia = 0;
        // let num_camaras = 0;
        if (data != null) {
            data = JSON.parse(data.ampliacion);
            var sourceLayer = L.geoJSON(data);
            console.log(camara_icono, 'hola');
            console.log(sourceLayer, 'hoaa');
            sourceLayer.eachLayer(function (layer) {
                if (layer.feature.geometry.type == "LineString") {
                    let linea = L.polyline(layer._latlngs);
                    distancia = distancia + linea.getDistance();
                    document.getElementById('longitud_in').value = distancia;
                    document.getElementById('longitud_in').placeholder = `Longitud: ${distancia}`;
                }
                // if (layer.feature.geometry.type == "Point") {
                //     num_camaras++
                //     // document.getElementById('longitud_in').placeholder = `Longitud: ${distancia}`;
                // }

            });
            // document.getElementById('cantidad_camaras').value = num_camaras;
        }
    });
}

function downloadMap(caption) {
    ocultarBotones(true);
    // console.log(map._container.width)
    var downloadOptions = {
        container: map._container,
        caption: {
            text: caption,
            font: '30px Arial',
            fillStyle: 'black',
            position: [100, 200]
        },
        exclude: ['.leaflet-control-zoom', '.leaflet-control-attribution'],
        format: 'image/png',
        fileName: 'Map.png'
    };
    var promise = map.downloadExport(downloadOptions);
    var data = promise.then(function (result) {

        document.querySelector('#imgMap').src = result.data;
        document.querySelector('#textMap').value = result.data;
        Swal.fire(
            'Exito!',
            'Captura guardada.',
            'success'
        )
        ocultarBotones(false);
    });
}
// function dibujarCamaras(data) {
//     if (data != null) {
//         data.eachLayer((layer) => {
//             if (layer.feature.geometry.type == "Point") {
//                 console.log(layer, 'poly')
//                 layer.options.icon.options.iconRetinaUrl = 'alc_camara.png';
//                 layer.options.icon.options.iconUrl = 'alc_camara.png';
//                 layer.options.icon.options.iconAnchor = [12, 12];
//                 layer.options.icon.options.iconSize = [24, 24];

//             }
//         })
//     }
// }
function mapLink() {
    let lat = document.getElementById('x_exact').value;
    let lng = document.getElementById('y_exact').value;
    var enlace = "https://maps.google.com/?q=" + lat + "," + lng;
    document.getElementById('ubicacion_geo').value = enlace;
}

function ocultarBotones(flag) {
    const barras = document.querySelectorAll('.leaflet-top');

    if (flag) {
        barras.forEach((clase) => {
            clase.style.display = 'none';
        });
    } else {
        barras.forEach((clase) => {
            clase.style.display = 'block';
        });
    }
}
