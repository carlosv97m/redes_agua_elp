L.TileLayer.BetterWMS = L.TileLayer.WMS.extend({

    onAdd: function (map) {
        // Triggered when the layer is added to a map.
        //   Register a click listener, then do all the upstream WMS things
        L.TileLayer.WMS.prototype.onAdd.call(this, map);
        map.on('click', this.getFeatureInfo, this);
    },

    onRemove: function (map) {
        // Triggered when the layer is removed from a map.
        //   Unregister a click listener, then do all the upstream WMS things
        L.TileLayer.WMS.prototype.onRemove.call(this, map);
        map.off('click', this.getFeatureInfo, this);
    },

    getFeatureInfo: function (evt) {
        // Make an AJAX request to the server and hope for the best
        var url = this.getFeatureInfoUrl(evt.latlng),
            showResults = L.Util.bind(this.showGetFeatureInfo, this);
        $.ajax({
            url: url,
            success: function (data, status, xhr) {
                console.log(data,typeof data)
                var err = typeof data === 'string' ? null : data;
                showResults(err, evt.latlng, data);
            },
            error: function (xhr, status, error) {
                showResults(error);
            }
        });
    },

    getFeatureInfoUrl: function (latlng) {
        // Construct a GetFeatureInfo request URL given a point
        var point = this._map.latLngToContainerPoint(latlng, this._map.getZoom()),
            size = this._map.getSize(),

            params = {
                request: 'GetFeatureInfo',
                service: 'WMS',
                srs: 'EPSG:4326',
                styles: this.wmsParams.styles,
                transparent: this.wmsParams.transparent,
                version: this.wmsParams.version,
                format: this.wmsParams.format,
                bbox: this._map.getBounds().toBBoxString(),
                height: size.y,
                width: size.x,
                layers: this.wmsParams.layers,
                query_layers: this.wmsParams.layers,
                info_format: 'text/javascript'
            };

        params[params.version === '1.3.0' ? 'i' : 'x'] = Math.round(point.x);
        params[params.version === '1.3.0' ? 'j' : 'y'] = Math.round(point.y);

        return this._url + L.Util.getParamString(params, this._url, true);
    },

    showGetFeatureInfo: function (err, latlng, content) {
        let datos = "";
        let contenido = ""
        datos = jsonpToJSON(content);
        console.log(content)
        if (err || datos.features.length == 0) { console.log(err); return; } // do nothing if there's an error
        // console.log(jsonpToJSON(content));
        contenido = mostrarDatos(datos);
        // Otherwise show the content in a popup, or something.
        L.popup({ maxWidth: 800 })
            .setLatLng(latlng)
            .setContent(contenido)
            .openOn(this._map);
    }
});

function jsonpToJSON(jsonp) {
    // Find the index of the first parenthesis and the last parenthesis
    const start = jsonp.indexOf('(');
    const end = jsonp.lastIndexOf(')');

    // Extract the JSON string between the parentheses
    const json = jsonp.substring(start + 1, end);

    // Parse the JSON string
    return JSON.parse(json);
}

function mostrarDatos(datos) {
    // console.log(datos,'holas')
    let cadena = "";
    let llave_feature= null;
    let propiedades = null;
    // console.log(datos,'hola')
    datos.features.forEach(feature => {
        cadena += "<p>"
        cadena += "<b style='font-size: 1.2rem'>"+ feature.id.split('.')[0]+"</b> <br>";
        propiedades = feature.properties;
        llave_feature = Object.keys(feature["properties"])
        llave_feature.forEach(llave => {
            if(propiedades[llave] != null){
                cadena += `<b>${llave}: </b> ${propiedades[llave]}<br>` ;
            }
        })
        cadena += "</p>"
    });
    return cadena;

}

L.tileLayer.betterWms = function (url, options) {
    return new L.TileLayer.BetterWMS(url, options);
};
