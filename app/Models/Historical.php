<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historical extends Model
{
    use HasFactory;
    protected $table = "historicals";
    public function registrar_historico($solicitud_id, $estado, $usuario_id)
    {
        $historical = new Historical();
        $historical->estado       = $estado;
        $historical->usuario_id   = $usuario_id;
        $historical->solicitud_id = $solicitud_id;
        $historical->save();
    }
}
