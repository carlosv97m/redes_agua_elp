<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Informe extends Model
{
    use HasFactory;

    //Relacion uno a muchos
    public function solicitud()
    {
        return $this->belongsTO('App\Models\Solicitud');
    }
    public function materials()
    {
        return $this->belongsToMany('App\Models\Material', 'informe_materials', 'informe_id', 'material_id');
    }
    public function aporte_m_vecinos()
    {
        return $this->hasMany('App\Models\Aporte_m_vecino');
    }
    public function cambio_estado_informe($id, $estado_actual, $estado_a_cambiar, $motivo)
    {
        $find_id = Informe::where('solicitud_id', $id)->select('id')->first();
        $informe_id = Informe::findOrFail($find_id->id);
        $tabla = 'cambio_estado';
        $data = [
            'informe_id' => $informe_id->id,
            'estado_actual' => $estado_actual,
            'estado_a_cambiar' => $estado_a_cambiar,
            'motivo' => $motivo,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        try {
            // Utiliza el constructor de consultas de Laravel para insertar datos
            DB::table($tabla)->insert($data);

            $informe = Informe::findOrFail($informe_id->id);
            $informe->estado_in = $estado_a_cambiar;
            $informe->save();

            return ['success' => true, 'message' => 'Datos insertados correctamente.'];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'Error al insertar datos: ' . $e->getMessage()];
        }
    }
}
