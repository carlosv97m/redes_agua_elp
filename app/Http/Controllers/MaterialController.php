<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Match_;

class MaterialController extends Controller
{

    public function index()
    {
        $materials = Material::all();
        return view('materials.index',compact('materials'));
    }


    public function create()
    {
        return view('materials.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'nombre_material' => 'required|unique:materials|max:255',
        ]);
        $material = new Material();
        $material->codigo = strtoupper($request->codigo);
        $material->nombre_material = strtoupper($request->nombre_material);
        $material->observaciones = strtoupper($request->observaciones);
        $material->cantidad = intval($request->cantidad);
        $material->precio_unitario = $request->precio_unitario;
        $material->estado = $request->estado;
        $material->unidad_med = strtoupper($request->unidad_medida);
        $material->save();
        return redirect()->route('materials.index');
    }


    public function show($id)
    {
        //
    }


    public function edit(Material $material)
    {
        return view('materials.edit',compact('material'));
    }


    public function update(Request $request, Material $material)
    {
        $material->codigo = strtoupper($request->codigo);
        $material->nombre_material = strtoupper($request->nombre_material);
        $material->observaciones = strtoupper($request->observaciones);
        $material->cantidad = intval($request->cantidad);
        $material->precio_unitario = floatVal($request->precio);
        $material->estado = $request->estado;
        $material->save();
        return redirect()->route('materials.index');
    }

    public function destroy( Material $material)
    {
        // $material->estado = "no disponible";
        $material->delete();
        return redirect()->route('materials.index');
    }

}
