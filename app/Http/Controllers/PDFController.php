<?php

namespace App\Http\Controllers;

use App\Models\Informe;
use App\Models\Informe_material;
use App\Models\Solicitud;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\DB;

class PDFController extends Controller
{
    public function PDF(Informe $informe)
    {
        //$pdf = PDF::loadview('PDF/informe',compact('informe'));
        //return $pdf->stream('Informe.pdf');
        // return $informe;
        if ($informe->imagen_amp != null) {

            return view('PDF.informe', compact('informe'));
        } else {
            return view('PDF.informe', compact('informe'));
            // $pdf = PDF::loadview('PDF/informe', compact('informe'));
            // return $pdf->stream('Informe.pdf');
        }
    }

    public function PDF_informe_material(Informe $informe)
    {

        $inspector = DB::table('users')
            ->join('cronogramas', 'users.id', '=', 'cronogramas.user_id')
            ->join('solicituds', 'solicituds.id', '=', 'cronogramas.solicitud_id')
            ->join('informes', 'informes.solicitud_id', '=', 'solicituds.id')
            ->where('informes.id', $informe->id)
            ->select('users.name as nombre_inspector')
            ->get();

        $materiales = DB::table('informe_materials')
            ->join('materials', 'informe_materials.material_id', '=', 'materials.id')
            ->select(
                'informe_materials.cantidad as cantidad',
                'informe_materials.u_medida as u_medida',
                'informe_materials.observador as observador',
                'informe_materials.precio_unitario as precio_unitario',
                'informe_materials.sub_total as sub_total',
                'materials.nombre_material as nombre_material'
            )
            ->where('informe_materials.informe_id', $informe->id)
            ->get();
        $mano_obra = DB::table('mano_obras')
            ->join('ejecucions', 'ejecucions.id', '=', 'mano_obras.ejecucion_id')
            ->join('actividad_mano_obras', 'actividad_mano_obras.id', '=', 'mano_obras.actividad_id')
            ->select(
                'actividad_mano_obras.descripcion as descripcion',
                'actividad_mano_obras.unidad_medida as unidad',
                'mano_obras.cantidad as cantidad',
                'mano_obras.precio_uni as precio_unitario',
                'mano_obras.observador as observador'
            )
            ->where('ejecucions.informe_id', $informe->id)
            ->get();

        // $pdf = PDF::loadview('PDF/informe_material', compact('informe', 'materiales', 'inspector', 'mano_obra'));
        // return $pdf->stream('Informe_material.pdf');

        return view('PDF.informe_material', compact('informe', 'materiales', 'inspector', 'mano_obra'));

        // return $informe;
    }

    public function PDF_informe_descargo_material(Informe $informe)
    {
        $inspector = DB::table('users')
            ->join('cronogramas', 'users.id', '=', 'cronogramas.user_id')
            ->join('solicituds', 'solicituds.id', '=', 'cronogramas.solicitud_id')
            ->join('informes', 'informes.solicitud_id', '=', 'solicituds.id')
            ->join('ejecucions', 'ejecucions.informe_id', '=', 'informes.id')
            ->where('informes.id', $informe->id)
            ->select(
                'users.name as nombre_inspector',
                'solicituds.fecha_sol as fecha_sol',
                'solicituds.zona_sol as zona_sol',
                'solicituds.calle_sol as calle_sol',
                'ejecucions.fecha_ejecutada as fecha_ejecutada'
            )
            ->first();

        $materiales = DB::table('informe_materials')
            ->join('materials', 'informe_materials.material_id', '=', 'materials.id')
            ->select(
                'informe_materials.cantidad as cantidad',
                'informe_materials.u_medida as u_medida',
                'informe_materials.observador as observador',
                'informe_materials.precio_unitario as precio_unitario',
                'informe_materials.sub_total as sub_total',
                'materials.nombre_material as nombre_material'
            )
            ->where('informe_materials.informe_id', $informe->id)
            ->get();
        $mano_obra = DB::table('mano_obras')
            ->join('ejecucions', 'ejecucions.id', '=', 'mano_obras.ejecucion_id')
            ->join('actividad_mano_obras', 'actividad_mano_obras.id', '=', 'mano_obras.actividad_id')
            ->select(
                'actividad_mano_obras.descripcion as descripcion',
                'actividad_mano_obras.unidad_medida as unidad',
                'mano_obras.cantidad as cantidad',
                'mano_obras.precio_uni as precio_unitario',
                'mano_obras.observador as observador',
                'mano_obras.alto as alto',
                'mano_obras.ancho as ancho',
                'mano_obras.largo as largo'
            )
            ->where('ejecucions.informe_id', $informe->id)
            ->get();

        // $pdf = PDF::loadview('PDF.descargo_materiales_reporte', compact('informe', 'materiales', 'inspector', 'mano_obra'));
        // return $pdf->stream('Reporte_Descargo_Material.pdf');

        return view('PDF.descargo_materiales_reporte', compact('informe', 'materiales', 'inspector', 'mano_obra'));
    }

    public function PDF_pedido(Informe $informe)
    {
        $materiales = DB::table('informe_materials')
            ->join('materials', 'informe_materials.material_id', '=', 'materials.id')
            ->select(
                'informe_materials.cantidad as cantidad',
                'informe_materials.u_medida as u_medida',
                'informe_materials.sub_total as sub_total',
                'materials.nombre_material as nombre_material',
                'materials.codigo as codigo'
            )
            ->where('informe_materials.informe_id', $informe->id)
            ->where('informe_materials.observador', 'Elapas')
            ->get();
        $inspector = DB::table('informes')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'cronogramas.user_id', '=', 'users.id')
            ->select('users.name as name', 'informes.estado_in as estado')
            ->where('informes.id', $informe->id)
            ->first();
        $jefe_r = DB::table('users')
            ->where('users.tipo_user', 'Jefe de red')
            ->select('users.name as name')
            ->first();
        // $materiales=DB::select("SELECT m.nombre_material,im.cantidad,im.u_medida FROM informe_materials im
        //                         INNER JOIN materials m on m.id=im.material_id WHERE im.informe_id=1");
        // $pdf = PDF::loadview('PDF/pedido_material',compact('informe','materiales','inspector','jefe_r'));
        // return $pdf->stream('reporte_pedido.pdf');
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadview('PDF/pedido_material', compact('informe', 'materiales', 'inspector', 'jefe_r'))->stream('reporte_pedido.pdf');
        // return $informe;
    }

    public function PDF_cronograma($fecha_inicio, $fecha_fin, $user_id)
    {
        $cronogramas = DB::table('informes')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->leftJoin('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'cronogramas.user_id', '=', 'users.id')
            ->select(
                'solicituds.id as id_solicitud',
                'solicituds.zona_sol as zona',
                'solicituds.nombre_sol as nombre_sol',
                'solicituds.celular_sol as celular',
                'users.name as name',
                'cronogramas.fecha_inspe as fecha_inspe'
            )
            ->when($fecha_inicio && $fecha_fin, function($query) use ( $fecha_inicio, $fecha_fin){
                $fecha_inicio = $fecha_inicio ." 00:00:00";
                $fecha_fin = $fecha_fin." 23:59:59";
                return $query->whereBetween('cronogramas.fecha_inspe', [$fecha_inicio, $fecha_fin]);
            })
            ->when($user_id, function ($query) use ($user_id) {
                return $query->where('users.id', $user_id);
            })
            ->when(empty($fecha_inicio) && empty($fecha_fin) && empty($user_id), function ($query) {
                return $query->where('cronogramas.estado', 'asignado');
            })
            ->orderBy('cronogramas.fecha_inspe')
            ->orderByRaw('CAST(solicituds.id AS UNSIGNED) DESC')
            ->get();

        $pdf = PDF::loadview('PDF.reporte_cronograma', compact('cronogramas', 'fecha_inicio', 'fecha_fin'));
        return $pdf->stream('cronograma.pdf');
        // return $cronogramas;
    }
    public function PDF_reporte_pedido(Informe $informe)
    {
        $pdf = PDF::loadview('PDF/informe', compact('informe'));
        return $pdf->stream('Informe.pdf');
        // return $informe;
    }

    public function PDF_proyecto(Informe $informe)
    {
        $inspector = DB::table('informes')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'cronogramas.user_id', '=', 'users.id')
            ->select('users.name as nombre')
            ->where('informes.id', $informe->id)
            ->first();
        $materiales = DB::table('informe_materials')
            ->join('materials', 'informe_materials.material_id', '=', 'materials.id')
            ->select(
                'informe_materials.cantidad as cantidad',
                'informe_materials.u_medida as u_medida',
                'informe_materials.observador as observador',
                'informe_materials.precio_unitario as precio_unitario',
                'informe_materials.sub_total as sub_total',
                'materials.nombre_material as nombre_material'
            )
            ->where('informe_materials.informe_id', $informe->id)
            ->get();
        $mano_obra = DB::table('mano_obras')
            ->join('ejecucions', 'ejecucions.id', '=', 'mano_obras.ejecucion_id')
            ->join('actividad_mano_obras', 'actividad_mano_obras.id', '=', 'mano_obras.actividad_id')
            ->select(
                'actividad_mano_obras.descripcion as descripcion',
                'actividad_mano_obras.unidad_medida as unidad',
                'mano_obras.cantidad as cantidad',
                'mano_obras.precio_uni as precio_unitario',
                'mano_obras.observador as observador'
            )
            ->where('ejecucions.informe_id', $informe->id)
            ->get();

        // $pdf = PDF::loadview('PDF/proyecto',compact('informe','inspector'));
        // return $pdf->stream('Informe.pdf');
        return view('PDF/proyecto', compact('informe', 'inspector', 'materiales', 'mano_obra'));
        // return $informe;
    }

    public function generar_reporte_proyectista(Request $request)
    {
        $materiales = [];
        $mano_obras = [];
        //dd($request->mano_obra_check);
        if ($request->material_check != null) {
            $materiales = DB::table('materials')
                ->join('informe_materials', 'informe_materials.material_id', '=', 'materials.id')
                ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                ->join('ejecucions', 'ejecucions.informe_id', '=', 'informes.id')
                ->whereIn('informe_materials.material_id', $request->material_check)
                ->whereBetween('ejecucions.fecha_ejecutada', [$request->fecha_i, $request->fecha_h])
                ->orderBy('informe_materials.material_id')
                ->get();
        }
        if ($request->mano_obra_check != null) {
            $mano_obras = DB::table('actividad_mano_obras')
                ->join('mano_obras', 'mano_obras.actividad_id', '=', 'actividad_mano_obras.id')
                ->join('ejecucions', 'ejecucions.id', '=', 'mano_obras.ejecucion_id')
                ->whereIn('mano_obras.actividad_id', $request->mano_obra_check)
                ->whereBetween('ejecucions.fecha_ejecutada', [$request->fecha_i, $request->fecha_h])
                ->orderBy('mano_obras.actividad_id')
                ->get();
        }
        $fecha_i = $request->fecha_i;
        $fecha_h = $request->fecha_h;
        return response(compact('mano_obras', 'materiales', 'fecha_i', 'fecha_h'));
        // $pdf = PDF::loadview('PDF/informe', compact('informe'));
        //return $pdf->stream('Informe.pdf');
    }

    public function generar_reporte_ampliaciones(Request $request)
    {
        if ($request->user_id == 0) {
            $ampliaciones = DB::table('solicituds')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users', 'users.id', '=', 'cronogramas.user_id')
                ->leftJoin('informes', 'solicituds.id', '=', 'informes.solicitud_id')
                ->leftJoin('ejecucions', 'solicituds.id', '=', 'ejecucions.solicitud_id')
                ->select(
                    'solicituds.id as id_sol',
                    'solicituds.nombre_sol as solicitante',
                    'solicituds.zona_sol as zona',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'users.name as nombre',
                    'informes.fecha_autorizado as fecha_autorizado',
                    'informes.fecha_visto_bueno as fecha_visto_bueno',
                    'informes.estado_in as estado',
                    'informes.longitud_inspector as longitud',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada'

                )
                ->whereBetween('solicituds.fecha_sol', [$request->fecha_i, $request->fecha_h])
                ->get();
        } else {
            $ampliaciones = DB::table('solicituds')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users', 'users.id', '=', 'cronogramas.user_id')
                ->leftJoin('informes', 'solicituds.id', '=', 'informes.solicitud_id')
                ->leftJoin('ejecucions', 'solicituds.id', '=', 'ejecucions.solicitud_id')
                ->select(
                    'solicituds.id as id_sol',
                    'solicituds.nombre_sol as solicitante',
                    'solicituds.zona_sol as zona',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'users.name as nombre',
                    'informes.fecha_autorizado as fecha_autorizado',
                    'informes.fecha_visto_bueno as fecha_visto_bueno',
                    'informes.estado_in as estado',
                    'informes.longitud_inspector as longitud',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada'

                )
                ->where('cronogramas.user_id', $request->user_id)
                ->whereBetween('solicituds.fecha_sol', [$request->fecha_i, $request->fecha_h])
                ->get();
        }


        return response(compact('ampliaciones'));
    }
    public function acta_final($id)
    {
        // echo "ID DE SOLICITUD: ".$id;
        $primera = DB::table('solicituds as s')
            ->join('informes as i', 's.id', '=', 'i.solicitud_id')
            ->join('cronogramas as c', 'c.solicitud_id','=','s.id')
            ->join('ejecucions as e', 'e.solicitud_id', '=', 's.id')
            ->join('users as u','u.id','=','e.user_id')
            ->join('informe_materials as im', 'im.informe_id', '=', 'i.id')
            ->select('e.id as ejecucion_id', 'e.informe_id as informe_id', 'e.fecha_progrmada', 'e.fecha_ejecutada', 'i.diametro_in', 'i.longitud_inspector', 's.calle_sol', 's.zona_sol', 'u.name', DB::raw('SUM(im.cantidad) as total_cantidad_material'), DB::raw('SUM(im.precio_unitario) as total_costo_material'), DB::raw('COUNT(im.id) as total_material'))
            ->where('s.id', '=', $id)
            ->groupBy('im.informe_id')
            ->get();
        // return $primera;
        foreach ($primera as $key) {
            $resultado = DB::table('mano_obras as mo')
            ->join('ejecucions as e', 'e.id', '=', 'mo.ejecucion_id')
            ->where('mo.ejecucion_id', '=' , $key->ejecucion_id)
            ->select(DB::raw('SUM(mo.cantidad) as total_cantidad_obra'), DB::raw('SUM(mo.precio_uni) as total_costo_obra'),DB::raw('COUNT(mo.id) as total_obra'))
            ->first();
            $key->total_obra           = $resultado->total_obra;
            $key->total_cantidad_obra  = $resultado->total_cantidad_obra;
            $key->total_costo_obra     = $resultado->total_costo_obra;
        }
        foreach ($primera as $valor){
            $informes = DB::table('informe_materials')
                ->select(DB::raw('COUNT(informe_materials.id) as total_material_elapas'))
                ->where([
                    ['informe_materials.informe_id', $valor->informe_id],
                    ['informe_materials.observador','=','Elapas']
                ])->first();
            $key->total_material_elapas  = $informes->total_material_elapas;
        }
        foreach ($primera as $value){
            $informes = DB::table('mano_obras')
                ->select(DB::raw('COUNT(mano_obras.id) as total_obra_elapas'))
                ->where([
                    ['mano_obras.ejecucion_id', $value->ejecucion_id],
                    ['mano_obras.observador','=','Elapas']
                ])->first();
            $key->total_obra_elapas  = $informes->total_obra_elapas;
        }

        $acta = collect($primera);
        // return $acta;
        $materiales = DB::table('informe_materials')
            ->join('informes', 'informe_materials.informe_id', '=', 'informes.id')
            ->join('materials', 'informe_materials.material_id', '=', 'materials.id')
            ->select(
                'informe_materials.cantidad as cantidad',
                'informe_materials.u_medida as u_medida',
                'informe_materials.observador as observador',
                'informe_materials.precio_unitario as precio_unitario',
                'informe_materials.sub_total as sub_total',
                'materials.nombre_material as nombre_material'
            )
            ->where('informes.solicitud_id', $id)
            ->get();
        $mano_obra = DB::table('mano_obras')
            ->join('ejecucions', 'ejecucions.id', '=', 'mano_obras.ejecucion_id')
            ->join('actividad_mano_obras', 'actividad_mano_obras.id', '=', 'mano_obras.actividad_id')
            ->select(
                'actividad_mano_obras.descripcion as descripcion',
                'actividad_mano_obras.unidad_medida as unidad',
                'mano_obras.cantidad as cantidad',
                'mano_obras.precio_uni as precio_unitario',
                'mano_obras.observador as observador'
            )
            ->where('ejecucions.solicitud_id', $id)
            ->get();
        $pdf = PDF::loadview('PDF/acta_entrega', compact('acta','materiales','mano_obra'));
        return $pdf->stream('Acta_de_entrega.pdf');
    }
}
