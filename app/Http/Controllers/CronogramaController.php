<?php

namespace App\Http\Controllers;

use App\Models\Cronograma;
use App\Models\Historical;
use App\Models\Informe;
use App\Models\Solicitud;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use Illuminate\Validation\Rule;

class CronogramaController extends Controller
{

    public function index()
    {
        $inspectores = User::where('tipo_user', 'Inspector')->get();
        $solicitud = Solicitud::where('estado_sol', 'aprobado')->get();
        return view('cronograma.index', compact('solicitud', 'inspectores'));
    }

    public function mostrar(Request $request)
    {
        $fecha_inicio = $request->fecha_i;
        $fecha_fin    = $request->fecha_f;
        $user_id      = $request->user_id;
        // $query = "CAST(solicituds.id AS SIGNED) ASC";
        $cronogramas = DB::table('informes')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->leftJoin('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'cronogramas.user_id', '=', 'users.id')
            ->select(
                'solicituds.id as id_solicitud',
                'solicituds.zona_sol as zona',
                'solicituds.nombre_sol as nombre_sol',
                'solicituds.celular_sol as celular',
                'users.name as name',
                'cronogramas.fecha_inspe as fecha_inspe'
            )
            ->when($fecha_inicio && $fecha_fin, function($query) use ( $fecha_inicio, $fecha_fin){
                return $query->whereBetween('cronogramas.fecha_inspe', [$fecha_inicio, $fecha_fin]);
            })
            ->when($user_id, function ($query) use ($user_id) {
                return $query->where('users.id', $user_id);
            })
            ->when(empty($fecha_inicio) && empty($fecha_fin) && empty($user_id), function ($query) {
                return $query->where('cronogramas.estado', 'asignado');
            })
            ->orderByRaw('CAST(solicituds.id AS UNSIGNED) DESC')
            ->get();
        $inspectores = User::where('tipo_user', 'inspector')->get();

        // return $cronogramas;
        if($user_id == 0 || $user_id == NULL){
            $fecha_inicio = $request->fecha_i ? date('Y-m-d', strtotime($request->fecha_i)) : date('Y-m-d');
            $fecha_fin    = $request->fecha_f ? date('Y-m-d', strtotime($request->fecha_f)) : date('Y-m-d');
            $user_id      = $request->user_id ? $request->user_id : 0;
        }
        return view('cronograma.cronograma', compact('cronogramas', 'inspectores', 'fecha_inicio','fecha_fin','user_id'));
    }

    public function show_request(Request $request)
    {
        $request->validate([
            'fecha_i' => 'required|date',
            'fecha_f' => 'required|date',
            'user_id' => 'required'
        ]);
        $cronogramas = DB::table('informes')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'cronogramas.user_id', '=', 'users.id')
            ->select(
                'solicituds.zona_sol as zona',
                'solicituds.nombre_sol as nombre_sol',
                'solicituds.celular_sol as celular',
                'users.name as name',
                'cronogramas.fecha_inspe as fecha_inspe'
            )
            ->where('cronogramas.estado', 'asignado')
            ->whereBetween('cronogramas.fecha_inspe', [$request->fecha_i, $request->fecha_f])
            ->Where('users.id', $request->user_id)
            ->get();
        return response()->json(['mensaje'=>'Datos Encontrados', 'data'=> $cronogramas]);
    }


    public function create()
    {
        return view('cronograma.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'fecha_inspe' => 'required',
            'solicitud_id' => 'required|unique:cronogramas',
        ]);
        $cronograma = new Cronograma();
        $cronograma->user_id = $request->user_id;
        $cronograma->solicitud_id = $request->solicitud_id;
        $cronograma->fecha_inspe = $request->fecha_inspe;
        $cronograma->estado = "asignado";
        $cronograma->save();

        $informe = new Informe();

        $informe->estado_in = 'asignado';
        $informe->fecha_hora_in = $request->fecha_inspe;
        $informe->solicitud_id = $request->solicitud_id;
        $informe->save();

        $solicitud = Solicitud::find($request->solicitud_id);
        $solicitud->estado_sol = 'asignado';
        $solicitud->save();

        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($solicitud->id, 'asignado', $id);
        return redirect()->route('cronograma.index');
        // return $request;
    }

    public function edit(Solicitud $solicitud)
    {
        return view('solicitud.edit', compact('solicitud'));
    }

    public function update(Request $request, Solicitud $solicitud)
    {
        $solicitud->nombre_sol = $request->nombre_sol;
        $solicitud->celular_sol = $request->celular_sol;
        $solicitud->zona_sol = $request->zona_sol;
        $solicitud->calle_sol = $request->calle_sol;
        $solicitud->fecha_sol = $request->fecha_sol;
        $solicitud->estado_sol = $request->estado_sol;
        $solicitud->x_aprox = $request->x_aprox;
        $solicitud->y_aprox = $request->y_aprox;
        $solicitud->save();
        return redirect()->route('solicitud.index');
    }
    public function aprobar(Solicitud $solicitud)
    {
        $solicitud->estado_sol = "aprobado";
        $solicitud->save();
        return redirect()->route('solicitud.index');
        // return $solicitud->estado_sol;
    }
    public function rechazar(Solicitud $solicitud)
    {
        $solicitud->estado_sol = "rechazado";
        $solicitud->save();
        return redirect()->route('solicitud.index');
        // return $solicitud->estado_sol;
    }
    public function destroy(Solicitud $solicitud)
    {
        $solicitud->delete();
        return redirect()->route('solicitud.index')->with('eliminar', 'Ok');
    }
}
