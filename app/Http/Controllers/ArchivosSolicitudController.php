<?php

namespace App\Http\Controllers;

use App\Models\Archivos_solicitud;
use App\Models\Solicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ArchivosSolicitudController extends Controller
{
    public function show(Request $request)
    {
        $archivos = Solicitud::join('archivos_solicituds','solicituds.id','=','archivos_solicituds.solicitud_id')
            ->where('solicituds.id',$request->id)
            ->select('archivos_solicituds.id as id_as', 'archivos_solicituds.descripcion as desc','archivos_solicituds.nombre_archivo as nombre', 'solicituds.id as id')
            ->get();
        return response()->json(['mensaje' => 'Datos encontrados', 'data' => $archivos, 'id'=>$request->id]);
    }
    public function store(Request $request)
    {
        $aux = Auth::user()->tipo_user;
        if($request){
            // verificacion de los datos
            $request->validate([
                'descripcion' => 'required|string','solicitud_id' => 'required|integer',]);
            // armar el archivo subido
            $now = date("d-m-Y H:i:s");
            $name=date( "d-m-Y H:i:s", strtotime($now)).'.pdf';//validar extencion
            $name=str_replace(' ','_',$name);
            $name=str_replace(':','_',$name);
            $name=str_replace('-','_',$name);
            // return $name;
            // guardar la informacion
            $datos = new Archivos_solicitud();
            $datos->descripcion    = $request->descripcion;
            $datos->fecha_archivo  = date('Y-m-d');
            $datos->user_id        = Auth::user()->id;
            $datos->solicitud_id   = $request->solicitud_id;
            $datos->nombre_archivo = $name;
            if ($request->hasFile('archivo_solicitud')) {
                if ($request->file('archivo_solicitud')->isValid()) {
                    $file = $request->file('archivo_solicitud');
                    $datos->nombre_archivo = $name;
                    Storage::disk('archivos_solicitudes')->put($datos->nombre_archivo,  File::get($file));
                }
                $datos->save();
            }
            if ($aux == 'Inspector') {
                return redirect()->route('informes.index');
            } elseif($aux == 'Proyectista'){
                return redirect()->route('proyectos.index');
            } elseif($aux == 'Monitor'){
                return redirect()->route('monitoreo.index');
            }else {
                return redirect()->route('solicitud.index')->with('archivo', 'ok');
            }
        } else {
            return redirect()->route('solicitud.index')->with('archivo', 'no');
        }
    }
    public function destroy(Request  $request)
    {
        // return $request;
        // $id = Archivos_solicitud::findOrFail($request->id_eliminar)->id;
        $creado_por = Archivos_solicitud::findOrFail($request->id_eliminar)->user_id;
        // return Auth::user()->id;
        if( (Auth::user()->id) == $creado_por ){
            $archivos = Archivos_solicitud::findOrFail($request->id_eliminar);
            $archivos->delete();
            return redirect()->route('solicitud.index')->with('archivo_destroy', 'ok');
        } else {
            return redirect()->route('solicitud.index')->with('archivo_destroy', 'no');
        }
    }
    public function mostrar(Request $request)
    {
        $archivos_solicitud = Archivos_solicitud::findOrFail($request->id)->nombre_archivo;
        if(!empty($archivos_solicitud)){
            return response()->json(['mensaje' => 'Archivo encontrado', 'data' => $archivos_solicitud, 'id'=>$request->id]);
        }
        else{
            return response()->json(['mensaje' => 'Sin datos', 'data' => [],'id'=>$request->id ]);
        }
    }
}
?>
