<?php

namespace App\Http\Controllers;

use App\Models\Ejecucion;
use App\Models\Historical;
use Illuminate\Http\Request;
use App\Models\Informe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EjecucionController extends Controller
{
    public function store(Request $request)
    {
        $informe = Informe::find($request->informe_id);
        $informe->estado_in = 'programado';
        $informe->save();

        $ejecucion = new Ejecucion();
        $ejecucion->fecha_progrmada = $request->fecha_programada;
        $ejecucion->informe_id = $request->informe_id;
        $ejecucion->solicitud_id = $request->solicitud_id;
        $ejecucion->user_id = $request->user_id;
        $ejecucion->save();

        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($request->solicitud_id, 'programado', $id);

        return redirect()->route('informes.concluido');
    }

    public function ejecutada($id_ejecucion,  Request $request)
    {
        $informe = Informe::find($request->id_informe);
        $informe->estado_in = "ejecutado";
        $informe->save();

        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($informe->solicitud_id, 'ejecutado', $id);

        $ejecucion = Ejecucion::find($id_ejecucion);
        $ejecucion->fecha_ejecutada = $request->fecha_ejecutada;
        $ejecucion->save();
        $id = Auth::user()->id;
        $aux = Auth::user()->tipo_user;
        if ($aux == "Administrador") {
            return redirect()->route('informes.autorizado')->with('actualizado', 'ok');
        } else {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('ejecucions', 'ejecucions.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'solicituds.zona_sol as zonal_sol',
                    'solicituds.calle_sol as calle_sol',
                    'ejecucions.id as id_ejecucion',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada'
                )
                ->where('cronogramas.user_id', $id)
                ->whereIn('informes.estado_in', ['ejecutandose', 'firmado', 'solicitud de firma'])
                ->get();
            return view('informes.concluido', compact('informes'));
        }
    }
}
