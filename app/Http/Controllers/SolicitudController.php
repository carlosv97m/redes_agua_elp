<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Cronograma;
use App\Models\Ejecucion;
use App\Models\Historical;
use App\Models\Informe;
use App\Models\Informe_material;
use App\Models\Mano_obra;
use App\Models\Solicitud;
use Database\Seeders\Informes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use PDF;
use Dompdf\Dompdf;
use Dompdf\Options;

class SolicitudController extends Controller
{
    public function index()
    {
        $aux = Auth::user()->tipo_user;
        if($aux == 'Administrador') {
            $solicitud = Solicitud::all();
            return view('solicitud.index', compact('solicitud'));
        } else {
            $solicitud = Solicitud::where('estado_sol', 'pendiente')->get();
            return view('solicitud.index', compact('solicitud'));
        }
    }
    public function reject()
    {
        $solicitud = Solicitud::where('estado_sol', 'aplazado')->get();
        return view('solicitud.index', compact('solicitud'));
    }

    public function create()
    {
        return view('solicitud.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nombre_sol' => 'required',
            'celular_sol' => 'required',
            'zona_sol' => 'required',
            'calle_sol' => 'required',
            'x_aprox' => 'required',
            'y_aprox' => 'required',
            'sol_escaneada' => 'required|mimes:pdf|max:10000'
        ]);
        $sol_anterior = DB::table('solicituds')->orderby('created_at', 'DESC')->take(1)->get();;
        $sol = new Solicitud();
        $sol->nombre_sol = strtoupper($request->nombre_sol);
        $sol->celular_sol = $request->celular_sol;
        $sol->zona_sol = strtoupper($request->zona_sol);
        $sol->calle_sol = strtoupper($request->calle_sol);
        $sol->fecha_sol = $request->fecha_sol;
        $sol->estado_sol = $request->estado_sol;
        $sol->x_aprox = $request->x_aprox;
        $sol->y_aprox = $request->y_aprox;
        if ($request->hasFile('sol_escaneada')) {
            if ($request->file('sol_escaneada')->isValid()) {
                $file = $request->file('sol_escaneada');
                $nombre = $sol_anterior->isEmpty() ? 1 : $sol_anterior[0]->id + 1;
                $sol->sol_escaneada = 'solicitud_' . $nombre . '.pdf';
                Storage::disk('solicitudes')->put($sol->sol_escaneada,  File::get($file));
            }
            $sol->save();
        }
        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($sol->id, 'solicitado', $id);
        return redirect()->route('solicitud.index')->with('crear', 'ok');
    }

    public function edit(Solicitud $solicitud)
    {
        return view('solicitud.edit', compact('solicitud'));
    }
    public function update(Request $request, Solicitud $solicitud)
    {
        $request->validate([
            'nombre_sol' => 'required',
            'celular_sol' => 'required',
            'zona_sol' => 'required',
            'calle_sol' => 'required',
            'x_aprox' => 'required',
            'y_aprox' => 'required',
            // 'sol_escaneada' => 'required|mimes:pdf|max:10000'
        ]);
        // return $request;
        $solicitud->nombre_sol = strtoupper($request->nombre_sol);
        $solicitud->celular_sol = $request->celular_sol;
        $solicitud->zona_sol = strtoupper($request->zona_sol);
        $solicitud->calle_sol = strtoupper($request->calle_sol);
        $solicitud->fecha_sol = $request->fecha_sol;
        $solicitud->estado_sol = $request->estado_sol;
        $solicitud->x_aprox = $request->x_aprox;
        $solicitud->y_aprox = $request->y_aprox;
        if ($request->hasFile('sol_escaneada')) {
            if ($request->file('sol_escaneada')->isValid()) {
                $file = $request->file('sol_escaneada');
                $nombre = 'solicitud_' . $solicitud->id;
                $solicitud->sol_escaneada = $nombre . '.pdf';
                Storage::disk('solicitudes')->put($solicitud->sol_escaneada,  File::get($file));
            }
        }
        $solicitud->save();
        return redirect()->route('solicitud.index')->with('editar', 'ok');
    }
    public function aprobar(Solicitud $solicitud)
    {
        $solicitud->estado_sol = "aprobado";
        $solicitud->save();
        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($solicitud->id, 'aprobado', $id);
        return redirect()->route('solicitud.index')->with('aprobar', 'Ok');
        // return $solicitud->estado_sol;
    }
    public function form_rechazado($id)
    {
        $solicitud = Solicitud::select(
            'solicituds.id as id',
            'solicituds.x_aprox as x_aprox',
            'solicituds.y_aprox as y_aprox'
        )->where('solicituds.id', $id)->first();
        return view('solicitud.rechazar', compact('solicitud'));
    }
    public function rechazar(Request $request)
    {
        $solicitud = Solicitud::find($request->id_solicitud);
        $solicitud->sol_rechazada = 'rechazada_' . $solicitud->id . '.png';
        $solicitud->estado_sol = "aplazado";
        if (strlen($request->observaciones) > 0) {
            $solicitud->observaciones = $request->observaciones;
        }
        $image_64 = $request->textMap;
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);

        $image = str_replace(' ', '+', $image);
        Storage::disk('rechazadas')->put($solicitud->sol_rechazada, base64_decode($image));
        //return var_dump($informe);
        $solicitud->save();

        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($request->id_solicitud, 'aplazado', $id);

        return redirect()->route('solicitud.index');
    }
    public function destroy(Solicitud $solicitud)
    {
        $solicitud->delete();
        return redirect()->route('solicitud.index')->with('eliminar', 'Ok');
    }

    public function PDF_rechazado(Solicitud $solicitud)
    {
        return view('PDF/reporte_rechazado', compact('solicitud'));
    }

    public function comprobante_solicitud(Solicitud $solicitud)
    {
        // $options = [
        //     'chroot' => base_path(),
        //     'isHtml5ParserEnabled' => true,
        //     'isRemoteEnabled' => true
        // ];

        // return PDF::setOptions($options)
        //     ->loadview('PDF/comprobante', compact('solicitud'))
        //     ->stream('Comprobante de Solicitud.pdf');
        return view('PDF/comprobante', compact('solicitud'));
    }

    public function solicitud_escaneada(Request $request)
    {
        $solicitud = Solicitud::find($request->id)->sol_escaneada;
        return response()->json(['mensaje' => 'Archivo encontrado', 'data' => $solicitud, 'id' => $request->id]);
    }

    public function guardarAmpliacion(Request $request, Solicitud $solicitud)
    {
        $solicitud->ampliaciones = $request->ampliaciones;
        $solicitud->save();

        return var_dump($request->ampliaciones);
    }

    public function obtenerAmpliacion(Solicitud $solicitud)
    {


        return ["ampliacion" => $solicitud->ampliaciones];
    }
    public function avance()
    {
        $solicitud = Solicitud::all();
        return view('solicitud.avance', compact('solicitud'));
    }
    public function pasos_solicitud(Request $request)
    {

        $cambios_realizados = Solicitud::join('informes','informes.solicitud_id', '=', 'solicituds.id')
            ->join('cambio_estado', 'cambio_estado.informe_id', '=', 'informes.id')
            ->where('solicituds.id', $request->id)
            ->get();
        // $bd_query = "SELECT * FROM informes as i INNER JOIN solicituds as s ON s.id = i.solicitud_id INNER JOIN cambio_estado as ce ON ce.informe_id = i.id WHERE s.id = $request->id";
        // return response()->json(['success' => true, 'mensaje' => $bd_query, 'info' => $cambios_realizados]);
        if ($cambios_realizados->isEmpty()) {
            $solicitud = Historical::where('solicitud_id', $request->id)
                ->select('estado', 'created_at')->orderBy('created_at', 'ASC')->groupBy('estado')->get();
            $ultimos_registros = DB::table('historicals')
                ->where('solicitud_id', $request->id)
                ->select('estado')
                ->orderBy('created_at', 'DESC')->groupBy('estado')
                ->take(2)
                ->get();
            if( $solicitud->isEmpty() && $ultimos_registros->isEmpty() ){
                return response()->json(['success' => false, 'mensaje' => 'Sin Informacion']);
            } else {
                return response()->json(['success' => true, 'mensaje' => 'Archivo encontrado', 'data' => $solicitud, 'id' => $request->id, 'ultimos_registros' => $ultimos_registros]);
            }
            // return response()->json(['mensaje' => 'Esta vacio']);
        } else {
            $informe_id = Informe::where('solicitud_id', $request->id)->select('id')->first();
            $solicitud = Historical::where('solicitud_id', $request->id)
                ->select('estado', 'created_at')->orderBy('created_at', 'ASC')->groupBy('estado')->get();
            $ultimos_registros = DB::table('historicals')
                ->where('solicitud_id', $request->id)
                ->select('estado')
                ->orderBy('created_at', 'DESC')->groupBy('estado')
                ->take(2)
                ->get();
            $ultimo_cambio = Informe::join('cambio_estado', 'cambio_estado.informe_id', '=', 'informes.id')
                ->where('cambio_estado.informe_id', $informe_id->id)
                ->select('cambio_estado.estado_actual', 'cambio_estado.estado_a_cambiar','cambio_estado.motivo')
                ->orderBy('cambio_estado.created_at', 'DESC')
                ->first();
            if(($solicitud->isEmpty()) && ($ultimo_cambio->isEmpty())){
                return response()->json(['success' => false, 'mensaje' => 'Sin Informacion']);
            } else {
                return response()->json(['success' => true, 'mensaje' => 'Archivo encontrado', 'data' => $solicitud, 'id' => $request->id, 'ultimos_registros' => $ultimos_registros, 'estado_que_se_cambio' => $ultimo_cambio->estado_a_cambiar, 'estado_en_el_que_estaba' => $ultimo_cambio->estado_actual, 'motivo' => $ultimo_cambio->motivo]);
            }
            // return response()->json(['mensaje' => 'No esta vacio']);
        }
    }
    public function cambiar_estado(Request $request)
    {
        // return $request;
        // pregunta clave, se podra eliminar todo el registro o solo iremos por partes
        // para ser controlados por la tabla estados

        if ($request->estado == 'solicitado') {
            $solicitud = Solicitud::findOrFail($request->solicitud_id);
            $solicitud->estado_sol = 'solicitado';
            $solicitud->save();
        }
        if ($request->estado == 'aprobado') {
            // Borrar cronograma
            $cronograma = Cronograma::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $borrar_c = Cronograma::findOrFail($cronograma->id);
            $borrar_c->delete();
            // Borrar Informe
            $cronograma = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $borrar_i = Informe::findOrFail($cronograma->id);
            $borrar_i->delete();
            // Cambiar el Estado a Solicitud Aprobada
            $solicitud = Solicitud::findOrFail($request->solicitud_id);
            $solicitud->estado_sol = 'aprobado';
            $solicitud->save();
        }
        if ($request->estado == 'rechazado') {
            $solicitud = Solicitud::findOrFail($request->solicitud_id);
            $solicitud->estado_sol = 'solicitado';
            $solicitud->save();
        }
        if ($request->estado == 'asignado') {
            // Cambiar el Estado a Solicitud Aprobada
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->espesifiar_in = '';
            $solicitud->estado_in = 'asignado';
            $solicitud->longitud_in = '';
            $solicitud->longitud_inspector = '';
            $solicitud->diametro_in = '';
            $solicitud->num_ben_in = '';
            $solicitud->num_flia_in = '';
            $solicitud->condicion_rasante = '';
            // $solicitud->cantidad_camaras = '';
            // $solicitud->cuenca = '';
            // $solicitud->origen_instalacion = '';
            $solicitud->imagen_amp = '';
            $solicitud->obs_informe = '';
            Storage::delete('Public/' . $solicitud->imagen_amp);
            $solicitud->save();
        }
        //Informe aprobada
        // if ($request->estado == 'programado') {
        //     $ejecucion = Ejecucion::where('solicitud_id', $request->solicitud_id)->select('id')->first();
        //     $borrar = Ejecucion::findOrFail($ejecucion->id);
        //     $borrar->delete();
        //     $solicitud = Solicitud::findOrFail($request->solicitud_id);
        //     $solicitud->estado_sol = 'aprobado';
        //     $solicitud->save();
        // }
        if ($request->estado == 'inspeccionado') {
            $ejecucion = Ejecucion::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $borrado_e = Ejecucion::findOrFail($ejecucion->id);
            $borrado_e->delete();
            // Borrado de la ejecucion, podra volver a editar el informe realizado por el inspector para una nueva validacion
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'inspeccionado';
            $solicitud->save();
        }
        if ($request->estado == 'autorizado') {
            // Se podra eliminar los materiales solicitados por el inspector
            $informe_id = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $mat = Informe_material::where('informe_id', $informe_id->id)->select('id')->get();
            foreach ($mat as $materiales) {
                $material = Informe_material::findOrFail($materiales->id);
                $material->delete();
            }
            // Se podra eliminar los trabajos de mano de obra solicitados por el inspector
            $ejecucion_id = Ejecucion::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $obra = Mano_obra::where('ejecucion_id', $ejecucion_id->id)->select('id')->get();
            foreach ($obra as $mano_obra) {
                $mano_de_obra = Mano_obra::findOrFail($mano_obra->id);
                $mano_de_obra->delete();
            }
            // El estado de la tabla informes vuelve a ser autorizado y poder seleccionar nuevos items para relaizar este proyecto
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'autorizado';
            $solicitud->save();
        }
        if ($request->estado == 'no autorizado') {
            $ejecucion = Ejecucion::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $borrado_e = Ejecucion::findOrFail($ejecucion->id);
            $borrado_e->delete();
            // Borrado de la ejecucion, podra volver a editar el informe realizado por el inspector para una nueva validacion
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'inspeccionado';
            $solicitud->save();
        }
        if ($request->estado == 'solicitud de firma') {
            // El estado de la tabla informes vuelve a ser autorizado y poder seleccionar nuevos items para relaizar este proyecto
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'solicitud de firma';
            $solicitud->save();
            // para volver a ver los materiales solicitados por el inspector
            $informe_id = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $mat = Informe_material::where('informe_id', $informe_id->id)->select('id')->get();
            foreach ($mat as $materiales) {
                $material = Informe_material::findOrFail($materiales->id);
                $material->estado_informe = "autorizado";
                $material->save();
            }
        }
        if ($request->estado == 'observado') {
            $ejecucion = Ejecucion::where('solicitud_id', $request->solicitud_id)->select('id')->first();

            $mano_obra = Mano_obra::where('ejecucion_id', $ejecucion->id)->get();

            if ($mano_obra) {
                $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
                $solicitud = Informe::findOrFail($soli->id);
                $solicitud->estado_in = 'observado';
                $solicitud->save();
            } else {
                $borrado_e = Ejecucion::findOrFail($ejecucion->id);
                $borrado_e->delete();
                // Borrado de la ejecucion, podra volver a editar el informe realizado por el inspector para una nueva validacion
                $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
                $solicitud = Informe::findOrFail($soli->id);
                $solicitud->estado_in = 'observado';
                $solicitud->save();
            }
        }
        if ($request->estado == 'firmado') {
            // El estado de la tabla informes vuelve a ser autorizado y poder seleccionar nuevos items para relaizar este proyecto
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'firmado';
            $solicitud->save();
            // return $obra;
        }
        if ($request->estado == 'ejecutadndose') {
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'ejecutadndose';
            $solicitud->save();
        }
        if ($request->estado == 'ejecutado') {
            $soli = Informe::where('solicitud_id', $request->solicitud_id)->select('id')->first();
            $solicitud = Informe::findOrFail($soli->id);
            $solicitud->estado_in = 'ejecutado';
            $solicitud->save();
        }
        $ultimo_id = Historical::where('solicitud_id', $request->solicitud_id)
            ->select('id')
            ->orderBy('id', 'desc')
            ->first();
        $destroy_historical = Historical::findorFail($ultimo_id->id);
        $destroy_historical->delete();
        return redirect()->route('solicitud.avance');
    }
    public function reporte_historicos(Request $request)
    {
        $solicitud = Historical::join('users', 'users.id', '=', 'historicals.usuario_id')
            ->where('historicals.solicitud_id', $request->id)
            ->select('historicals.estado', 'historicals.created_at', 'users.tipo_user', 'users.name')
            ->orderBy('historicals.created_at', 'ASC')
            ->groupBy('historicals.estado')
            ->get();

        foreach ($solicitud as $historical) {
            $historical->created_at = Carbon::parse($historical->created_at)->format('Y-m-d H:i:s');
            // calcula la cantidad de días transcurridos entre cada tarea
            // y almacénalos en una nueva propiedad del objeto
            $historical->dias_transcurridos = 0;
        }

        for ($i = 1; $i < count($solicitud); $i++) {
            $fecha_anterior = Carbon::parse($solicitud[$i - 1]->created_at);
            $fecha_actual = Carbon::parse($solicitud[$i]->created_at);
            $dias_transcurridos = $fecha_anterior->diffInDays($fecha_actual);
            $solicitud[$i]->fecha_anterior = date('d-m-Y', strtotime($fecha_anterior));
            $solicitud[$i]->fecha_actual = date('d-m-Y', strtotime($fecha_actual));
            $solicitud[$i]->dias_transcurridos = $dias_transcurridos;
        }

        // return $solicitud;
        return view('PDF.reporte_avance', ['solicitud' => $solicitud]);
    }
}
