<?php

namespace App\Http\Controllers;

use App\Models\Ejecucion;
use App\Models\Historical;
use Illuminate\Http\Request;
use App\Models\Informe;
use App\Models\Informe_material;
use App\Models\Solicitud;
use App\Models\Material;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class InformeController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $aux = Auth::user()->tipo_user;
        if ($aux == 'Inspector') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.celular_sol as celular_sol',
                    'solicituds.calle_sol as calle_sol',
                    'solicituds.zona_sol as zona_sol',
                    'solicituds.x_aprox as x_aprox',
                    'solicituds.y_aprox as y_aprox',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.ubicacion_geo as ubicacion',
                    'informes.longitud_inspector as longitud_inspector',
                    'informes.imagen_amp as captura',
                    'informes.x_exact as x_exact',
                    'informes.y_exact as y_exact',
                    'informes.estado_in as estado'
                )
                ->where('cronogramas.user_id', $id)
                ->whereIn('informes.estado_in', ['asignado', 'observado'])
                ->get();
            // return $informes;
            return view('informes.index', compact('informes', 'id'));
        } else if ($aux == 'Jefe de red') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users as usr', 'usr.id', '=', 'cronogramas.user_id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.celular_sol as celular_sol',
                    'solicituds.calle_sol as calle_sol',
                    'solicituds.zona_sol as zona_sol',
                    'solicituds.x_aprox as x_aprox',
                    'solicituds.y_aprox as y_aprox',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.longitud_inspector as longitud_inspector',
                    'informes.imagen_amp as captura',
                    'informes.estado_in as estado',
                    'informes.x_exact as x_exact',
                    'informes.y_exact as y_exact',
                    'informes.ubicacion_geo as ubicacion',
                    'cronogramas.user_id as user_id',
                    'usr.name as nombre_inspector'
                )
                // ->where('informes.estado_in', 'asignado')
                // ->orWhere('informes.estado_in', 'inspeccionado')
                ->whereIn('informes.estado_in', ['asignado', 'inspeccionado', 'observado'])
                ->get();
            // return $informes;
            return view('informes.index', compact('informes', 'id'));
        } else if ($aux == 'Administrador') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.celular_sol as celular_sol',
                    'solicituds.calle_sol as calle_sol',
                    'solicituds.zona_sol as zona_sol',
                    'solicituds.x_aprox as x_aprox',
                    'solicituds.y_aprox as y_aprox',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.ubicacion_geo as ubicacion',
                    'informes.longitud_inspector as longitud_inspector',
                    'informes.imagen_amp as captura',
                    'informes.x_exact as x_exact',
                    'informes.y_exact as y_exact',
                    'informes.estado_in as estado'
                )
                ->get();
            // return $informes;
            return view('informes.index', compact('informes', 'id'));
        } else {
            abort(403);
        }
    }
    public function inspeccionado()
    {
        $id = Auth::user()->id;
        $informes = DB::table('informes')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->select(
                'informes.id as id_informe',
                'solicituds.id as id_solicitud',
                'solicituds.nombre_sol as nombre_sol',
                'solicituds.celular_sol as celular_sol',
                'solicituds.calle_sol as calle_sol',
                'solicituds.zona_sol as zona_sol',
                'solicituds.x_aprox as x_aprox',
                'solicituds.y_aprox as y_aprox',
                'informes.fecha_hora_in as fecha_inspeccion',
                'informes.ubicacion_geo as ubicacion',
                'informes.longitud_inspector as longitud_inspector',
                'informes.imagen_amp as captura',
                'informes.x_exact as x_exact',
                'informes.y_exact as y_exact',
                'informes.estado_in as estado'
            )
            ->where('cronogramas.user_id', $id)
            ->whereIn('informes.estado_in', ['inspeccionado'])
            ->get();
        // return $informes;
        return view('informes.inspeccionados', compact('informes', 'id'));
    }

    public function autorizar(Request $request, $id)
    {
        date_default_timezone_set('America/La_Paz');
        $fecha = date('Y-m-d');
        $informe = Informe::find($id);
        $informe->estado_in = "autorizado";
        $informe->fecha_autorizado = $fecha;
        $informe->save();

        $verificacion_ejecutados = Ejecucion::join('informes', 'informes.id', '=', 'ejecucions.informe_id')->where([
            ['ejecucions.informe_id', $id]
        ])->exists();
        // return $verificacion_ejecutados;
        if ($verificacion_ejecutados) {

            $id_user = Auth::user()->id;
            $historical = new Historical();
            $historical->registrar_historico($informe->solicitud_id, 'autorizado', $id_user);
            // return 'No Hay';
        } else {
            // return 'Hay';
            $ejecucion = new Ejecucion();
            $ejecucion->fecha_progrmada = $request->fecha_programada;
            $ejecucion->user_id = $request->user_id;
            $ejecucion->informe_id = $informe->id;
            $ejecucion->solicitud_id = $request->solicitud_id;
            $ejecucion->save();

            $id_user = Auth::user()->id;
            $historical = new Historical();
            $historical->registrar_historico($informe->solicitud_id, 'autorizado', $id_user);
        }

        return redirect()->route('informes.index');
        // return $verificacion_ejecutados;
    }
    public function no_autorizar(Informe $informe)
    {
        // El informe proporcionado no esta bien elaborado, tiene que reacer el inspector
        $informe->estado_in = "observado";
        $informe->save();

        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($informe->solicitud_id, 'observado', $id);

        return redirect()->route('informes.index');
        // return $solicitud->estado_in;
    }
    public function firmar_informe(Informe $informe)
    {
        // Validamos si en este informe ya se solicito materiales
        $id = $informe->id;
        // Cambiamos el estado de los materiales
        $materials = Informe_material::where([
            ['informe_id', '=', $id],
            ['estado_informe', 'autorizado']
        ])->get();
        $validacion = Informe_material::where([
            ['informe_id', '=', $id],
            ['estado_informe', 'firmado']
        ])->exists();
        // return $validacion;
        // if($validacion){return 'ejecutandose';} else {return 'firmado';}
        if ($validacion) {
            $informe->estado_in = "ejecutandose";
            $id = Auth::user()->id;
            $historical = new Historical();
            $historical->registrar_historico($informe->solicitud_id, 'ejecutandose', $id);

            foreach ($materials as $mat) {
                $mat = Informe_material::findOrFail($mat->id);
                $mat->fecha_aprobado = date('Y-m-d');
                $mat->estado_informe = 'firmado';
                $mat->save();
            }
        } else {
            $informe->estado_in = "firmado";

            $id = Auth::user()->id;
            $historical = new Historical();
            $historical->registrar_historico($informe->solicitud_id, 'firmado', $id);

            foreach ($materials as $mat) {
                $mat = Informe_material::findOrFail($mat->id);
                $mat->fecha_aprobado = date('Y-m-d');
                $mat->estado_informe = 'firmado';
                $mat->save();
            }
        }
        $informe->save();

        return redirect()->route('informes.autorizado');
    }
    public function aprobar_proyecto(Informe $informe)
    {
        date_default_timezone_set('America/La_Paz');
        $fecha = date('Y-m-d');
        $informe->estado_in = "ejecutandose";
        $informe->fecha_visto_bueno = $fecha;
        $informe->save();

        $id = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($informe->solicitud_id, 'ejecutandose', $id);

        return redirect()->route('proyectos.index');
        // return $solicitud->estado_in;
    }
    public function autorizado()
    {
        $id = Auth::user()->id;
        $aux = Auth::user()->tipo_user;
        if ($aux == 'Inspector') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->leftJoin('ejecucions', 'ejecucions.informe_id', '=', 'informes.id')
                ->select(
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'solicituds.sol_escaneada as archivo',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'informes.id as id_informe',
                    'informes.x_exact as x_exact',
                    'informes.y_exact as y_exact',
                    'cronogramas.user_id as user_id',
                    'ejecucions.id as id_ejecucion',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada',
                )
                ->where('cronogramas.user_id', $id)
                ->whereIn('informes.estado_in', ['autorizado', 'firmado', 'solicitud de firma', 'observado'])
                // ->where(function ($query) {
                //     $query->where('informes.estado_in', 'autorizado')
                //         ->orWhere('informes.estado_in', 'firmado');
                // })
                ->get();
        } else if ($aux == 'Administrador') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users', 'users.id', '=', 'cronogramas.user_id')
                ->leftJoin('ejecucions', 'ejecucions.informe_id', '=', 'informes.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'solicituds.sol_escaneada as archivo',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'informes.x_exact as x_exact',
                    'informes.y_exact as y_exact',
                    'cronogramas.user_id as user_id',
                    'ejecucions.id as id_ejecucion',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada',
                    'users.name as nombre_inspector'
                )
                ->whereIn('informes.estado_in', ['autorizado', 'firmado', 'solicitud de firma', 'observado', 'ejecutandose', 'ejecutado'])
                ->get();
        } else {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users', 'users.id', '=', 'cronogramas.user_id')
                ->leftJoin('ejecucions', 'ejecucions.informe_id', '=', 'informes.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'solicituds.sol_escaneada as archivo',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'informes.x_exact as x_exact',
                    'informes.y_exact as y_exact',
                    'cronogramas.user_id as user_id',
                    'ejecucions.id as id_ejecucion',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada',
                    'users.name as nombre_inspector'
                )
                ->whereIn('informes.estado_in', ['autorizado', 'firmado', 'solicitud de firma', 'observado'])
                // ->orWhere('informes.estado_in', 'firmado')
                // ->orWhere('informes.estado_in', 'solicitud de firma')
                ->get();
        }
        return view('informes.autorizado', compact('informes'));
        // return $informes;
    }
    public function concluido()
    {
        $id = Auth::user()->id;
        $aux = Auth::user()->tipo_user;
        if ($aux == 'Inspector') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('ejecucions', 'ejecucions.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'solicituds.zona_sol as zonal_sol',
                    'solicituds.calle_sol as calle_sol',
                    'ejecucions.id as id_ejecucion',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada'
                )
                ->where('cronogramas.user_id', $id)
                ->whereIn('informes.estado_in', ['ejecutandose', 'firmado', 'solicitud de firma'])
                ->get();
        } else {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users', 'users.id', '=', 'cronogramas.user_id')
                ->join('ejecucions', 'ejecucions.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada',
                    'users.name as nombre_inspector'
                )
                ->whereIn('informes.estado_in', ['ejecutandose', 'ejecutado', 'firmado', 'solicitud de firma'])
                ->get();
        }

        return view('informes.concluido', compact('informes'));
        // return $informes;
    }
    public function informes_ejecutados() {
        $id = Auth::user()->id;
        $aux = Auth::user()->tipo_user;
        if ($aux == 'Inspector') {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('ejecucions', 'ejecucions.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'solicituds.zona_sol as zonal_sol',
                    'solicituds.calle_sol as calle_sol',
                    'ejecucions.id as id_ejecucion',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada'
                )
                ->where('cronogramas.user_id', $id)
                ->where('informes.estado_in', '=', 'ejecutado')
                ->get();
        } else {
            $informes = DB::table('informes')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
                ->join('users', 'users.id', '=', 'cronogramas.user_id')
                ->join('ejecucions', 'ejecucions.solicitud_id', '=', 'solicituds.id')
                ->select(
                    'informes.id as id_informe',
                    'solicituds.id as id_solicitud',
                    'solicituds.nombre_sol as nombre_sol',
                    'solicituds.zona_sol as zona_sol',
                    'informes.fecha_hora_in as fecha_inspeccion',
                    'informes.estado_in as estado',
                    'ejecucions.fecha_progrmada as fecha_programada',
                    'ejecucions.fecha_ejecutada as fecha_ejecutada',
                    'users.name as nombre_inspector'
                )
                ->where('informes.estado_in', '=', 'ejecutado')
                ->get();
        }

        return view('informes.ejecutados', compact('informes'));
    }
    public function create()
    {
        $solicitud = Solicitud::all();
        return view('informes.create', compact('solicitud'));
    }
    public function store(Request $request)
    {
        $informe = new Informe();
        $informe->fecha_hora_in = $request->fecha_hora_in;
        $informe->espesifiar_in = $request->espesifiar_in;
        $informe->x_exact = $request->x_exact;
        $informe->y_exact = $request->y_exact;
        $informe->ubicacion_geo = $request->ubicacion_geo;
        $informe->longitud_in = $request->longitud_in;
        $informe->diametro_in = $request->diametro_in;
        $informe->num_ben_in = $request->num_ben_in;
        $informe->num_flia_in = $request->num_flia_in;
        $informe->reservorio = $request->reservorio;
        $informe->condicion_rasante = $request->condicion_rasante;
        $informe->estado_in = $request->estado_in;
        $informe->solicitud_id = $request->solicitud_id;
        $informe->imagen_amp = 'informe_' + $request->solicitud_id + '.png';

        $image_64 = $request->textMap;
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);

        $image = str_replace(' ', '+', $image);
        Storage::disk('public')->put('informe_' + $request->solicitud_id + '.png', base64_decode($image));
        //return var_dump($informe);
        $informe->save();

        return redirect()->route('informes.index');
    }
    public function show_costos(Informe $informe)
    {
        $mat_inf = DB::table('materials')
            ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
            ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
            ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
            ->select(
                'informe_materials.id as id',
                'informes.id as id_i',
                'materials.nombre_material as material_n',
                'solicituds.nombre_sol as nombre_sol',
                'informe_materials.cantidad as cantidad',
                'informe_materials.precio_unitario as precio_unitario',
                'informe_materials.sub_total as sub_total',
                'informe_materials.u_medida as u_medida'
            )
            ->where([
                ['materials.estado', 'disponible'],
                ['informes.id', $informe->id]
            ])
            ->get();
        return view('material_informe.editar_costo', compact('mat_inf'));
    }
    public function update_costos(Request $request) {
        try {
            $change = Informe_material::find($request->id);
            $change->cantidad = $request->cantidad_solicitada;
            $change->precio_unitario = $request->precioUnitario;
            $change->sub_total = $request->precioTotal;
            $change->save();
            return response()->json(['status' => 'ok', 'message' => 'Costos actualizados correctamente']);
            // return redirect()->route('informes.show_costos')->with('modificado', 'ok');
        } catch (\Throwable $th) {
            return $th->getMessage();
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
            // return redirect()->route('informes.show_costos')->with('modificado', 'error');
        }
    }
    public function show(Informe $informe)
    {
        // Refactorizar
        $validar = Informe::where('id', $informe->id)->select('estado_in')->first();
        $estado = $validar->estado_in;
        $aux = Auth::user()->tipo_user;
        if ($aux == 'Administrador') {
            $mat_inf = DB::table('materials')
                ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
                ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                ->select(
                    'informe_materials.id as id',
                    'informes.id as id_i',
                    'materials.nombre_material as material_n',
                    'solicituds.nombre_sol as nombre_sol',
                    'informe_materials.cantidad as cantidad',
                    'informe_materials.precio_unitario as precio_unitario',
                    'informe_materials.u_medida as u_medida'
                )
                ->where([
                    ['materials.estado', 'disponible'],
                    ['informes.id', $informe->id]
                ])
                ->get();
        } else {
            if ($estado === "autorizado") {
                $mat_inf = DB::table('materials')
                    ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
                    ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                    ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                    ->select(
                        'informe_materials.id as id',
                        'informes.id as id_i',
                        'materials.nombre_material as material_n',
                        'solicituds.nombre_sol as nombre_sol',
                        'informe_materials.cantidad as cantidad',
                        'informe_materials.u_medida as u_medida'
                    )
                    ->where([
                        ['materials.estado', 'disponible'],
                        ['informes.id', $informe->id]
                    ])
                    // ->whereDate('fecha_aprobado','>=','2023-01-01')
                    ->get();
            } else {
                $mat_inf = DB::table('materials')
                    ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
                    ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                    ->join('solicituds', 'solicituds.id', '=', 'informes.solicitud_id')
                    ->select(
                        'informe_materials.id as id',
                        'informes.id as id_i',
                        'materials.nombre_material as material_n',
                        'solicituds.nombre_sol as nombre_sol',
                        'informe_materials.cantidad as cantidad',
                        'informe_materials.u_medida as u_medida'
                    )
                    ->where([
                        ['materials.estado', 'disponible'],
                        ['informes.id', $informe->id],
                        ['informe_materials.estado_informe', '<>', 'firmado']
                    ])
                    // ->whereDate('fecha_aprobado','>=','2023-01-01')
                    ->get();
            }
        }
        $materials = Material::where('estado', 'disponible')->get();
        return view('material_informe.index', compact('mat_inf', 'materials', 'informe'));
    }
    public function registrar_material(Informe $informe)
    {
        $materials = Material::where('estado', 'disponible')->get();
        // Refactorizar
        $validar = Informe::where('id', $informe->id)->select('estado_in')->first();
        $estado = $validar->estado_in;
        $aux = Auth::user()->tipo_user;
        if ($aux == 'Administrador') {
            $mat_inf = DB::table('materials')
                ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
                ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                ->select(
                    'informe_materials.id as id',
                    'informes.id as id_i',
                    'materials.codigo as codigo',
                    'materials.nombre_material as material_n',
                    'informe_materials.cantidad as cantidad',
                    'materials.unidad_med as unidad',
                    'informe_materials.precio_unitario as precio',
                    'informe_materials.observador as observador',
                )
                ->where([
                    ['materials.estado', 'disponible'],
                    ['informes.id', $informe->id],
                ])
                ->whereNotIn('informe_materials.estado_informe', ['Hardik', 'Vimal', 'Harshad'])
                ->get();
        } else {

            if ($estado === "autorizado") {
                $mat_inf = DB::table('materials')
                    ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
                    ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                    ->select(
                        'informe_materials.id as id',
                        'informes.id as id_i',
                        'materials.codigo as codigo',
                        'materials.nombre_material as material_n',
                        'informe_materials.cantidad as cantidad',
                        'materials.unidad_med as unidad',
                        'informe_materials.precio_unitario as precio',
                        'informe_materials.observador as observador',
                    )
                    ->where([
                        ['materials.estado', 'disponible'],
                        ['informes.id', $informe->id],
                    ])
                    ->whereNotIn('informe_materials.estado_informe', ['Hardik', 'Vimal', 'Harshad'])
                    ->get();
            } else {
                $mat_inf = DB::table('materials')
                    ->join('informe_materials', 'materials.id', '=', 'informe_materials.material_id')
                    ->join('informes', 'informes.id', '=', 'informe_materials.informe_id')
                    ->select(
                        'informe_materials.id as id',
                        'informes.id as id_i',
                        'materials.codigo as codigo',
                        'materials.nombre_material as material_n',
                        'informe_materials.cantidad as cantidad',
                        'materials.unidad_med as unidad',
                        'informe_materials.sub_total as precio',
                        'informe_materials.observador as observador',
                    )
                    ->where([
                        ['materials.estado', 'disponible'],
                        ['informes.id', $informe->id],
                        ['informe_materials.estado_informe', '<>', 'firmado']
                    ])
                    ->whereNotIn('informe_materials.estado_informe', ['Hardik', 'Vimal', 'Harshad'])
                    ->get();
            }
        }
        $data="";
        // // Api para usar bd_activos
        // $response = Http::get('http://192.168.18.123/bd_activos/ajax/activos.php?op=listar');
        // if ($response->successful()) {
        //     $data = $response->json(); // Obtener los datos de la respuesta en formato JSON
        //     // Manejar los datos de acuerdo a tus necesidades
        // } else {
        //     // Manejar el error de la llamada
        //     $errorMessage = $response->status() . ' - ' . $response->body();
        // }
        return view('material_informe.create', compact('materials'), compact('informe', 'mat_inf', 'data'));
    }
    public function edit(Informe $informe)
    {
        return view('informes.edit', compact('informe'));
    }
    public function update(Request $request, Informe $informe)
    {
        $request->validate([
            'fecha_hora_in' => 'required',
            'espesifiar_in' => 'required',
            'x_exact' => 'required',
            'y_exact' => 'required',
            'ubicacion_geo' => 'required',
            'longitud_in' => 'required',
            'longitud_inspector' => 'required',
            'diametro_in' => 'required',
            'num_ben_in' => 'required',
            'num_flia_in' => 'required',
            'condicion_rasante' => 'required',
            'solicitud_id' => 'required',
            'obs_informe' => 'nullable|string',
            'reservorio' => 'required|string',
            // 'cantidad_camaras' => 'required|integer',
            // 'cuenca' => 'required|string',
            // 'origen_instalacion' => 'required|string',
        ]);

        $informe->fecha_hora_in = $request->fecha_hora_in;
        $informe->espesifiar_in = $request->espesifiar_in;
        $informe->x_exact = $request->x_exact;
        $informe->y_exact = $request->y_exact;
        $informe->ubicacion_geo = $request->ubicacion_geo;
        $informe->longitud_in = $request->longitud_in;
        $informe->longitud_inspector = $request->longitud_inspector;
        $informe->diametro_in = $request->diametro_in;
        $informe->num_ben_in = $request->num_ben_in;
        $informe->num_flia_in = $request->num_flia_in;
        $informe->condicion_rasante = $request->condicion_rasante;
        $informe->solicitud_id = $request->solicitud_id;
        $informe->imagen_amp = 'informe_' . $request->solicitud_id . '.png';
        $informe->obs_informe = strtoupper($request->obs_informe);
        $informe->reservorio = strtoupper($request->reservorio);
        // $informe->cantidad_camaras = strtoupper($request->cantidad_camaras);
        // $informe->cuenca = strtoupper($request->cuenca);
        // $informe->origen_instalacion = strtoupper($request->origen_instalacion);


        if ($request->textMap != null) {
            $image_64 = $request->textMap;
            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            $image = str_replace($replace, '', $image_64);

            $image = str_replace(' ', '+', $image);
            Storage::disk('public')->put('informe_' . $request->solicitud_id . '.png', base64_decode($image));
        }
        $informe->save();

        // $id = Auth::user()->id;
        // $historical = new Historical();
        // $historical->registrar_historico($request->solicitud_id, 'inspeccionado', $id);
        return redirect()->route('informes.index')->with('modificado', 'ok');
    }
    public function esperar_aprobacion(Request $request)
    {
        //revisar aqui el historico
        $informe = Informe::findOrFail($request->id_informe);
        $materials = Informe_material::where([
            ['informe_id', '=', $request->id_informe],
            ['fecha_aprobado', '<>', NULL]
        ])->get();
        // return $materials;
        $estado = Informe::findOrFail($request->id_informe)->estado_in;
        if ($estado == 'ejecutandose') {
            $informe->estado_in = 'autorizado';
            $id = Auth::user()->id;
            $historical = new Historical();
            $historical->registrar_historico($informe->solicitud_id, 'autorizado', $id);
        } else {
            $informe->estado_in = 'solicitud de firma';
            $id = Auth::user()->id;
            $historical = new Historical();
            $historical->registrar_historico($informe->solicitud_id, 'solicitud de firma', $id);
        }
        $informe->save();
        return redirect()->route('informes.autorizado');
    }
    public function devolver_asignado($id)
    {
        $informe = Informe::findOrFail($id);
        $informe->estado_in = "observado";
        $informe->save();

        $id_user = Auth::user()->id;
        $historical = new Historical();
        $historical->registrar_historico($informe->solicitud_id, 'observado', $id_user);
        return redirect()->route('informes.autorizado');
    }
    public function paso_atras(Request $request)
    {
        // recibe el estado actual y el estado a donde se quiere ir, considerando
        // que  no modificara o destruira nada de los procesos ya realizados
        // por vista recibiremos el id pero de la solicitud
        $informe = new Informe();
        $cambio = $informe->cambio_estado_informe($request->solicitud_id_actual, $request->estado_actual, $request->estado_a_cambiar, $request->motivo);
        if ($cambio['success'] === true) {
            // return response()->json(['mensaje' => $cambio['message']]);
            return redirect()->route('solicitud.avance');
        } else {
            return redirect()->route('solicitud.avance');
            // return response()->json(['mensaje' => $cambio['message']]);
        }
    }
    public function verificacion_estado(Request $request)
    {
        $informe = Informe::findOrFail($request->id)->estado_in;
        if (!empty($informe)) {
            return response()->json(['mensaje' => 'Datos Encontrados', 'data' => $informe]);
        } else {
            return response()->json(['mensaje' => 'Datos No Encontrados', 'data' => '']);
        }
    }
    public function destroy($id)
    {
        //
    }
    public function ejecutados(Request $request)
    {
        $informes = Ejecucion::join('informes', 'informes.id', '=', 'ejecucions.informe_id')
            ->join('solicituds', 'solicituds.id', '=', 'ejecucions.solicitud_id')
            ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'users.id', '=', 'cronogramas.user_id')
            ->select('users.name', 'solicituds.nombre_sol', 'solicituds.fecha_sol', 'informes.fecha_hora_in', 'solicituds.calle_sol', 'solicituds.zona_sol', 'informes.estado_in', 'ejecucions.fecha_ejecutada', 'ejecucions.fecha_progrmada')
            ->where('informes.estado_in', 'ejecutado')
            ->when(($request->fecha_inicio != null && $request->fecha_fin != null), function ($q) use ($request) {
                $q->whereBetween('ejecucions.fecha_ejecutada', [$request->fecha_inicio, $request->fecha_fin]);
            })
            ->orderBy('ejecucions.fecha_ejecutada', 'desc')
            ->get();
        return view('informes.vista_concluidos', ['informes' => $informes, 'fecha_inicio' => $request->fecha_inicio, 'fecha_fin' => $request->fecha_fin]);
    }
    // public function vista_ejecutados(Request $request)
    // {

    //     // $informe = Solicitud::whereBetween('fecha_ejecutada', [$request->fecha_inicio, $request->fecha_fin])->select('solicitud_id')->get();
    //     // $solicitud = Solicitud::findOrFail($request->solicitud_id);
    //     if ($informe) {
    //         return response()->json(['mensaje' => 'Datos Encontrados', 'data' => $informe]);
    //     } else {
    //         return response()->json(['mensaje' => 'Sin Datos', 'data' => []]);
    //     }
    //     // return response()->json(['fecha_inicio'=>$request->fecha_inicio,'fecha_fin'=>$request->fecha_fin]);
    // }
    public function reporte_ejecutados($fecha_inicio = null, $fecha_fin = null)
    {
        // $informe = Ejecucion::join('informes', 'informes.id', '=', 'ejecucions.informe_id')
        //     ->join('solicituds', 'solicituds.id', '=', 'ejecucions.solicitud_id')
        //     ->select('solicituds.id', 'solicituds.nombre_sol', 'solicituds.fecha_sol', 'informes.fecha_hora_in', 'solicituds.calle_sol', 'solicituds.zona_sol', 'informes.estado_in', 'ejecucions.fecha_ejecutada')
        //     ->where('informes.estado_in', 'ejecutado')
        //     ->when(($fecha_inicio != null && $fecha_fin != null), function ($q) use ($fecha_inicio, $fecha_fin) {
        //         $q->whereBetween('ejecucions.fecha_ejecutada', [$fecha_inicio, $fecha_fin]);
        //     })
        //     ->orderBy('ejecucions.fecha_ejecutada','desc')
        //     ->get();

        $informe = Ejecucion::join('informes', 'informes.id', '=', 'ejecucions.informe_id')
            ->join('solicituds', 'solicituds.id', '=', 'ejecucions.solicitud_id')
            ->join('cronogramas', 'cronogramas.solicitud_id', '=', 'solicituds.id')
            ->join('users', 'users.id', '=', 'cronogramas.user_id')
            ->select('users.name', 'solicituds.nombre_sol', 'solicituds.fecha_sol', 'informes.fecha_hora_in', 'solicituds.calle_sol', 'solicituds.zona_sol', 'informes.estado_in', 'ejecucions.fecha_ejecutada', 'ejecucions.fecha_progrmada')
            ->where('informes.estado_in', 'ejecutado')
            ->when(($fecha_inicio != null && $fecha_fin != null), function ($q) use ($fecha_inicio, $fecha_fin) {
                $q->whereBetween('ejecucions.fecha_ejecutada', [$fecha_inicio, $fecha_fin]);
            })
            ->orderBy('ejecucions.fecha_ejecutada', 'desc')
            ->get();
        return view('PDF.reporte_concluidos', compact('informe', 'fecha_inicio', 'fecha_fin'));
    }
    public function solicitar_autorizacion($informe_id)
    {
        $informe = Informe::findOrFail($informe_id);
        $informe->estado_in = 'inspeccionado';
        $informe->save();
        return redirect()->route('informes.index');
    }
}
